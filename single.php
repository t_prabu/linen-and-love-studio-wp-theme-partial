<?php 
  get_header(); 

?>

<section class="content-section-wrapper">

    <div class="container">
        <div class="content-wrapper row">
            <div class="col-md-8">
                <main>
                    <?php 
                      if ( have_posts() ):
  while ( have_posts() ) : the_post(); 
?>
                    <article class="article-wrapper">
                        <div class="top-wrapper">
                            <?php if (has_post_thumbnail()): ?>

                            <div class="img-wrapper">
                                <div class="img-bg"
                                    style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(), 'post-thumb')[0]; ?>);">
                                </div>
                                <img src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(), 'post-thumb')[0]; ?>"
                                    class="img-fluid" alt="" />
                            </div>
                            <?php endif; ?>
                            <div class="article-date">
                                <div class="text"><?php echo get_the_date('j'); ?>
                                    <?php echo get_the_date('F'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <header>
                                <h2>
                                    <?php echo explode(' ', get_the_title())[0] ;?>
                                    <strong> <?php echo implode(' ',array_slice(explode(' ', get_the_title()), 1)) ;?></strong>
                                </h2>
                                <hr />
                            </header>


                            <?php  the_content() ;?>

                        </div>
                    </article>
                    <?php
    endwhile; 
 endif;
 ?>
                </main>
            </div>
            <div class="col-md-4 p-md-0">
                <?php  echo get_template_part('/template-parts/page-sidebar') ;?>
            </div>
        </div>
    </div>
</section>


<?php
  get_footer(); 
