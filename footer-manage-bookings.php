<!--  footer-info section -->
<footer class="footer-section-wrapper">
	<div class="footer-rights">
		<div class="container">
			<div class="row">
				<div class="order-2 order-md-1 col-md-5 text-center text-md-left"><a href="#">&copy; <?php echo date("Y");?> Linen
						and Love Studio. All rights reserved</a></div>
				<div class="order-1 order-md-2 col-md-7 text-center text-md-right">
					<!-- <a href="#">Terms & Conditions</a> | -->
					<a
						href="<?php echo site_url('deposits-cancellation') ;?>">Deposits
						& Cancellation</a> |
					<a
						href="<?php echo site_url('policies-rules-guidelines') ;?>">Policies,
						Rules & Guidelines</a>
				</div>
			</div>
		</div>
	</div>
</footer>
<!--  /footer-info section -->
<!-- / Footer End -->
<?php  echo get_template_part('/template-parts/vue-templates') ;?>
<?php wp_footer(); ?>
</body>

</html>