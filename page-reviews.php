<?php 
  get_header(); 

?>
<section class="content-section-wrapper">
    <header class="content-section-header">
        <div class="container">
            <h1><strong><?php echo explode(' ', get_the_title())[0] ;?></strong>
                <?php echo implode(' ',array_slice(explode(' ', get_the_title()), 1)) ;?>
            </h1>
            <h4>Luxury and natural light photography</h4>
        </div>
    </header>
    <section class="review-section-wrapper vmp-b-70 ">
        <div class="container">
            <?php 
                      $args = [
    'post_type' => 'reviews',
    'order' => 'DESC'];
    
$loop = new WP_Query($args);
$counter  = 0;

                      if ($loop->have_posts()):
while ($loop->have_posts()): $loop->the_post(); ?>
            <div
                class="review-wrapper js-review-wrapper <?php echo $counter>1 ? 'd-none' : '' ;?>">
                <div class="top-wrapper">
                    <div class="img-wrapper">
                        <div class="img-bg"
                            style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(), 'review-author-thumb')[0]; ?>);">
                        </div>
                        <img src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(), 'review-author-thumb')[0]; ?>"
                            class="img-fluid" alt="" />
                    </div>
                </div>
                <div class="content">
                    <p>
                        <?php echo nl2br(get_field('review_text')) ;?>
                    </p>
                </div>
                <div class="bottom-wrapper">
                    <div class="author"><?php the_title(); ?>
                    </div>
                    <div class="review-rating text-center">
                        <?php for ( $i = 0;  $i < get_field('review_rating'); $i++ ): ?>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <?php endfor; ?>
                    </div>
                </div>
            </div>
            <?php
            $counter +=1;
                endwhile; 
                wp_reset_postdata();
    // wpbeginner_numeric_posts_nav();
 endif;
 ?>
         

        </div>
        <button class="view-more-btn js-view-all-review-btn">View All Reviews <i style="display:none;"
                class="fa fa-circle-o-notch fa-spin fa-fw"></i></button>
    </section>
</section>
<?php
  get_footer(); 
