<?php
header('Content-Type: application/javascript');


$javascript = [
	'../plugins/jquery/jquery-3.3.1.min.js',
	'../plugins/bootstrap/bootstrap.bundle.min.js',
	'../plugins/owlcarousel2/owl.carousel.min.js',
	'../plugins/fancybox/jquery.fancybox.min.js',
	'../plugins/sweetalert2/sweetalert2.all.min.js',
	'../plugins/axios/axios.min.js',
	'../plugins/moment/moment.min.js',
	'../plugins/vue/vue.js',
	'../plugins/v-calendar/v-calendar.min.js',
	'../plugins/vue-moment-lib/vue-moment-lib.umd.min.js',
	'custom-min.js',
	'sub-rooms-component-min.js',
	'booking-app-min.js',
	'cart-app-min.js'
	];
foreach ($javascript as $file) {
    echo file_get_contents($file) . "\n";
}
