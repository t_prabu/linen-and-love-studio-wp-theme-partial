<?php
header('Content-Type: application/javascript');

$javascript = [
	'manage-booking-app-min.js',
	'manage-booking-room-calendar-app-min.js',
	'calendar-app-min.js'
	];
foreach ($javascript as $file) {
    echo file_get_contents($file) . "\n";
}
