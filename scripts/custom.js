(function ($) {
  /* layzyInits */
  window.addEventListener("load", loadAllTooLazy);
  window.addEventListener("load", getSetInstaHeroGallery);
  /* /layzyInits */
  updateHeaderCartBtn();
  $(function () {
    // $('.js-home-testimonial-slider').owlCarousel({
    // 	items: 1,
    // 	smartSpeed: 500,
    // 	navSpeed: 1000,
    // 	autoplayHoverPause: true,
    // 	dots: true
    // nav: true,
    // navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>',
    //     '<i class="fa fa-angle-right" aria-hidden="true"></i>'
    // ]
    // 	});

    (function () {
      "use strict";
      $(".js-studio-owl-active-big-thumb").attr(
        "src",
        $(".js-studio-top-slider").find("img:eq(0)").attr("src")
      );
    })();

    let jsStudioTopSlider = $(".js-studio-top-slider").owlCarousel({
      responsive: {
        0: {
          items: 1,
        },
        678: {
          items: 2,
        },
        1000: {
          items: 5,
        },
      },
      smartSpeed: 500,
      navSpeed: 1000,
      autoplayHoverPause: true,
      margin: 5,
      dots: true,
      nav: true,
      autoplay: true,
      autoplayTimeout: 5000,
      loop: true,
      navText: [
        '<svg xmlns="http://www.w3.org/2000/svg" width="23" height="75"><path fill-rule="evenodd" fill="#ADADAD" d="M23.013 74.987L1.358 37.501 23.013.011h-1.369L-.013 37.501l21.657 37.486h1.369z"/></svg>',
        '<svg xmlns="http://www.w3.org/2000/svg" width="23" height="75"><path fill-rule="evenodd" fill="#ADADAD" d="M-.013 74.987l21.655-37.486L-.013.011h1.369l21.657 37.49L1.356 74.987H-.013z"/></svg>',
      ],
      // afterMove: function(elem) {
      //   var current = this.currentItem;
      //   var src = elem
      //     .find(".owl-item")
      //     .eq(current)
      //     .find("img")
      //     .attr("src");
      //     console.log(src);

      // //   // console.log("Image current is " + src);
      // }
    });

    jsStudioTopSlider.on("changed.owl.carousel", function (property) {
      var current = property.item.index;
      var src = $(property.target)
        .find(".owl-item")
        .eq(current)
        .find("img")
        .attr("src");
      // // console.log("Image current is " + src);
      $(".js-studio-owl-active-big-thumb").attr("src", src);
    });

    $(".js-studio-slider-prev-btn").on("click", function () {
      jsStudioTopSlider.trigger("prev.owl.carousel", [300]);
    });
    $(".js-studio-slider-next-btn").on("click", function () {
      jsStudioTopSlider.trigger("next.owl.carousel");
    });

    $(".js-booking-ellipse-slider").owlCarousel({
      items: 1,
      smartSpeed: 500,
      navSpeed: 1000,
      autoplayHoverPause: true,
      dots: false,
      nav: true,
      animateOut: "fadeOut",
      navText: [
        '<svg xmlns="http://www.w3.org/2000/svg" width="12" height="43"><path fill-rule="evenodd" opacity=".122" d="M12.006 42.993L1.72 21.501 12.006.007h-1.725L-.006 21.501l10.287 21.492h1.725z"/></svg>',
        '<svg xmlns="http://www.w3.org/2000/svg" width="12" height="43"><path fill-rule="evenodd" opacity=".122" d="M-.006 42.993L10.28 21.501-.006.007h1.725l10.287 21.494L1.719 42.993H-.006z"/></svg>',
      ],
    });

    // ScrollReveal().reveal('.feature', {
    //     duration: 1500,
    //     distance: '100px',
    //     easing: 'cubic-bezier(0.5, -0.01, 0, 1.005)',
    //     origin: 'bottom',
    //     interval: 600
    // });

    /* inits */
    // getSetInstaHeroGallery();

    $(".js-insta-view-more-btn").click(function (e) {
      e.preventDefault();
      getSetInstaHeroGallery();
    });
    $(".js-view-all-review-btn").click(function (e) {
      e.preventDefault();
      $(".js-review-wrapper").removeClass("d-none");
    });

    /* /inits */

    /* mobile menu functions */
    $(".js-mobile-menu-wrapper ul>li a:not(:only-child)").each(function () {
      if ($(this).attr("href") !== "#") {
        let clone = $(this).clone();
        let subParent = $(this).parent().find("ul.sub-menu");
        subParent.prepend("<li></li>");
        subParent.find("li:eq(0)").append(clone);
        $(this).prop("href", "#");
      }
    });
    $(".js-mobile-menu-wrapper ul>li a:not(:only-child)").on("click", function (
      e
    ) {
      e.preventDefault();
      let parent = $(this).parent();
      if (!parent.hasClass("active-sub")) {
        parent.addClass("active-sub");
        parent.find("ul:eq(0)").slideDown();
      } else if (parent.hasClass("active-sub")) {
        parent.removeClass("active-sub");
        parent.find("ul:eq(0)").slideUp();
      }
    });
    $(".js-mobile-menu-toggle").on("click", function (e) {
      e.preventDefault();
      let $this = $(this);
      let $list = $(".js-nav-list");
      if (!$this.hasClass("active")) {
        $this.addClass("active");
        $list.slideDown();
      } else if ($this.hasClass("active")) {
        $this.removeClass("active");
        $list.slideUp();
      }
    });
    /* /mobile menu functions */

    $("body").on("click", ".js-show-message-bubble", function () {
      let el = $(this);
      let parent = el.closest(".js-message-bubble-wrapper");
      parent.addClass("active");
    });
    $("body").on("click", ".js-hide-message-bubble", function () {
      let el = $(this);
      let parent = el.closest(".js-message-bubble-wrapper");
      parent.removeClass("active");
    });
  });
})(jQuery);

(function () {
  "use strict";
  $(document).ready(function () {
    let el = $(".js-fixed-float-book-btn-wrapper");
    if (el) {
      $(window).on("scroll", function () {
        let scrollSize = $(window).scrollTop();
        if (scrollSize >= 342) {
          el.addClass("active");
        } else {
          el.removeClass("active");
        }
      });
    }
  });
})();
(function () {
  "use strict";
  $(document).ready(function () {
    let el = $(".js-fixed-float-book-now-btn-wrapper");
    if (el) {
      $(window).on("scroll", function () {
        let scrollSize = $(window).scrollTop();
        if (scrollSize >= 342) {
          el.addClass("active");
        } else {
          el.removeClass("active");
        }
      });
    }
  });
})();

(function () {
  window.toastAlert = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener("mouseenter", Swal.stopTimer);
      toast.addEventListener("mouseleave", Swal.resumeTimer);
    },
  });
  /*   Swal.mixin({
    showClass: {
      popup: "animate__animated animate__fadeInDown",
    },
    hideClass: {
      popup: "animate__animated animate__fadeOutUp",
    },
  }); */

  /* toastAlert.fire({
    icon: "success",
    title: "Signed in successfully",
  }); */
})();
/* (function() {
  "use strict";
  $(document).ready(function() {
    let el = $(".js-bucket-menu");
    let header = $(".site-header");
    let wpAdminHeader = $("#wpadminbar");

    if (el) {
      $(window).on("scroll", function() {
        let scrollSize = $(window).scrollTop();
        if (scrollSize >= header.height()) {
          el.addClass("sticky");
          if (wpAdminHeader) {
            el.css('top',wpAdminHeader.height()+'px');
          }
        } else {
          el.removeClass("sticky");
           el.css('top',0);
        }
      });
    }
  });
})(); */

/* function getSetInstaHeroGallery() {
  let instaLoadBtn = $(".js-insta-view-more-btn");
  instaLoadBtn.find(".fa").toggle();
  let instaParent = $(".js-insta-gallery-wrapper");
  let instaAfter = instaParent.attr("data-next-page");
  instaAfter = parseInt(instaAfter);
  let max = 6;
  let LoopMax = instaAfter + max;
  // let instaAPIURI = 'https://instagram.com/graphql/query/?query_id=226457155&variables={"id":"10145435650","first":6}';
  
  // https://www.instagram.com/graphql/query/?query_hash=298b92c8d7cad703f7565aa892ede943&variables={%22tag_name%22:%22linenandlovestudios%22,%22first%22:12,%22after%22:%22QVFEUVJvOTdCZHA4ajdtQURNYzRQNjE2RUdJS2t2ajJZTEQ0LWJyb0xwN3ZrSTZ4WkVDRS1sTFFhTExQbU1pVmtsRk5qS2pkWl84V0JBRDZFRU81TmFfNg==%22}

  // let instaAPIURI = 'https://www.instagram.com/amazing.architecture/?__a=1';
  // let instaAPIURI =
  //   "https://www.instagram.com/explore/tags/linenandlovestudios/?__a=1";
  let instaAPIURI =
    "https://www.instagram.com/graphql/query/?query_hash=298b92c8d7cad703f7565aa892ede943&variables={%22tag_name%22:%22linenandlovestudios%22,%22first%22:400}";
  if (typeof window.instaMediaData == "undefined") {
    $.ajax({
      type: "get",
      url: instaAPIURI,
      success: function(res) {
        console.log(res);
        window.instaMediaData =
          typeof res.graphql.hashtag.edge_hashtag_to_media !== "undefined"
            ? res.graphql.hashtag.edge_hashtag_to_media
            : [];
        let instaEdges = window.instaMediaData.edges;

        for (let i = instaAfter; i < LoopMax; i++) {
          instaParent.append(
            '<div loading="lazy" class="img-wrapper col-md-4 col-4"><a href="https://www.instagram.com/p/' +
              instaEdges[i].node.shortcode +
              '/" target="_blank" rel="noopener noreferrer"><div class="img-bg" style="background: url(' +
              instaEdges[i].node.thumbnail_src +
              ')"></div> <img loading="lazy" src="' +
              instaEdges[i].node.thumbnail_src +
              '" alt="" /> </a></div>'
          );
        }
        instaParent.attr("data-next-page", instaAfter + max);

        instaLoadBtn.find(".fa").toggle();
      }
    });
  }
  if (typeof window.instaMediaData !== "undefined") {
    let instaEdges = window.instaMediaData.edges;

    for (let i = instaAfter; i < LoopMax; i++) {
      instaParent.append(
        '<div class="img-wrapper col-md-4 col-4"><a href="https://www.instagram.com/p/' +
          instaEdges[i].node.shortcode +
          '/" target="_blank" rel="noopener noreferrer"><div class="img-bg" style="background: url(' +
          instaEdges[i].node.thumbnail_src +
          ')"></div> <img src="' +
          instaEdges[i].node.thumbnail_src +
          '" alt="" /> </a></div>'
      );
    }
    instaParent.attr("data-next-page", instaAfter + max);
    instaLoadBtn.find(".fa").toggle();
  }
}
 */

function getSetInstaHeroGallery() {
  getSetInstaGallery();
}

function getSetInstaGallery(params) {
  let gallery = window.localStorage.getItem("insta_gallery");

  let instaParent = $(".js-insta-gallery-wrapper");
  let instaLoadBtn = $(".js-insta-view-more-btn");
  instaLoadBtn.find(".fa").toggle();

  if (!gallery) {
    axios({
      method: "GET",
      url: bookingApiObj.url,
      params: {
        action: "get_ig_gallery",
        security: bookingApiObj.security,
      },
    })
      .then((res) => {
        if (res.data) {
          window.localStorage.setItem("insta_gallery", JSON.stringify(res.data));
          getSetInstaGallery();
        }
        instaLoadBtn.find(".fa").toggle();

        // this.isAppLoading = false;
      })
      .catch((err) => {
        // this.isAppLoading = false;
        console.log(err);
        alert("Oops! something went wrong please try again");
      });

    return;
  }
  gallery = JSON.parse(gallery);
  let limit = 8;

  let instaAfter = instaParent.data("next-page")
    ? instaParent.data("next-page")
    : 0;

  let limitNow = (instaAfter + 1) * limit;
  for (let i = instaAfter * limit; i < limitNow; i++) {
    let edge = gallery[i];
    instaParent.append(
      '<div loading="lazy" class="img-wrapper col-md-3 col-3" style="padding-top: 25% !important;"><a href="' +
        edge.url +
        '" target="_blank" rel="noopener noreferrer"><div class="img-bg" style="background: url(' +
        edge.displayUrl +
        ')"></div> <img loading="lazy" src="' +
        edge.displayUrl +
        '" alt="" /> </a></div>'
    );
  }
  instaParent.data("next-page", instaAfter + 1);
  instaLoadBtn.find(".fa").toggle();
}

/* function getSetInstaHeroGallery() {
  
  let instaAPIURI = "https://api.instagram.com/v1/users/self/media/recent/?access_token=13611218248.1677ed0.8d14d939cdad4d51aad636e5e6c30a72&count=9";
    
  let instaLoadBtn = $(".js-insta-view-more-btn");
  instaLoadBtn.find(".fa").toggle();
  let instaParent = $(".js-insta-gallery-wrapper");
  let instaAfter =
    instaParent.data("next-page") == null || instaParent.data("next-page") == 0
      ? null
      : instaParent.data("next-page");
  // let instaVariables = '{"tag_name":"linenandlovestudios","first":4}';
  if (instaAfter) {
    // instaVariables =
    //   '{"tag_name":"linenandlovestudios","first":4,"after":' + instaAfter + "}";
    instaAPIURI = instaAfter;

  }
  // let instaAPIURI =
  //   "https://www.instagram.com/graphql/query/?query_hash=298b92c8d7cad703f7565aa892ede943&variables=" +
  //   instaVariables;

  $.ajax({
    type:"get",
    url:instaAPIURI,
    success: function(res) {
      // console.log(res);
      if (res.meta.code == 200) {
        let instaEdges = res.data;
        instaEdges.forEach(edge => {
          instaParent.append(
            '<div loading="lazy" class="img-wrapper col-md-3 col-3" style="padding-top: 25% !important;"><a href="' +
              edge.link +
              '" target="_blank" rel="noopener noreferrer"><div class="img-bg" style="background: url(' +
              edge.images.low_resolution.url +
              ')"></div> <img loading="lazy" src="' +
              edge.images.low_resolution.url +
              '" alt="" /> </a></div>'
          );
        });
        instaParent.data(
          "next-page",
          res.pagination.next_url
        );
        // console.log(res.pagination.next_url);
      }
      instaLoadBtn.find(".fa").toggle();
    }
  });
} */

function loadAllTooLazy() {
  // document.addEventListener('DOMContentLoaded', function() {
  var lazySrcs = [].slice.call(document.querySelectorAll(".lazy-load"));
  if ("IntersectionObserver" in window) {
    let lazySrcObserver = new IntersectionObserver(function (
      entries,
      observer
    ) {
      entries.forEach(function (entry) {
        if (entry.isIntersecting) {
          let lazySrc = entry.target;
          lazySrc.src = lazySrc.dataset.src;
          lazySrc.srcset = lazySrc.dataset.srcset;
          lazySrc.classList.remove("lazy-load");
          lazySrcObserver.unobserve(lazySrc);
        }
      });
    });
    lazySrcs.forEach(function (lazySrc) {
      lazySrcObserver.observe(lazySrc);
    });
  } else {
    // Possibly fall back to a more compatible method here
  }
  // });
}

function updateHeaderCartBtn() {
  let cartEl = $(".js-cart-link");
  let cartBadge = cartEl.find(".js-cart-badge");
  let cartData = getBookingCart();
  if (!getBookingCart() || Object.keys(cartData.rooms).length < 1) {
    cartEl.addClass("d-none");
  }
  if (cartData && cartData.rooms && Object.keys(cartData.rooms).length > 0) {
    cartEl.removeClass("d-none");
    cartBadge.text(Object.keys(cartData.rooms).length);
  }
}
