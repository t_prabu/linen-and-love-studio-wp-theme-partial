function calendarAppInit() {
  if (!document.querySelector("#calendar-app")) {
    return;
  }
  new Vue({
    el: "#calendar-app",
    data: {
      test: "test str",
      isAppLoading: false,
      current_month: null,
      picker_date: new Date(),
      selected_date: this.moment().format("YYYY-MM-DD"),
      current_month_dates: [],
      calendar_start_date: null,
      calendar_end_date: null,
      rooms_info: null,
      bookings_info: [],
      room_booking_info: null,
      weekdays: [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
        "Sunday",
      ],
      colors: [
        "#903D83",
        "#F39531",
        "#408D3B",
        "#40806A",
        "#9F6B3F",
        "#7462E0",
        "#9B59B6",
        "#9B59B6",
      ],
    },
    mounted() {
      this.selected_date = this.moment().format("YYYY-MM-DD");
      this.current_month = this.moment(
        this.selected_date,
        "YYYY-MM-DD"
      ).month();
    },
    computed: {
      trans_rooms_info() {
        let tempObj = this.rooms_info;
        if (tempObj) {
          let keysArr = Object.keys(tempObj);
          keysArr.forEach((item, i) => {
            if (tempObj.hasOwnProperty(item)) {
              tempObj[item].color = this.colors[i];
            }
          });
        }
        /* for (const key in tempObj) {
          if (tempObj.hasOwnProperty(key)) {
            tempObj[key].color = this.colors[parseInt(key)];
          }
        } */
        return tempObj;
      },
    },
    watch: {
      current_month() {
        this.set_current_month_dates();
      },
    },
    methods: {
      moment: window.moment,
      bg_color_style_attr(hex) {
        return `background-color:${hex};`;
      },

      activeId(date) {
        let id = "";
        if (date.isSame(moment(), "day")) {
          id = "todays-date";
        }
        return id;
      },
      compileEventHtml(room_obj_data, date) {
        let room_obj = room_obj_data;
        let html = "";
        for (const key in room_obj) {
          if (room_obj.hasOwnProperty(key)) {
            let this_room_obj_data = room_obj[key];
            let room = this.rooms_info[key];
            let slots = [];
            for (const key in this_room_obj_data) {
              if (this_room_obj_data.hasOwnProperty(key)) {
                slots.push(this_room_obj_data[key]);
              }
            }
            let thisData = slots[0];
            let tempBookingId = thisData.booking_id;
            let timeLineArr = [{ hrs: 0, from_at: null, to_at: null }];
            let booking = null;
            if (thisData.booking_id) {
              booking = this.bookings_info[thisData.booking_id];
            }
            slots.forEach((booked_time, i) => {
              if (i < 1) {
                booked_time.time_from;
                timeLineArr[0].hrs = 1;
                timeLineArr[0].from_at = booked_time.time_from;
                timeLineArr[0].to_at = booked_time.time_to;
              } else {
                let a_time = moment(
                  booked_time.time_from,
                  "YYYY-MM-DD hh:mm A"
                );
                let b_time = moment(
                  slots[i - 1].time_from,
                  "YYYY-MM-DD hh:mm A"
                );
                let theDiff = a_time.diff(b_time, "hours");
                // console.log(theDiff);
                if (theDiff == 1) {
                  timeLineArr[0].hrs += 1;
                  timeLineArr[0].id = booked_time.id;
                  timeLineArr[0].to_at = booked_time.time_to;
                } else {
                  if (theDiff > 0) {
                    timeLineArr.push({
                      hrs: 1,
                      id: booked_time.id,
                      from_at: booked_time.time_from,
                      to_at: booked_time.time_to,
                    });
                  }
                }
              }
            });
            timeLineArr.forEach((el) => {
              let tempHtml = `<div class="text-white event-wrapper" style="background-color:${
                room.color
              };">
        <div class="muted small">
           <i class="fa fa-clock-o" aria-hidden="true"></i> ${
             el.hrs
           } (Hrs) - ${this.moment(el.from_at, "YYYY-MM-DD hh:mm A").format(
                "hh:mm A"
              )} - ${this.moment(el.to_at, "YYYY-MM-DD hh:mm A").format(
                "hh:mm A"
              )}
        </div>`;

              if (booking) {
                tempHtml += `<div>
            <div>
                <i class="fa fa-user" aria-hidden="true"></i> ${
                  booking.user_name
                }
            </div>
            <div>
                <i class="fa fa-phone" aria-hidden="true"></i> <a class="text-white" href="tel:+1${
                  booking.user_phone
                }">${booking.user_phone}</a>
            </div>
            <div class="muted small">
                <i class="fa fa-camera" aria-hidden="true"></i> ${room.page.post_title.replace(
                  "Book ",
                  ""
                )} |  <a class="text-white" href="${base_url}/invoice/${
                  booking.invoice_id
                }" target="_blank">Invoice# ${
                  booking.invoice_id
                } <i class="fa fa-link" aria-hidden="true"></i></a>
            </div>
        </div>`;
              } else {
                tempHtml += `<div>
            <div>
                <i class="fa fa-user" aria-hidden="true"></i> Reserved Manually
            </div>
            <div class="muted small">
                <i class="fa fa-camera" aria-hidden="true"></i> ${room.page.post_title.replace(
                  "Book ",
                  ""
                )}
            </div>
        </div>`;
              }
              tempHtml += `</div>`;
              html = html + tempHtml;
            });
          }
        }
        return html;
      },

      getBookedByMonthly() {
        this.rooms_info = [];
        this.bookings_info = null;
        this.room_booking_info = null;
        axios({
          method: "GET",
          url: bookingApiObj.url,
          params: {
            action: "get_booked_by_monthly",
            from_at: this.calendar_start_date.format("YYYY-MM-DD"),
            to_at: this.calendar_end_date.format("YYYY-MM-DD"),
          },
        })
          .then((res) => {
            //   this.roomsData = res.data.data;
            if (res.data.data) {
              this.rooms_info = res.data.data.rooms_info;
              this.bookings_info = res.data.data.bookings_info;
              this.room_booking_info = res.data.data.room_booking_info;
            }
          })
          .catch((err) => {
            console.log(err);
            this.isAppLoading = false;
            alert("Oops! something went wrong please try again");
          });
      },
      setNow(date) {
        this.selected_date = this.moment(date).format("YYYY-MM-DD");
        this.current_month = this.moment(
          this.selected_date,
          "YYYY-MM-DD"
        ).month();
      },
      changeMonth(num) {
        this.current_month = this.current_month + num;
        this.selected_date = this.moment(this.selected_date, "YYYY-MM-DD")
          .month(this.current_month)
          .format("YYYY-MM-DD");
      },
      set_current_month_dates() {
        let start_day_offset = this.moment(
          this.moment().month(this.current_month).date(1)
        ).day();

        if (start_day_offset == 0) {
          start_day_offset = 7;
        }

        let tempArr = [];
        let dateInc = -1;
        let totDays = 41;

        let days_in_month =
          this.moment().month(this.current_month).daysInMonth() + 1;

        totDays = days_in_month + start_day_offset;
        start_day_offset = 2 - start_day_offset;
        dateInc = start_day_offset;

        for (dateInc; dateInc < days_in_month; dateInc++) {
          /*  let thisMoment = this.moment()
          .month(this.current_month)
          .date(dateInc); */
          /* let thisMoment = this.moment(this.moment()
          .month(this.current_month)).date(dateInc); */
          let thisMoment = this.moment()
            .month(this.current_month)
            .date(dateInc);

          tempArr.push(thisMoment);
        }

        this.current_month_dates = tempArr;
        this.calendar_start_date = tempArr[0];
        this.calendar_end_date = tempArr[tempArr.length - 1];
        this.getBookedByMonthly();
      },
    },
  });
}
calendarAppInit();
