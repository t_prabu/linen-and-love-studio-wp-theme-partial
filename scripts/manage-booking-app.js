function bookingManagementAppInit() {
  if (!document.querySelector("#booking-management-app")) {
    return;
  }
  new Vue({
    el: "#booking-management-app",
    data: {
      indexData: null,
      current_page: 1,
      per_page: 10,
      total_pages: 1,
      isAppLoading: true,
      selectedRoom: ""
    },
    mounted() {
      this.getIndex();
    },
    computed: {
      total_pages_arr() {
        let tempArr = [];
        for (let index = 0; index < this.total_pages; index++) {
          tempArr.push(index + 1);
        }
        return tempArr;
      }
    },
    methods: {
      moment: window.moment,
      filterReset() {
        this.current_page = 1;
        this.selectedRoom = "";
        this.getIndex();
      },
      filterRoom() {
        this.current_page = 1;
        this.getIndex();
      },

      approveInvoice(invoice_id) {
        /*  */
        this.isAppLoading = true;
        axios({
          method: "POST",
          url: bookingApiObj.url,
          params: {
            action: "manage_booking_api",
            security: bookingApiObj.security
          },
          data: {
            route: "approve",
            invoice_id: invoice_id
          }
        })
          .then(res => {
            this.isAppLoading = false;
            this.getIndex();
          })
          .catch(err => {
            this.isAppLoading = false;
            alert("Oops! something went wrong please try again");
          });
        /*  */
      },
      cancelInvoice(invoice_id) {
        /*  */
        this.isAppLoading = true;
        axios({
          method: "POST",
          url: bookingApiObj.url,
          params: {
            action: "manage_booking_api",
            security: bookingApiObj.security
          },
          data: {
            route: "cancel",
            invoice_id: invoice_id
          }
        })
          .then(res => {
            this.isAppLoading = false;
            this.getIndex();
          })
          .catch(err => {
            this.isAppLoading = false;
            alert("Oops! something went wrong please try again");
          });
        /*  */
      },
      changePage(int) {
        this.current_page = int;
        this.getIndex();
      },
      invoiceUrl(id) {
        return base_url + "/invoice/" + id;
      },
      invoicePaymentType(obj) {
        let status = obj ? JSON.parse(obj) : { type: "gateway" };
        let str = "Gateway";

        if (status.type) {
          if (status.type == "manual") {
            str = "Manual";
          }
        }
        return str;
      },
      invoicePaymentStatus(status) {
        let str = `<span class="text-danger">Declined</span>`;
        switch (status) {
          case "approved":
            str = `<span class="text-success">Approved</span>`;
            break;
          case "payment_pending":
            str = `<span class="text-warning">Payment Pending</span>`;;
            break;
        }
        return str;
      },
      handleIndexData(res) {
        this.indexData = res.data.data;
        this.total_pages = Math.ceil(this.indexData.total / this.per_page);

        // console.log(res.data.data);
      },
      getIndex() {
        /*  */
        this.isAppLoading = true;
        axios({
          method: "POST",
          url: bookingApiObj.url,

          params: {
            action: "manage_booking_api",
            security: bookingApiObj.security
          },
          data: {
            route: "index",
            per_page: this.per_page,
            page: this.current_page,
            page_id: this.selectedRoom
          }
        })
          .then(res => {
            this.isAppLoading = false;
            this.handleIndexData(res);
          })
          .catch(err => {
            this.isAppLoading = false;
            alert("Oops! something went wrong please try again");
          });
        /*  */
      }
    }
  });
}
bookingManagementAppInit();
