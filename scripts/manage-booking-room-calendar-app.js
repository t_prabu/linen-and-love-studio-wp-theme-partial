Vue.component("time-sub-slot-wrapper-manage", {
  props: {
    this_data: {
      default: function () {
        return {};
      },
    },
  },
  data() {
    return {
      isAppLoading: false,
      time_slot_data: null,
      is_render: true,
    };
  },
  mounted() {
    this.time_slot_data = this.this_data;
  },
  watch: {
    this_data() {
      this.time_slot_data = this.this_data;
    },
  },
  methods: {
    reRenderSlot() {
      this.is_render = false;
      console.log(this.is_render);
      this.$nextTick(() => {
        this.is_render = true;
      });
    },
    updateTimeSlot(status, index) {
      let reserved = false;
      if (status == "reserved") {
        reserved = true;
      }
      if (status == "canceled") {
        reserved = false;
      }

      /*  */
      this.isAppLoading = true;
      axios({
        method: "POST",
        url: bookingApiObj.url,
        params: {
          action: "manage_booking_api",
          security: bookingApiObj.security,
        },
        data: {
          route: "update-time-slot-status",
          data: {
            index: index,
            booked_date: moment(
              this.time_slot_data.dateTime,
              "YYYY-MM-DD hh:mm A"
            ).format("YYYY-MM-DD"),
            /* after booking created */
            booking_id: null,
            room_id: this.time_slot_data.dateTime.room_id,
            time_from: moment(
              this.time_slot_data.dateTime,
              "YYYY-MM-DD hh:mm A"
            ).format("YYYY-MM-DD HH:mm:ss"),
            time_to: moment(this.time_slot_data.dateTime, "YYYY-MM-DD hh:mm A")
              .add(1, "hour")
              .format("YYYY-MM-DD HH:mm:ss"),
            price: this.time_slot_data.price,
            reserved: reserved,
            room_id: this.time_slot_data.room_id,
          },
        },
      })
        .then((res) => {
          this.isAppLoading = false;
        })
        .catch((err) => {
          this.isAppLoading = false;
          alert("Oops! something went wrong please try again");
        });
      /*  */
    },
  },
  template: "#time-sub-slot-wrapper-manage-template",
});
Vue.component("time-slot-wrapper-manage", {
  data() {
    return {};
  },
  props: {
    date: {
      default: "",
    },

    roomTimeSlots: {
      default: function () {
        return [];
      },
    },
    bookedTimeSlots: {
      default: function () {
        return [];
      },
    },
  },
  computed: {
    dailyTimeSlots() {
      if (!this.roomTimeSlots || !this.bookedTimeSlots) {
        return [];
      }
      let timeSlotsArr = [];
      let dayStr = moment(this.date).format("dddd").toLowerCase();
      if (this.roomTimeSlots && this.roomTimeSlots[dayStr]) {
        this.roomTimeSlots[dayStr].forEach((slot) => {
          let dateTimeArr = [];
          let date = this.date;
          let timeFrom = slot.time_from;
          let timeTo = slot.time_to;
          let timeNow = date + " " + timeFrom;
          dateTimeArr.push(timeNow);

          while (
            moment(moment(timeNow, "YYYY-MM-DD hh:mm A")).isBefore(
              moment(date + " " + timeTo, "YYYY-MM-DD hh:mm A")
            )
          ) {
            timeNow = moment(timeNow, "YYYY-MM-DD hh:mm A")
              .add(1, "hour")
              .format("YYYY-MM-DD hh:mm A");
            dateTimeArr.push(timeNow);
          }
          dateTimeArr.forEach((dateTime) => {
            let reserved = false;
            let in_the_past = false;
            let reserved_count = 0;
            let slot_max_reservations = parseInt(
              this.$root.roomInfo.slot_max_reservations
            );

            if (this.bookedTimeSlots && this.bookedTimeSlots[this.date]) {
              this.bookedTimeSlots[this.date].forEach((bookedTimeSlot) => {
                if (
                  moment(
                    moment(bookedTimeSlot.time_from, "YYYY-MM-DD HH:mm:ss")
                  ).isSame(moment(dateTime, "YYYY-MM-DD hh:mm A"))
                ) {
                  reserved = true;
                  reserved_count++;
                }
              });
            }

            let tmp_arr = [];

            for (let i = 0; i < slot_max_reservations; i++) {
              tmp_arr.push({ index: i, reserved: false });
            }

            for (let i = 0; i < reserved_count; i++) {
              tmp_arr[i].reserved = true;
            }

            if (!moment(dateTime, "YYYY-MM-DD hh:mm A").isAfter(moment())) {
              reserved = true;
              in_the_past = true;
              /* for (let i = 0; i < slot_max_reservations; i++) {
                tmp_arr[i].reserved = true;
              } */
            }

            let tempObj = {
              dateTime: dateTime,
              time: moment(dateTime, "YYYY-MM-DD hh:mm A").format("hh:mm A"),
              price: slot.price,
              in_the_past: in_the_past,
              time_from: timeFrom,
              time_to: timeTo,
              name: slot.name,
              price: slot.price,
              room_id: slot.room_id,
              reserved: reserved,
              fillable_slots: tmp_arr,
            };
            timeSlotsArr.push(JSON.parse(JSON.stringify(tempObj)));
          });
        });
      }
      return timeSlotsArr;
    },
  },
  updated() {},
  methods: {
    passDatatoParent(data) {},
  },
  template: `<div><time-sub-slot-wrapper-manage @pass-data-to-parent="passDatatoParent" v-for="(timeSlot, index) in dailyTimeSlots" :key="index" :this_data="timeSlot"></time-sub-slot-wrapper-manage></div>`,
});
function bookingManagementRoomCalendarAppInit() {
  if (!document.querySelector("#booking-management-room-calendar-app")) {
    return;
  }

  new Vue({
    el: "#booking-management-room-calendar-app",
    data: {
      isAppLoading: true,
      isInitDataLoaded: false,
      selectedRoom: "",
      roomInfo: {},
      roomTimeSlots: [],
      roomThisWeekBookedTimeSlots: [],
      calendar: {
        pickerDate: new Date(),
        isoWeekDays: [
          "monday",
          "tuesday",
          "wednesday",
          "thursday",
          "friday",
          "saturday",
          "sunday",
        ],
        isoWeekDaysInt: [1, 2, 3, 4, 5, 6, 7],
        startMoment: moment().isoWeekday(1),
        thisWeek: moment().isoWeekday(1).week(),
        currentWeek: moment().week(),
      },
    },
    computed: {
      weekDates() {
        let thisWeekDates = [];
        this.calendar.isoWeekDaysInt.forEach((day) => {
          thisWeekDates.push(
            moment()
              .week(this.calendar.currentWeek)
              .isoWeekday(day)
              .format("YYYY-MM-DD")
          );
        });
        return thisWeekDates;
      },
      weekStartDate() {
        return moment()
          .week(this.calendar.currentWeek)
          .isoWeekday(1)
          .subtract(1, "days")
          .format("YYYY-MM-DD");
      },
      weekEndDate() {
        return moment()
          .week(this.calendar.currentWeek)
          .isoWeekday(7)
          .add(1, "days")
          .format("YYYY-MM-DD");
      },
    },
    mounted() {
      this.isAppLoading = false;
      // Add a request interceptor
    },

    methods: {
      selectedRoomChange() {
        // console.log(this.selectedRoom);
        if (this.selectedRoom) {
          this.calendar.currentWeek = moment().week();
          this.roomTimeSlots = [];
          this.roomThisWeekBookedTimeSlots = [];
          this.getRoomInfo();
        }
      },
      setNow(date) {
        // console.log(date.toString());
        // console.log(date.diff());
        // console.log(moment(date).diff(moment().week(1),'week'));
        let weekDiff = moment(date).diff(moment().week(1), "week") + 1;
        console.log(weekDiff);
        console.log(moment(date).week());
        // this.calendar.currentWeek = moment(date).week();
        this.calendar.currentWeek = weekDiff;
        this.getRoomInfo();
      },
      getRoomInfo() {
        this.isInitDataLoaded = false;
        this.isAppLoading = true;
        axios({
          method: "GET",
          url: bookingApiObj.url,
          params: {
            action: "get_booking_page_info",
            security: bookingApiObj.security,
            page_id: this.selectedRoom,
            bookings_from: this.weekStartDate,
            bookings_to: this.weekEndDate,
          },
        })
          .then((res) => {
            this.roomInfo = res.data.data.room_info;
            this.roomTimeSlots = res.data.data.room_time_slots;
            this.roomThisWeekBookedTimeSlots = res.data.data.room_booking_info;
            this.isAppLoading = false;
            this.isInitDataLoaded = true;
          })
          .catch((err) => {
            this.isAppLoading = false;
            alert("Oops! something went wrong please try again");
          });
      },
      setCalendarPreviousWeek() {
        this.calendar.currentWeek -= 1;
        // console.log(this.calendar.currentWeek);
        this.getRoomInfo();
      },
      setCalendarNextWeek() {
        this.calendar.currentWeek += 1;
        // console.log(this.calendar.currentWeek);
        this.getRoomInfo();
      },
    },
  });
}
bookingManagementRoomCalendarAppInit();
