Vue.component("daily-wrapper", {
  props: {
    roomInfo: {
      default: null,
    },
    date: {
      default: "2019-01-01",
    },
  },
  data() {
    return {
      // dayShortName: '',
      // date: '',
    };
  },
  template: `<div class="daily-wrapper">
					<div class="day-label"><strong>{{dayShort}}</strong> {{dateShort}}</div>
					<div class="time-slots-wrapper">
						<slot></slot>
					</div>
				</div>`,
  computed: {
    dayShort() {
      return moment(this.date).format("ddd").toUpperCase();
    },
    dateShort() {
      return moment(this.date).format("MMM D").toUpperCase();
    },
    // notPastDate() {
    // 	return moment(this.date).isSameOrAfter(moment().format('YYYY-MM-DD'));
    // }
  },
  methods: {},
});

Vue.component("time-sub-slot-wrapper", {
  props: {
    roomInfo: {
      default: null,
    },
    data: {
      default: function () {
        return {};
      },
    },
  },
  data() {
    return {};
  },
  methods: {
    sendTimeSlotToCart(timeSlot) {
      if (timeSlot.reserved) {
        return;
      }
      if (
        moment(moment().format("YYYY-MM-DD")).isSame(
          moment(timeSlot.dateTime, "YYYY-MM-DD hh:mm A").format("YYYY-MM-DD")
        )
      ) {
        Swal.fire({
          showClass: { popup: "animate__animated animate__fadeInDown" },
          hideClass: { popup: "animate__animated animate__fadeOutUp" },
          // timer: 3000,
          type: "warning",
          title: "Same day booking not available",
          html: `<div>Please contact us over the phone to for same day booking <a href="tel:+18005257452">905-604-0221</a>.</div>`,
        });
        return;
      }
      if (this.data.addedToCart) {
        this.data.addedToCart = false;
        this.$emit("pass-data-to-parent", timeSlot);
      } else if (!this.data.addedToCart) {
        this.data.addedToCart = true;
        this.$emit("pass-data-to-parent", timeSlot);
      }
    },
  },
  template: `<div  class="time-slot-wrapper" :class="{reserved: data.reserved, 'added-to-cart': data.addedToCart}"  @click="sendTimeSlotToCart(data)">{{data.time}}</div>`,
});

Vue.component("time-slot-wrapper", {
  data() {
    return {};
  },
  props: {
    roomInfo: {
      default: null,
    },
    room_id: {
      default: null,
    },
    room_name: {
      default: null,
    },
    date: {
      default: "",
    },
    cart: {
      default: function () {
        return [];
      },
    },
    roomTimeSlots: {
      default: function () {
        return [];
      },
    },
    bookedTimeSlots: {
      default: function () {
        return [];
      },
    },
  },
  computed: {
    dailyTimeSlots() {
      if (!this.roomTimeSlots || !this.bookedTimeSlots) {
        return [];
      }

      let timeSlotsArr = [];
      let dayStr = moment(this.date).format("dddd").toLowerCase();
      if (this.roomTimeSlots && this.roomTimeSlots[dayStr]) {
        this.roomTimeSlots[dayStr].forEach((slot) => {
          let dateTimeArr = [];
          let date = this.date;
          let timeFrom = slot.time_from;
          let timeTo = slot.time_to;
          let timeNow = date + " " + timeFrom;

          if (
            !moment(date + " " + timeFrom, "YYYY-MM-DD hh:mm A").isBetween(
              moment(date + " " + "7:59 AM", "YYYY-MM-DD hh:mm A"),
              moment(date + " " + "8:01 PM", "YYYY-MM-DD hh:mm A")
            )
          ) {
            return false;
          }

          dateTimeArr.push(timeNow);

          while (
            moment(moment(timeNow, "YYYY-MM-DD hh:mm A")).isBefore(
              moment(date + " " + timeTo, "YYYY-MM-DD hh:mm A")
            )
          ) {
            timeNow = moment(timeNow, "YYYY-MM-DD hh:mm A")
              .add(1, "hour")
              .format("YYYY-MM-DD hh:mm A");
            dateTimeArr.push(timeNow);
          }
          dateTimeArr.forEach((dateTime) => {
            let reserved = false;
            let reserved_count = 0;
            let slot_max_reservations = parseInt(
              this.roomInfo.slot_max_reservations
            );

            if (this.bookedTimeSlots && this.bookedTimeSlots[this.date]) {
              this.bookedTimeSlots[this.date].forEach((bookedTimeSlot) => {
                if (
                  moment(
                    moment(bookedTimeSlot.time_from, "YYYY-MM-DD HH:mm:ss")
                  ).isSame(moment(dateTime, "YYYY-MM-DD hh:mm A"))
                ) {
                  reserved = true;
                  reserved_count++;
                }
              });
            }
            // console.log(reserved_count);
            // console.log(this.roomInfo);
            if (this.roomInfo.type == "makeup_room") {
              reserved = reserved_count >= slot_max_reservations;
            }

            if (!moment(dateTime, "YYYY-MM-DD hh:mm A").isAfter(moment())) {
              reserved = true;
            }

            let addedToCart = false;
            if (this.cart) {
              this.cart.forEach((item) => {
                if (
                  item.dateTime == dateTime &&
                  this.roomInfo.id == item.room_id
                ) {
                  // if (item.dateTime == dateTime) {
                  addedToCart = true;
                }
              });
            }

            let tempObj = {
              dateTime: dateTime,
              time: moment(dateTime, "YYYY-MM-DD hh:mm A").format("hh:mm A"),
              price: slot.price,
              reserved: reserved,
              room_name: this.room_name,
              room_type: this.roomInfo.type,
              room_id: this.room_id,
              addedToCart: addedToCart,
            };
            timeSlotsArr.push(JSON.parse(JSON.stringify(tempObj)));
          });
        });
      }
      return timeSlotsArr;
    },
  },
  updated() {},
  methods: {
    passDatatoParent(data) {
      this.$emit("add-item-to-cart", data);
    },
  },
  template: `<div><time-sub-slot-wrapper @pass-data-to-parent="passDatatoParent" v-for="(timeSlot, index) in dailyTimeSlots" :key="index" :data="timeSlot" :room-info="roomInfo" ></time-sub-slot-wrapper></div>`,
});

Vue.component("cart-wrapper", {
  props: {
    cart: {
      default: "",
    },
    rooms_data: {
      default: null,
    },
  },
  data() {
    return {
      promo_code: null,
      promo_code_invalid: false,
      promo_code_validated: false,
      promo_code_data: null,
    };
  },
  computed: {
    validatePromoCode() {
      let promo_data = this.promo_code_data;
      return (
        promo_data &&
        promo_data.is_active == 1 &&
        moment(moment()).isBetween(
          moment(promo_data.from_at, "YYYY-MM-DD HH:mm:ss"),
          moment(promo_data.to_at, "YYYY-MM-DD HH:mm:ss")
        ) &&
        parseInt(promo_data.use_count) <= parseInt(promo_data.max_use_count)
      );
    },
  },
  watch: {
    validatePromoCode() {
      if (this.validatePromoCode === true) {
        this.$emit("add-promo-code", this.promo_code_data);
        this.promo_code_invalid = false;
      }
      if (this.validatePromoCode === false) {
        this.$emit("remove-promo-code", null);
        this.promo_code_invalid = true;
      }
    },
  },
  template: "#cart-wrapper-template",

  methods: {
    validatePromoCodeData() {
      let promo_data = this.promo_code_data;
      /* console.log(promo_data);
        console.log(
          parseInt(promo_data.use_count) <= parseInt(promo_data.max_use_count)
        );
        console.log(
          moment(moment()).isBetween(
            moment(promo_data.from_at, "YYYY-MM-DD HH:mm:ss"),
            moment(promo_data.to_at, "YYYY-MM-DD HH:mm:ss")
          )
        ); */
      return (
        promo_data &&
        promo_data.is_active == 1 &&
        moment(moment()).isBetween(
          moment(promo_data.from_at, "YYYY-MM-DD HH:mm:ss"),
          moment(promo_data.to_at, "YYYY-MM-DD HH:mm:ss")
        ) &&
        parseInt(promo_data.use_count) <= parseInt(promo_data.max_use_count)
      );
    },
    applyPromoCode() {
      this.promo_code_data = null;
      this.$emit("remove-promo-code", null);
      if (!/LAL-[A-Z]{3}$/.test(this.promo_code)) {
        this.promo_code_invalid = true;
        return;
      }
      /*  */
      this.promo_code_invalid = false;
      this.$emit("on-is-app-loading", true);
      axios({
        method: "POST",
        url: bookingApiObj.url,
        params: {
          action: "manage_booking_coupon_api",
          security: bookingApiObj.security,
        },
        data: {
          route: "get_coupon_code",
          code: this.promo_code,
        },
      })
        .then((res) => {
          this.$emit("on-is-app-loading", false);
          if (res.data.data) {
            this.promo_code_data = res.data.data;
            this.validatePromoCodeData();
          } else {
            this.promo_code_data = null;
            this.promo_code_invalid = true;
          }
        })
        .catch((err) => {
          this.$emit("on-is-app-loading", false);
          console.log(err);
          this.showDanger("Oops! something went wrong please try again");
        });
      /*  */
    },

    removeFromCart(room_id, item_data) {
      this.$emit("remove-from-cart", room_id, item_data);
    },
    cartTotalBeforePromo(rooms_data, cart_rooms) {
      let tempVal = 0;
      for (const key in cart_rooms) {
        tempVal =
          tempVal +
          this.subtotalAfterDiscount(rooms_data[key], cart_rooms[key]);
      }

      // if (this.validatePromoCode) {
      //   tempVal =
      //     tempVal - (tempVal * this.promo_code_data.off_percent) / 100;
      // }

      return tempVal.toFixed(2);
    },
    cartTotal(rooms_data, cart_rooms) {
      let tempVal = 0;
      for (const key in cart_rooms) {
        tempVal =
          tempVal +
          this.subtotalAfterDiscount(rooms_data[key], cart_rooms[key]);
      }

      if (this.validatePromoCode) {
        tempVal = tempVal - (tempVal * this.promo_code_data.off_percent) / 100;
      }

      return tempVal.toFixed(2);
    },
    discountAmount(room_data, room_bookings) {
      let tempVal = 0;

      room_bookings.forEach((item) => {
        tempVal = tempVal + parseFloat(item.price);
      });
      // console.log(tempVal);
      if (this.eligibleForDiscount(room_data, room_bookings)) {
        tempVal =
          (tempVal *
            parseFloat(
              this.eligibleDiscount(room_data, room_bookings).percent
            )) /
          100;
      }

      return tempVal;
    },
    subtotal(room_data, room_bookings) {
      let tempVal = 0;

      room_bookings.forEach((item) => {
        tempVal = tempVal + parseFloat(item.price);
      });
      // console.log(tempVal);
      if (this.eligibleForDiscount(room_data, room_bookings)) {
        tempVal =
          tempVal -
          (tempVal *
            parseFloat(
              this.eligibleDiscount(room_data, room_bookings).percent
            )) /
            100;
      }
      // tempVal = tempVal + (tempVal * room_data.tax_percentage) / 100;
      return tempVal;
    },
    subtotalWithoutDiscount(room_data, room_bookings) {
      let tempVal = 0;

      room_bookings.forEach((item) => {
        tempVal = tempVal + parseFloat(item.price);
      });
      // console.log(tempVal);
      // if (this.eligibleForDiscount(room_data, room_bookings)) {
      //   tempVal =
      //     tempVal -
      //     (tempVal *
      //       parseFloat(
      //         this.eligibleDiscount(room_data, room_bookings).percent
      //       )) /
      //       100;
      // }
      // tempVal = tempVal + (tempVal * room_data.tax_percentage) / 100;
      return tempVal;
    },
    subtotalAfterDiscount(room_data, room_bookings) {
      let tempVal = 0;

      room_bookings.forEach((item) => {
        tempVal = tempVal + parseFloat(item.price);
      });
      // console.log(tempVal);
      if (this.eligibleForDiscount(room_data, room_bookings)) {
        tempVal =
          tempVal -
          (tempVal *
            parseFloat(
              this.eligibleDiscount(room_data, room_bookings).percent
            )) /
            100;
      }
      tempVal = tempVal;
      return tempVal;
    },
    eligibleDiscount(room_data, room_bookings) {
      // console.log(room_data);
      let tempDiscount = false;
      if (room_data.room_discounts) {
        room_data.room_discounts.forEach((discount) => {
          if (
            discount.hours <= room_bookings.length &&
            discount.max_hours >= room_bookings.length
          ) {
            tempDiscount = discount;
          }
        });
      }
      return tempDiscount;
    },
    eligibleForDiscount(room_data, room_bookings) {
      // console.log(room_data);
      let isTrue = false;
      if (room_data.room_discounts) {
        room_data.room_discounts.forEach((discount) => {
          if (
            discount.hours <= room_bookings.length &&
            discount.max_hours >= room_bookings.length
          ) {
            isTrue = true;
          }
        });
      }
      return isTrue;
    },
    cartProceedNext() {
      this.$emit("cart-proceed-next");
    },
  },
});
function bookingAppInit() {
  if (!document.querySelector("#booking-app")) {
    return;
  }

  new Vue({
    el: "#booking-app",
    data: {
      renderCart: true,
      isAppLoading: true,
      isInitDataLoaded: false,
      currentStep: "step-1",
      roomInfo: {},
      roomTimeSlots: [],
      roomsData: null,
      roomThisWeekBookedTimeSlots: [],
      cart: [],
      promo_data: null,
      user_allow_manual: false,
      set_as_manual_confirm: false,
      show_sub_rooms: false,
      shown_sub_rooms_confirm: false,
      show_sub_rooms_confirm: false,
      count_to_show_sub_rooms_confirm: 0,
      on_count_show_sub_rooms_confirm: 1,
      booking_cart: {
        rooms: {
          // "3": {
          //   room_id: "",
          //   cart: []
          // }
        },
      },
      calendar: {
        pickerDate: new Date(),
        isoWeekDays: [
          "monday",
          "tuesday",
          "wednesday",
          "thursday",
          "friday",
          "saturday",
          "sunday",
        ],
        isoWeekDaysInt: [1, 2, 3, 4, 5, 6, 7],
        startMoment: moment().isoWeekday(1),
        thisWeek: moment().isoWeekday(1).week(),
        currentWeek: moment().week(),
      },
      checkOutFormData: {
        form: {
          value: "none",
          ready: false,
          error: false,
          form_submit: true,
        },
        name: {
          value: "",
          error: "",
        },
        email: {
          value: "",
          error: false,
        },
        phone: {
          value: "",
          error: false,
        },
        photographer_name: {
          value: "",
          error: false,
        },
        type_of_shoot: {
          value: "",
          error: false,
        },
        note: {
          value: "",
          error: false,
        },
        agree_to_terms: {
          value: "",
          error: false,
        },
        agree_to_guests: {
          value: "",
          error: false,
        },
        /* agree_to_promo_emails: {
          value: "",
          error: false
        }, */
        /* agree_to_pre_setup: {
          value: "",
          error: false
        } */
      },
      sub_rooms: {},
    },
    watch: {
      show_sub_rooms_confirm() {
        if (this.show_sub_rooms_confirm) {
          this.$nextTick(() => {
            Swal.fire({
              showClass: {
                popup: "animate__animated animate__fadeInDown",
              },
              hideClass: {
                popup: "animate__animated animate__fadeOutUp",
              },
              title: "Book Makeup Station",
              text: "Would you like to reserve our Makeup Station?",
              icon: "question",
              showCancelButton: true,
              confirmButtonColor: "#28a745",
              confirmButtonText: "Yes",
              cancelButtonColor: "#dc3545",
              cancelButtonText: "Later",
            }).then((result) => {
              if (result.value) {
                // this.show_sub_rooms_confirm = false
                this.show_sub_rooms = true;
                this.show_sub_rooms_confirm = false;
                // Swal.fire("Deleted!", "Your file has been deleted.", "success");
              } else {
                this.currentStep = "step-2";
                window.location.hash = "#booking-app";
              }
            });
          });
        }
      },
      cart() {
        this.booking_cart.rooms[this.roomInfo.id] = this.cart;
        setBookingCart(this.booking_cart);
        this.getRoomsData();
        this.forceRerenderCart();
      },
      booking_cart() {
        this.getRoomsData();
      },
      show_sub_rooms() {
        if (this.show_sub_rooms) {
          $("html").addClass("overflow-hidden");
        }
        if (!this.show_sub_rooms) {
          $("html").removeClass("overflow-hidden");
        }
      },
    },
    computed: {
      weekDates() {
        let thisWeekDates = [];
        this.calendar.isoWeekDaysInt.forEach((day) => {
          thisWeekDates.push(
            moment()
              .week(this.calendar.currentWeek)
              .isoWeekday(day)
              .format("YYYY-MM-DD")
          );
        });
        return thisWeekDates;
      },
      weekStartDate() {
        return moment()
          .week(this.calendar.currentWeek)
          .isoWeekday(1)
          .subtract(1, "days")
          .format("YYYY-MM-DD");
      },
      weekEndDate() {
        return moment()
          .week(this.calendar.currentWeek)
          .isoWeekday(7)
          .add(1, "days")
          .format("YYYY-MM-DD");
      },
      cartGrandTotal() {
        if (this.cart.length < 1) {
          return 0;
        }
        let tax = parseFloat(this.roomInfo.tax_percentage);
        let total = this.cart.reduce((sum, current, i) => {
          return sum + parseFloat(current.price);
        }, 0);
        // let grandTotal = total + (total * tax) / 100;
        let grandTotal = total + (total * tax) / 100;

        if (this.promo_data) {
          grandTotal =
            grandTotal -
            (grandTotal * parseFloat(this.promo_data.off_percent)) / 100;
        }

        grandTotal = total + (total * 13) / 100;

        return grandTotal.toFixed(2);
      },
      cartSubTotal() {
        if (this.cart.length < 1) {
          return 0;
        }
        let total = this.cart.reduce((sum, current, i) => {
          return sum + parseFloat(current.price);
        }, 0);
        let subTotal = total;
        return subTotal;
      },
      cartItemsForFormData() {
        if (this.cart.length < 1) {
          return [];
        }
        let cartItems = [];
        cartItems = this.cart.map((item) => {
          return {
            booked_date: moment(item.dateTime, "YYYY-MM-DD hh:mm A").format(
              "YYYY-MM-DD"
            ),
            /* after booking created */
            booking_id: null,
            room_id: this.roomInfo.id,
            time_from: moment(item.dateTime, "YYYY-MM-DD hh:mm A").format(
              "YYYY-MM-DD HH:mm:ss"
            ),
            time_to: moment(item.dateTime, "YYYY-MM-DD hh:mm A")
              .add(1, "hour")
              .format("YYYY-MM-DD HH:mm:ss"),
            price: item.price,
          };
        });

        return cartItems;
      },
    },
    mounted() {
      this.isAppLoading = false;
      if (window.user_allow_manual) {
        this.user_allow_manual = true;
      }
      let bookingCart = getBookingCart();
      if (bookingCart) {
        this.booking_cart = bookingCart;
        setBookingCart(this.booking_cart);
      }
      this.get_sub_rooms_data();

      // Add a request interceptor
    },
    created() {
      this.getRoomInfo();
    },
    methods: {
      get_sub_rooms_data() {
        // this.isAppLoading = true;

        axios({
          method: "GET",
          url: bookingApiObj.url,
          params: {
            action: "get_sub_rooms_data",
            security: bookingApiObj.security,
          },
        })
          .then((res) => {
            this.sub_rooms = res.data.data;
            // console.log(res.data.data);

            // this.isAppLoading = false;
          })
          .catch((err) => {
            // this.isAppLoading = false;
            console.log(err);
            alert("Oops! something went wrong please try again");
          });
      },
      onIsAppLoading(bool) {
        this.isAppLoading = bool;
      },
      addPromoCode(data) {
        this.promo_data = data;
      },
      removePromoCode() {
        this.promo_data = null;
      },
      forceRerenderCart() {
        this.renderCart = false;
        this.$nextTick().then(() => {
          this.renderCart = true;
        });
      },
      getRoomsData() {
        this.isAppLoading = true;

        if (this.roomsData) {
          if (
            JSON.stringify(Object.keys(this.booking_cart.rooms).sort()) ==
            JSON.stringify(Object.keys(this.roomsData).sort())
          ) {
            this.isAppLoading = false;
            return false;
          }
        }

        if (Object.keys(this.booking_cart.rooms).length < 1) {
          this.isAppLoading = false;
          return false;
        }
        this.roomsData = null;

        axios({
          method: "GET",
          url: bookingApiObj.url,
          params: {
            action: "get_rooms_data",
            security: bookingApiObj.security,
            rooms: Object.keys(this.booking_cart.rooms),
          },
        })
          .then((res) => {
            this.roomsData = res.data.data;
            // console.log(res.data.data);
            this.isAppLoading = false;
          })
          .catch((err) => {
            this.isAppLoading = false;
            alert("Oops! something went wrong please try again");
          });
      },
      onSubmitDetailsFormBookAsPostPay() {
        this.checkOutFormData.form.error = true;
        let formValidate = true;
        for (let item in this.checkOutFormData) {
          if (item == "form" || item == "note") {
            continue;
          }
          if (this.checkOutFormData.hasOwnProperty(item)) {
            if (this.checkOutFormData[item].value == "") {
              formValidate = false;
            }
          }
        }
        if (formValidate) {
          this.checkOutFormData.form.error = false;
          this.checkOutFormData.form.ready = true;

          let postFormData = {
            booking: {
              user_id: null,
              room_id: this.roomInfo.id,
              user_name: this.checkOutFormData.name.value,
              user_email: this.checkOutFormData.email.value,
              user_phone: this.checkOutFormData.phone.value,
              user_address: null,
              photographer_name: this.checkOutFormData.photographer_name.value,
              type_of_shoot: this.checkOutFormData.type_of_shoot.value,
              user_note: this.checkOutFormData.note.value,
            },
            invoice: {
              /* after booking created */
              booking_id: null,
              status: "payment_pending",
              tax: this.roomInfo.tax_percentage,
              sub_total: this.cartSubTotal,
              grand_total: this.cartGrandTotal,
              created_at: "",
            },
            /* cart items */
            room_booking: this.cartItemsForFormData,
          };

          /*  */
          this.isAppLoading = true;
          axios({
            method: "POST",
            url: bookingApiObj.url,
            // headers: { 'content-type': 'application/x-www-form-urlencoded' },
            params: {
              action: "post_book_as_post_payment",
              security: bookingApiObj.security,
            },
            data: postFormData,
          })
            .then((res) => {
              this.isAppLoading = false;
              this.handlePostPaymentBookingRes(res);
            })
            .catch((err) => {
              this.isAppLoading = false;
              alert("Oops! something went wrong please try again");
            });
          /*  */
        }
      },
      handlePostPaymentBookingRes(res) {
        this.checkOutFormData.form.form_submit = false;

        if (typeof res == "undefined" || res.error || res.data.error) {
          this.checkOutFormData.form.form_submit = true;
          Swal.fire({
            showClass: {
              popup: "animate__animated animate__fadeInDown",
            },
            hideClass: {
              popup: "animate__animated animate__fadeOutUp",
            },
            // timer: 3000,
            type: typeof res.message ? res.message : "error",
            title: "Error try again later.",
          });
          return false;
        }

        if (res.data.invoice_id) {
          let swalTimerInterval;
          Swal.fire({
            showClass: {
              popup: "animate__animated animate__fadeInDown",
            },
            hideClass: {
              popup: "animate__animated animate__fadeOutUp",
            },
            type: "success",
            title: "Booking Confirmation",
            html: `<P> Please settle the amount by electronic transfer to confirm the booking. Our email id is <a href="mailto:hello@linenandlovestudios.com">hello@linenandlovestudios.com</a> </P> <P> Your <strong>Invoice ID</strong> is <strong>#${res.data.invoice_id}</strong> a copy of the invoice will be sent your email as well. </P> <P> Thank You </P>`,
            onClose: () => {
              window.location = base_url;
            },
          });
        }
      },
      invoiceCartSubTotal(rooms_data, cart_rooms) {
        let tempVal = 0;
        for (const key in cart_rooms) {
          tempVal = tempVal + this.subtotal(rooms_data[key], cart_rooms[key]);
        }
        return tempVal;
      },
      cartTotal(rooms_data, cart_rooms) {
        let tempVal = 0;
        for (const key in cart_rooms) {
          tempVal = tempVal + this.subtotal(rooms_data[key], cart_rooms[key]);
        }
        if (this.promo_data) {
          tempVal =
            tempVal - (tempVal * parseFloat(this.promo_data.off_percent)) / 100;
        }
        tempVal = tempVal + (tempVal * 13) / 100;
        return tempVal;
      },
      subtotal(room_data, room_bookings) {
        let tempVal = 0;

        room_bookings.forEach((item) => {
          tempVal = tempVal + parseFloat(item.price);
        });
        // console.log(tempVal);
        if (this.eligibleForDiscount(room_data, room_bookings)) {
          tempVal =
            tempVal -
            (tempVal *
              parseFloat(
                this.eligibleDiscount(room_data, room_bookings).percent
              )) /
              100;
        }
        // tempVal = tempVal + (tempVal * room_data.tax_percentage) / 100;
        return tempVal;
      },
      eligibleDiscount(room_data, room_bookings) {
        let tempDiscount = false;
        if (room_data.room_discounts) {
          room_data.room_discounts.forEach((discount) => {
            if (
              discount.hours <= room_bookings.length &&
              discount.max_hours >= room_bookings.length
            ) {
              tempDiscount = discount;
            }
          });
        }
        return tempDiscount;
      },
      eligibleForDiscount(room_data, room_bookings) {
        let isTrue = false;
        if (room_data.room_discounts) {
          room_data.room_discounts.forEach((discount) => {
            if (
              discount.hours <= room_bookings.length &&
              discount.max_hours >= room_bookings.length
            ) {
              isTrue = true;
            }
          });
        }
        return isTrue;
      },
      onSubmitDetailsForm() {
        this.checkOutFormData.form.error = true;
        let formValidate = true;
        for (let item in this.checkOutFormData) {
          if (item == "form" || item == "note") {
            continue;
          }
          if (this.checkOutFormData.hasOwnProperty(item)) {
            if (this.checkOutFormData[item].value == "") {
              formValidate = false;
            }
            if (item == "phone") {
              if (/^\d{10}$/.test(this.checkOutFormData.phone.value)) {
                this.checkOutFormData.phone.error = false;
              } else {
                formValidate = false;
                this.checkOutFormData.phone.error = true;
              }
            }
          }
        }
        /*         console.log(formValidate);
        console.log(this.checkOutFormData.phone.error);
        return 'test'; */
        if (formValidate) {
          this.checkOutFormData.form.error = false;
          this.checkOutFormData.form.ready = true;
          let bookings = [];

          for (const key in this.booking_cart.rooms) {
            let room_bookings = this.booking_cart.rooms[key];

            bookings.push({
              room_id: key,
              user_id: null,
              invoice_id: null,
              tax_percent: this.roomsData[key].tax_percentage,
              discount_percent: this.eligibleForDiscount(
                this.roomsData[key],
                room_bookings
              )
                ? this.eligibleDiscount(this.roomsData[key], room_bookings)
                    .percent
                : 0,
              total: this.subtotal(this.roomsData[key], room_bookings),
              user_name: this.checkOutFormData.name.value,
              user_email: this.checkOutFormData.email.value,
              user_phone: this.checkOutFormData.phone.value,
              user_address: null,
              photographer_name: this.checkOutFormData.photographer_name.value,
              type_of_shoot: this.checkOutFormData.type_of_shoot.value,
              user_note: this.checkOutFormData.note.value,
              room_bookings: room_bookings.map((booking) => {
                return {
                  booked_date: moment(
                    booking.dateTime,
                    "YYYY-MM-DD hh:mm A"
                  ).format("YYYY-MM-DD"),
                  /* after booking created */
                  booking_id: null,
                  room_id: this.roomInfo.id,
                  time_from: moment(
                    booking.dateTime,
                    "YYYY-MM-DD hh:mm A"
                  ).format("YYYY-MM-DD HH:mm:ss"),
                  time_to: moment(booking.dateTime, "YYYY-MM-DD hh:mm A")
                    .add(1, "hour")
                    .format("YYYY-MM-DD HH:mm:ss"),
                  price: booking.price,
                };
              }),
            });
          }

          let postFormData = {
            set_as_manual_confirm: this.set_as_manual_confirm,
            bookings: bookings,
            invoice: {
              /* after booking created */
              status: "processing",
              tax: 13,
              sub_total: this.invoiceCartSubTotal(
                this.roomsData,
                this.booking_cart.rooms
              ),
              grand_total: this.cartTotal(
                this.roomsData,
                this.booking_cart.rooms
              ),
            },
            /* cart items */
            // room_booking: this.cartItemsForFormData
          };

          if (this.promo_data) {
            postFormData.invoice.discount_percent = this.promo_data.off_percent;
            postFormData.invoice.coupon_code = this.promo_data.code;
          }

          /*  */

          // console.log(postFormData);

          // return false;

          this.isAppLoading = true;

          // if(){}

          axios({
            method: "post",
            url: bookingApiObj.url,
            // headers: { 'content-type': 'application/x-www-form-urlencoded' },
            params: {
              action: this.set_as_manual_confirm
                ? "post_payment_manual"
                : "post_payment_pre_auth",
              security: bookingApiObj.security,
            },
            data: postFormData,
          })
            .then((res) => {
              this.isAppLoading = false;
              this.handlePaymentPreAuthRes(res);
            })
            .catch((err) => {
              this.isAppLoading = false;
              alert("Oops! something went wrong please try again");
            });
          /*  */
        }
      },
      handlePaymentPreAuthRes(res) {
        this.checkOutFormData.form.form_submit = false;

        if (this.set_as_manual_confirm) {
          if (typeof res == "undefined") {
            Swal.fire({
              showClass: {
                popup: "animate__animated animate__fadeInDown",
              },
              hideClass: {
                popup: "animate__animated animate__fadeOutUp",
              },
              // timer: 3000,
              type: "error",
              title: "Something went wrong try please try again",
            });
            return false;
          } else {
            removeBookingCart();
            Swal.fire({
              showClass: {
                popup: "animate__animated animate__fadeInDown",
              },
              hideClass: {
                popup: "animate__animated animate__fadeOutUp",
              },
              type: "success",
              title: "Booking Confirmed!",
              html: "You will be redirected to invoice page shortly.",
              timer: 2000,
            });
            setTimeout(() => {
              window.location = res.data.invoice_url;
            }, 2000);
            return false;
          }
        }

        if (
          typeof res == "undefined" ||
          typeof res.data.gateway_res_data == "undefined" ||
          res.data.gateway_res_data.response_code >= 50
        ) {
          Swal.fire({
            showClass: {
              popup: "animate__animated animate__fadeInDown",
            },
            hideClass: {
              popup: "animate__animated animate__fadeOutUp",
            },
            // timer: 3000,
            type: "error",
            title: "Payment Portal Approval Failed!",
          });
          return false;
        }

        if (res.data.gateway_res_data.response_code < 50) {
          removeBookingCart();
          let swalTimerInterval;
          Swal.fire({
            showClass: {
              popup: "animate__animated animate__fadeInDown",
            },
            hideClass: {
              popup: "animate__animated animate__fadeOutUp",
            },
            type: "success",
            title: "Payment Portal Approval Granted!",
            html: "You will be redirected to secure payment portal shortly.",
            timer: 5000,
          });
          let formHtml = `<form class="js-moneris-payment-redirect-submit-form" action="${res.data.gateway_url}" method="POST">
								<input type="hidden" name="hpp_preload" value="" />
								<input type="hidden" name="hpp_id" value="${res.data.gateway_res_data.hpp_id}" />
								<input type="hidden" name="ticket" value="${res.data.gateway_res_data.ticket}" />
							</form>`;

          document
            .querySelector("body")
            .insertAdjacentHTML("beforeend", formHtml);
          setTimeout(() => {
            document
              .querySelector(".js-moneris-payment-redirect-submit-form")
              .submit();
          }, 4000);
        }
      },
      onCartProceedNext() {
        if (this.roomInfo.type == "studio") {
          // this.count_to_show_sub_rooms_confirm++;
          // if (
          //   this.count_to_show_sub_rooms_confirm ==
          //   this.on_count_show_sub_rooms_confirm ) {
          if (
            Object.keys(this.sub_rooms).length > 0 &&
            !this.shown_sub_rooms_confirm
          ) {
            this.show_sub_rooms_confirm = true;
            this.shown_sub_rooms_confirm = true;
            return;
          }
          // }
        }
        this.currentStep = "step-2";
        window.location.hash = "#booking-app";
      },
      addToCart(data) {
        if (data.addedToCart) {
          this.cart.push(data);
          /* if (this.roomInfo.type == "studio") {
            this.count_to_show_sub_rooms_confirm++;
            if (
              this.count_to_show_sub_rooms_confirm ==
              this.on_count_show_sub_rooms_confirm ) {
              if (Object.keys(this.sub_rooms).length > 0) {
                this.show_sub_rooms_confirm = true;
              }
            }
          } */
          toastAlert.fire({
            icon: "success",
            title: "Added to Cart",
          });
        }
        if (!data.addedToCart) {
          this.cart.forEach((item, i) => {
            if (data.dateTime == item.dateTime) {
              this.cart.splice(i, 1);
            }
          });
          toastAlert.fire({
            icon: "error",
            title: "Removed from Cart",
          });
        }
      },
      removeFromCart(room_id, item_data) {
        this.booking_cart.rooms[room_id].forEach((item, i) => {
          if (item_data.dateTime == item.dateTime) {
            this.booking_cart.rooms[room_id].splice(i, 1);
          }
        });
        setBookingCart(this.booking_cart);
        this.forceRerenderCart();
      },
      setNow(date) {
        // console.log(date.toString());
        // console.log(date.diff());
        // console.log(moment(date).diff(moment().week(1),'week'));
        let weekDiff = moment(date).diff(moment().week(1), "week") + 1;
        // console.log(weekDiff);
        // console.log(moment(date).week());
        // this.calendar.currentWeek = moment(date).week();
        this.calendar.currentWeek = weekDiff;
        this.getRoomInfo();
      },
      testSubmit(ev) {
        ev.preventDefault();
        ev.stopPropagation();
        let formData = new FormData(ev.target);
      },
      getRoomInfo() {
        this.isInitDataLoaded = false;
        this.isAppLoading = true;
        axios({
          method: "GET",
          url: bookingApiObj.url,
          params: {
            action: "get_booking_page_info",
            security: bookingApiObj.security,
            page_id: pageInfoObj.page_id,
            bookings_from: this.weekStartDate,
            bookings_to: this.weekEndDate,
          },
        })
          .then((res) => {
            this.roomInfo = res.data.data.room_info;
            this.roomTimeSlots = res.data.data.room_time_slots;
            this.roomThisWeekBookedTimeSlots = res.data.data.room_booking_info;
            this.isAppLoading = false;
            this.isInitDataLoaded = true;
            if (
              typeof this.booking_cart.rooms[this.roomInfo.id] != "undefined"
            ) {
              this.cart = this.booking_cart.rooms[this.roomInfo.id];
            }
          })
          .catch((err) => {
            this.isAppLoading = false;
            alert("Oops! something went wrong please try again");
          });
      },
      setCalendarPreviousWeek() {
        this.calendar.currentWeek -= 1;
        this.getRoomInfo();
      },
      setCalendarNextWeek() {
        this.calendar.currentWeek += 1;
        this.getRoomInfo();
      },
      switchToStep1() {
        this.currentStep = "step-1";
      },
      switchToStep2() {
        if (this.cart.length < 1) {
          return;
        }
        this.currentStep = "step-2";
      },
    },
  });
}
bookingAppInit();

function setBookingCart(data) {
  let temp = {
    booking_cart: {
      rooms: {
        "3": {
          room_id: "",
          cart: [],
        },
      },
    },
  };

  for (const key in data.rooms) {
    if (data.rooms[key].length < 1) {
      delete data.rooms[key];
    }
  }
  window.localStorage.setItem("booking_cart", JSON.stringify(data));
  updateHeaderCartBtn();
}

function removeBookingCart() {
  window.localStorage.removeItem("booking_cart");
  updateHeaderCartBtn();
}

function getBookingCart() {
  let tempReturn = null;
  let tempVal = window.localStorage.getItem("booking_cart");
  if (tempVal) {
    tempReturn = JSON.parse(tempVal);
  }
  return tempReturn;
}
