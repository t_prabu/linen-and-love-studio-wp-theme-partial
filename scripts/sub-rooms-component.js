Vue.component("sub-rooms-component", {
  template: "#sub-rooms-component-template",
  data() {
    return {
      isAppLoading: true,
      isInitDataLoaded: false,
      selectedRoom: null,
      roomInfo: {},
      roomTimeSlots: [],
      roomsData: [],
      roomThisWeekBookedTimeSlots: [],
      cart: [],
      calendar: {
        pickerDate: new Date(),
        isoWeekDays: [
          "monday",
          "tuesday",
          "wednesday",
          "thursday",
          "friday",
          "saturday",
          "sunday",
        ],
        isoWeekDaysInt: [1, 2, 3, 4, 5, 6, 7],
        startMoment: moment().isoWeekday(1),
        thisWeek: moment().isoWeekday(1).week(),
        currentWeek: moment().week(),
      },
    };
  },
  props: {
    enableCalendarCtrl: {
      default: false,
    },
    /*    calendar: {
      default: null,
    }, */
/*     weekDates: {
      default: null,
    },
    weekStartDate: {
      default: null,
    },
    weekEndDate: {
      default: null,
    }, */
  },
  computed: {
    weekDates() {
      let thisWeekDates = [];
      this.calendar.isoWeekDaysInt.forEach((day) => {
        thisWeekDates.push(
          moment()
            .week(this.calendar.currentWeek)
            .isoWeekday(day)
            .format("YYYY-MM-DD")
        );
      });
      return thisWeekDates;
    },
    weekStartDate() {
      return moment()
        .week(this.calendar.currentWeek)
        .isoWeekday(1)
        .subtract(1, "days")
        .format("YYYY-MM-DD");
    },
    weekEndDate() {
      return moment()
        .week(this.calendar.currentWeek)
        .isoWeekday(7)
        .add(1, "days")
        .format("YYYY-MM-DD");
    },
  },
  watch: {
    cart() {
      // console.log("test");
      this.$root.booking_cart.rooms[this.roomInfo.id] = this.cart;
      setBookingCart(this.$root.booking_cart);
      this.$root.getRoomsData();
      this.$root.forceRerenderCart();
    },
    selectedRoom() {
      if (this.selectedRoom) {
        this.getRoomInfo();
      }
    },
  },
  mounted() {
    // this.isAppLoading = false;
  },
  created() {
    // this.getRoomInfo();
    this.get_sub_rooms_data();
  },
  methods: {
    setCalendarPreviousWeek() {
      this.calendar.currentWeek -= 1;
      this.getRoomInfo();
    },
    setCalendarNextWeek() {
      this.calendar.currentWeek += 1;
      this.getRoomInfo();
    },
    setNow(date) {
      // console.log(date.toString());
      // console.log(date.diff());
      // console.log(moment(date).diff(moment().week(1),'week'));
      let weekDiff = moment(date).diff(moment().week(1), "week") + 1;
      // console.log(weekDiff);
      // console.log(moment(date).week());
      // this.calendar.currentWeek = moment(date).week();
      this.calendar.currentWeek = weekDiff;
      this.getRoomInfo();
    },
    addToCart(data) {
      if (data.addedToCart) {
        this.cart.push(data);
        if (this.roomInfo.type == "studio") {
          this.count_to_show_sub_rooms_confirm++;
          if (
            this.count_to_show_sub_rooms_confirm ==
            this.on_count_show_sub_rooms_confirm
          ) {
            this.show_sub_rooms_confirm = true;
          }
        }
        toastAlert.fire({
          icon: "success",
          title: "Added to Cart",
        });
      }
      if (!data.addedToCart) {
        this.cart.forEach((item, i) => {
          if (data.dateTime == item.dateTime) {
            this.cart.splice(i, 1);
          }
        });
        toastAlert.fire({
          icon: "error",
          title: "Removed from Cart",
        });
      }
    },
    getRoomInfo() {
      this.isInitDataLoaded = false;
      this.isAppLoading = true;
      axios({
        method: "GET",
        url: bookingApiObj.url,
        params: {
          action: "get_booking_page_info",
          security: bookingApiObj.security,
          page_id: this.selectedRoom.page_id,
          bookings_from: this.weekStartDate,
          bookings_to: this.weekEndDate,
        },
      })
        .then((res) => {
          this.roomInfo = res.data.data.room_info;
          this.roomTimeSlots = res.data.data.room_time_slots;
          this.roomThisWeekBookedTimeSlots = res.data.data.room_booking_info;
          this.isAppLoading = false;
          this.isInitDataLoaded = true;
          if (
            typeof this.$root.booking_cart.rooms[this.roomInfo.id] !=
            "undefined"
          ) {
            this.cart = this.$root.booking_cart.rooms[this.roomInfo.id];
            // console.log("test");
          }
        })
        .catch((err) => {
          this.isAppLoading = false;
          console.log(err);
          alert("Oops! something went wrong please try again");
        });
    },
    get_sub_rooms_data() {
      this.isAppLoading = true;

      axios({
        method: "GET",
        url: bookingApiObj.url,
        params: {
          action: "get_sub_rooms_data",
          security: bookingApiObj.security,
        },
      })
        .then((res) => {
          this.roomsData = res.data.data;
          // console.log(res.data.data);

          for (const key in this.roomsData) {
            if (this.roomsData.hasOwnProperty(key)) {
              this.selectedRoom = this.roomsData[key];
              return;
            }
          }

          this.isAppLoading = false;
        })
        .catch((err) => {
          this.isAppLoading = false;
          console.log(err);
          alert("Oops! something went wrong please try again");
        });
    },
    passDatatoParent(data) {
      this.$emit("add-item-to-cart", data);
    },
  },
});
