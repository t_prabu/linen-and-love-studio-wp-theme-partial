function cartAppInit() {
  if (!document.querySelector("#cart-app")) {
    return;
  }
  new Vue({
    el: "#cart-app",
    data: {
      renderCart: true,
      isAppLoading: true,
      isInitDataLoaded: false,
      currentStep: "step-1",
      roomInfo: {},
      roomTimeSlots: [],
      roomsData: null,
      roomThisWeekBookedTimeSlots: [],
      cart: [],
      promo_data: null,
      user_allow_manual: false,
      set_as_manual_confirm: false,
      show_sub_rooms: false,
      show_sub_rooms_confirm: false,
      count_to_show_sub_rooms_confirm: 0,
      on_count_show_sub_rooms_confirm: 1,
      booking_cart: {
        rooms: {
          // "3": {
          //   room_id: "",
          //   cart: []
          // }
        },
      },
      calendar: {
        pickerDate: new Date(),
        isoWeekDays: [
          "monday",
          "tuesday",
          "wednesday",
          "thursday",
          "friday",
          "saturday",
          "sunday",
        ],
        isoWeekDaysInt: [1, 2, 3, 4, 5, 6, 7],
        startMoment: moment().isoWeekday(1),
        thisWeek: moment().isoWeekday(1).week(),
        currentWeek: moment().week(),
      },
      checkOutFormData: {
        form: {
          value: "none",
          ready: false,
          error: false,
          form_submit: true,
        },
        name: {
          value: "",
          error: "",
        },
        email: {
          value: "",
          error: false,
        },
        phone: {
          value: "",
          error: false,
        },
        photographer_name: {
          value: "",
          error: false,
        },
        type_of_shoot: {
          value: "",
          error: false,
        },
        note: {
          value: "",
          error: false,
        },
        agree_to_terms: {
          value: "",
          error: false,
        },
        agree_to_guests: {
          value: "",
          error: false,
        },
        sub_rooms: {},
        /* agree_to_promo_emails: {
          value: "",
          error: false
        }, */
        /* agree_to_pre_setup: {
          value: "",
          error: false
        } */
      },
    },
    watch: {
      cart() {
        // console.log("test");
        this.booking_cart.rooms[this.roomInfo.id] = this.cart;
        setBookingCart(this.booking_cart);
        this.getRoomsData();
        this.forceRerenderCart();
      },
      booking_cart() {
        // console.log("test 2");
        this.getRoomsData();
      },
      show_sub_rooms() {
        if (this.show_sub_rooms) {
          $("html").addClass("overflow-hidden");
        }
        if (!this.show_sub_rooms) {
          $("html").removeClass("overflow-hidden");
        }
      },
    },
    computed: {
      weekDates() {
        let thisWeekDates = [];
        this.calendar.isoWeekDaysInt.forEach((day) => {
          thisWeekDates.push(
            moment()
              .week(this.calendar.currentWeek)
              .isoWeekday(day)
              .format("YYYY-MM-DD")
          );
        });
        return thisWeekDates;
      },
      weekStartDate() {
        return moment()
          .week(this.calendar.currentWeek)
          .isoWeekday(1)
          .subtract(1, "days")
          .format("YYYY-MM-DD");
      },
      weekEndDate() {
        return moment()
          .week(this.calendar.currentWeek)
          .isoWeekday(7)
          .add(1, "days")
          .format("YYYY-MM-DD");
      },
      cartGrandTotal() {
        if (this.cart.length < 1) {
          return 0;
        }
        let tax = parseFloat(this.roomInfo.tax_percentage);
        let total = this.cart.reduce((sum, current, i) => {
          return sum + parseFloat(current.price);
        }, 0);
        // let grandTotal = total + (total * tax) / 100;
        let grandTotal = total + (total * tax) / 100;

        if (this.promo_data) {
          grandTotal =
            grandTotal -
            (grandTotal * parseFloat(this.promo_data.off_percent)) / 100;
        }

        grandTotal = total + (total * 13) / 100;

        return grandTotal.toFixed(2);
      },
      cartSubTotal() {
        if (this.cart.length < 1) {
          return 0;
        }
        let total = this.cart.reduce((sum, current, i) => {
          return sum + parseFloat(current.price);
        }, 0);
        let subTotal = total;
        return subTotal;
      },
      cartItemsForFormData() {
        if (this.cart.length < 1) {
          return [];
        }
        let cartItems = [];
        cartItems = this.cart.map((item) => {
          return {
            booked_date: moment(item.dateTime, "YYYY-MM-DD hh:mm A").format(
              "YYYY-MM-DD"
            ),
            /* after booking created */
            booking_id: null,
            room_id: this.roomInfo.id,
            time_from: moment(item.dateTime, "YYYY-MM-DD hh:mm A").format(
              "YYYY-MM-DD HH:mm:ss"
            ),
            time_to: moment(item.dateTime, "YYYY-MM-DD hh:mm A")
              .add(1, "hour")
              .format("YYYY-MM-DD HH:mm:ss"),
            price: item.price,
          };
        });

        return cartItems;
      },
    },
    mounted() {
      this.isAppLoading = false;
      if (window.user_allow_manual) {
        this.user_allow_manual = true;
      }
      let bookingCart = getBookingCart();
      if (bookingCart) {
        this.booking_cart = bookingCart;
        setBookingCart(this.booking_cart);
      }
      this.get_sub_rooms_data();

      // Add a request interceptor
    },
    created() {
      //   this.getRoomInfo();
    },
    methods: {
      get_sub_rooms_data() {
        // this.isAppLoading = true;

        axios({
          method: "GET",
          url: bookingApiObj.url,
          params: {
            action: "get_sub_rooms_data",
            security: bookingApiObj.security,
          },
        })
          .then((res) => {
            this.sub_rooms = res.data.data;
            // console.log(res.data.data);

            // this.isAppLoading = false;
          })
          .catch((err) => {
            // this.isAppLoading = false;
            console.log(err);
            alert("Oops! something went wrong please try again");
          });
      },
      onIsAppLoading(bool) {
        this.isAppLoading = bool;
      },
      addPromoCode(data) {
        this.promo_data = data;
      },
      removePromoCode() {
        this.promo_data = null;
      },
      forceRerenderCart() {
        this.renderCart = false;
        this.$nextTick().then(() => {
          this.renderCart = true;
        });
      },
      getRoomsData() {
        this.isAppLoading = true;

        if (this.roomsData) {
          if (
            JSON.stringify(Object.keys(this.booking_cart.rooms).sort()) ==
            JSON.stringify(Object.keys(this.roomsData).sort())
          ) {
            this.isAppLoading = false;
            return false;
          }
        }

        if (Object.keys(this.booking_cart.rooms).length < 1) {
          this.isAppLoading = false;
          return false;
        }

        axios({
          method: "GET",
          url: bookingApiObj.url,
          params: {
            action: "get_rooms_data",
            security: bookingApiObj.security,
            rooms: Object.keys(this.booking_cart.rooms),
          },
        })
          .then((res) => {
            this.roomsData = res.data.data;
            // console.log(res.data.data);
            this.isAppLoading = false;
          })
          .catch((err) => {
            this.isAppLoading = false;
            alert("Oops! something went wrong please try again");
          });
      },
      onSubmitDetailsForm() {
        this.checkOutFormData.form.error = true;
        let formValidate = true;
        for (let item in this.checkOutFormData) {
          if (item == "form" || item == "note") {
            continue;
          }
          if (this.checkOutFormData.hasOwnProperty(item)) {
            if (this.checkOutFormData[item].value == "") {
              formValidate = false;
            }
            if (item == "phone") {
              if (/^\d{10}$/.test(this.checkOutFormData.phone.value)) {
                this.checkOutFormData.phone.error = false;
              } else {
                formValidate = false;
                this.checkOutFormData.phone.error = true;
              }
            }
          }
        }
        /*         console.log(formValidate);
        console.log(this.checkOutFormData.phone.error);
        return 'test'; */
        if (formValidate) {
          this.checkOutFormData.form.error = false;
          this.checkOutFormData.form.ready = true;
          let bookings = [];

          for (const key in this.booking_cart.rooms) {
            let room_bookings = this.booking_cart.rooms[key];

            bookings.push({
              room_id: key,
              user_id: null,
              invoice_id: null,
              tax_percent: this.roomsData[key].tax_percentage,
              discount_percent: this.eligibleForDiscount(
                this.roomsData[key],
                room_bookings
              )
                ? this.eligibleDiscount(this.roomsData[key], room_bookings)
                    .percent
                : 0,
              total: this.subtotal(this.roomsData[key], room_bookings),
              user_name: this.checkOutFormData.name.value,
              user_email: this.checkOutFormData.email.value,
              user_phone: this.checkOutFormData.phone.value,
              user_address: null,
              photographer_name: this.checkOutFormData.photographer_name.value,
              type_of_shoot: this.checkOutFormData.type_of_shoot.value,
              user_note: this.checkOutFormData.note.value,
              room_bookings: room_bookings.map((booking) => {
                return {
                  booked_date: moment(
                    booking.dateTime,
                    "YYYY-MM-DD hh:mm A"
                  ).format("YYYY-MM-DD"),
                  /* after booking created */
                  booking_id: null,
                  room_id: this.roomInfo.id,
                  time_from: moment(
                    booking.dateTime,
                    "YYYY-MM-DD hh:mm A"
                  ).format("YYYY-MM-DD HH:mm:ss"),
                  time_to: moment(booking.dateTime, "YYYY-MM-DD hh:mm A")
                    .add(1, "hour")
                    .format("YYYY-MM-DD HH:mm:ss"),
                  price: booking.price,
                };
              }),
            });
          }

          let postFormData = {
            set_as_manual_confirm: this.set_as_manual_confirm,
            bookings: bookings,
            invoice: {
              /* after booking created */
              status: "processing",
              tax: 13,
              sub_total: this.invoiceCartSubTotal(
                this.roomsData,
                this.booking_cart.rooms
              ),
              grand_total: this.cartTotal(
                this.roomsData,
                this.booking_cart.rooms
              ),
            },
            /* cart items */
            // room_booking: this.cartItemsForFormData
          };

          if (this.promo_data) {
            postFormData.invoice.discount_percent = this.promo_data.off_percent;
            postFormData.invoice.coupon_code = this.promo_data.code;
          }

          /*  */

          // console.log(postFormData);

          // return false;

          this.isAppLoading = true;

          // if(){}

          axios({
            method: "post",
            url: bookingApiObj.url,
            // headers: { 'content-type': 'application/x-www-form-urlencoded' },
            params: {
              action: this.set_as_manual_confirm
                ? "post_payment_manual"
                : "post_payment_pre_auth",
              security: bookingApiObj.security,
            },
            data: postFormData,
          })
            .then((res) => {
              this.isAppLoading = false;
              this.handlePaymentPreAuthRes(res);
            })
            .catch((err) => {
              this.isAppLoading = false;
              alert("Oops! something went wrong please try again");
            });
          /*  */
        }
      },
      handlePostPaymentBookingRes(res) {
        this.checkOutFormData.form.form_submit = false;

        if (typeof res == "undefined" || res.error || res.data.error) {
          this.checkOutFormData.form.form_submit = true;
          Swal.fire({
            showClass: {
              popup: "animate__animated animate__fadeInDown",
            },
            hideClass: {
              popup: "animate__animated animate__fadeOutUp",
            },
            // timer: 3000,
            type: typeof res.message ? res.message : "error",
            title: "Error try again later.",
          });
          return false;
        }

        if (res.data.invoice_id) {
          let swalTimerInterval;
          Swal.fire({
            showClass: {
              popup: "animate__animated animate__fadeInDown",
            },
            hideClass: {
              popup: "animate__animated animate__fadeOutUp",
            },
            type: "success",
            title: "Booking Confirmation",
            html: `<P> Please settle the amount by electronic transfer to confirm the booking. Our email id is <a href="mailto:hello@linenandlovestudios.com">hello@linenandlovestudios.com</a> </P> <P> Your <strong>Invoice ID</strong> is <strong>#${res.data.invoice_id}</strong> a copy of the invoice will be sent your email as well. </P> <P> Thank You </P>`,
            onClose: () => {
              window.location = base_url;
            },
          });
        }
      },
      invoiceCartSubTotal(rooms_data, cart_rooms) {
        let tempVal = 0;
        for (const key in cart_rooms) {
          tempVal = tempVal + this.subtotal(rooms_data[key], cart_rooms[key]);
        }
        return tempVal;
      },
      cartTotal(rooms_data, cart_rooms) {
        let tempVal = 0;
        for (const key in cart_rooms) {
          tempVal = tempVal + this.subtotal(rooms_data[key], cart_rooms[key]);
        }
        if (this.promo_data) {
          tempVal =
            tempVal - (tempVal * parseFloat(this.promo_data.off_percent)) / 100;
        }
        tempVal = tempVal + (tempVal * 13) / 100;
        return tempVal;
      },
      subtotal(room_data, room_bookings) {
        let tempVal = 0;

        room_bookings.forEach((item) => {
          tempVal = tempVal + parseFloat(item.price);
        });
        // console.log(tempVal);
        if (this.eligibleForDiscount(room_data, room_bookings)) {
          tempVal =
            tempVal -
            (tempVal *
              parseFloat(
                this.eligibleDiscount(room_data, room_bookings).percent
              )) /
              100;
        }
        // tempVal = tempVal + (tempVal * room_data.tax_percentage) / 100;
        return tempVal;
      },
      eligibleDiscount(room_data, room_bookings) {
        let tempDiscount = false;
        if (room_data.room_discounts) {
          room_data.room_discounts.forEach((discount) => {
            if (
              discount.hours <= room_bookings.length &&
              discount.max_hours >= room_bookings.length
            ) {
              tempDiscount = discount;
            }
          });
        }
        return tempDiscount;
      },
      eligibleForDiscount(room_data, room_bookings) {
        let isTrue = false;
        if (room_data.room_discounts) {
          room_data.room_discounts.forEach((discount) => {
            if (
              discount.hours <= room_bookings.length &&
              discount.max_hours >= room_bookings.length
            ) {
              isTrue = true;
            }
          });
        }
        return isTrue;
      },
      onSubmitDetailsFormBookAsPostPay() {
        this.checkOutFormData.form.error = true;
        let formValidate = true;
        for (let item in this.checkOutFormData) {
          if (item == "form" || item == "note") {
            continue;
          }
          if (this.checkOutFormData.hasOwnProperty(item)) {
            if (this.checkOutFormData[item].value == "") {
              formValidate = false;
            }
          }
        }
        if (formValidate) {
          this.checkOutFormData.form.error = false;
          this.checkOutFormData.form.ready = true;

          let postFormData = {
            booking: {
              user_id: null,
              room_id: this.roomInfo.id,
              user_name: this.checkOutFormData.name.value,
              user_email: this.checkOutFormData.email.value,
              user_phone: this.checkOutFormData.phone.value,
              user_address: null,
              photographer_name: this.checkOutFormData.photographer_name.value,
              type_of_shoot: this.checkOutFormData.type_of_shoot.value,
              user_note: this.checkOutFormData.note.value,
            },
            invoice: {
              /* after booking created */
              booking_id: null,
              status: "payment_pending",
              tax: this.roomInfo.tax_percentage,
              sub_total: this.cartSubTotal,
              grand_total: this.cartGrandTotal,
              created_at: "",
            },
            /* cart items */
            room_booking: this.cartItemsForFormData,
          };

          /*  */
          this.isAppLoading = true;
          axios({
            method: "POST",
            url: bookingApiObj.url,
            // headers: { 'content-type': 'application/x-www-form-urlencoded' },
            params: {
              action: "post_book_as_post_payment",
              security: bookingApiObj.security,
            },
            data: postFormData,
          })
            .then((res) => {
              this.isAppLoading = false;
              this.handlePostPaymentBookingRes(res);
            })
            .catch((err) => {
              this.isAppLoading = false;
              alert("Oops! something went wrong please try again");
            });
          /*  */
        }
      },

      handlePaymentPreAuthRes(res) {
        this.checkOutFormData.form.form_submit = false;

        if (this.set_as_manual_confirm) {
          if (typeof res == "undefined") {
            Swal.fire({
              showClass: {
                popup: "animate__animated animate__fadeInDown",
              },
              hideClass: {
                popup: "animate__animated animate__fadeOutUp",
              },
              // timer: 3000,
              type: "error",
              title: "Something went wrong try please try again",
            });
            return false;
          } else {
            removeBookingCart();
            Swal.fire({
              showClass: {
                popup: "animate__animated animate__fadeInDown",
              },
              hideClass: {
                popup: "animate__animated animate__fadeOutUp",
              },
              type: "success",
              title: "Booking Confirmed!",
              html: "You will be redirected to invoice page shortly.",
              timer: 2000,
            });
            setTimeout(() => {
              window.location = res.data.invoice_url;
            }, 2000);
            return false;
          }
        }

        if (
          typeof res == "undefined" ||
          typeof res.data.gateway_res_data == "undefined" ||
          res.data.gateway_res_data.response_code >= 50
        ) {
          Swal.fire({
            showClass: {
              popup: "animate__animated animate__fadeInDown",
            },
            hideClass: {
              popup: "animate__animated animate__fadeOutUp",
            },
            // timer: 3000,
            type: "error",
            title: "Payment Portal Approval Failed!",
          });
          return false;
        }

        if (res.data.gateway_res_data.response_code < 50) {
          removeBookingCart();
          let swalTimerInterval;
          Swal.fire({
            showClass: {
              popup: "animate__animated animate__fadeInDown",
            },
            hideClass: {
              popup: "animate__animated animate__fadeOutUp",
            },
            type: "success",
            title: "Payment Portal Approval Granted!",
            html: "You will be redirected to secure payment portal shortly.",
            timer: 5000,
          });
          let formHtml = `<form class="js-moneris-payment-redirect-submit-form" action="${res.data.gateway_url}" method="POST">
								<input type="hidden" name="hpp_preload" value="" />
								<input type="hidden" name="hpp_id" value="${res.data.gateway_res_data.hpp_id}" />
								<input type="hidden" name="ticket" value="${res.data.gateway_res_data.ticket}" />
							</form>`;

          document
            .querySelector("body")
            .insertAdjacentHTML("beforeend", formHtml);
          setTimeout(() => {
            document
              .querySelector(".js-moneris-payment-redirect-submit-form")
              .submit();
          }, 4000);
        }
      },
      onCartProceedNext() {
        this.currentStep = "step-2";
        window.location.hash = "#cart-app";
      },
      addToCart(data) {
        if (data.addedToCart) {
          this.cart.push(data);
        }
        if (!data.addedToCart) {
          this.cart.forEach((item, i) => {
            if (data.dateTime == item.dateTime) {
              this.cart.splice(i, 1);
            }
          });
        }
      },
      removeFromCart(room_id, item_data) {
        this.booking_cart.rooms[room_id].forEach((item, i) => {
          if (item_data.dateTime == item.dateTime) {
            this.booking_cart.rooms[room_id].splice(i, 1);
          }
        });
        setBookingCart(this.booking_cart);
      },
      setNow(date) {
        // console.log(date.toString());
        // console.log(date.diff());
        // console.log(moment(date).diff(moment().week(1),'week'));
        let weekDiff = moment(date).diff(moment().week(1), "week") + 1;
        // console.log(weekDiff);
        // console.log(moment(date).week());
        // this.calendar.currentWeek = moment(date).week();
        this.calendar.currentWeek = weekDiff;
        this.getRoomInfo();
      },
      testSubmit(ev) {
        ev.preventDefault();
        ev.stopPropagation();
        let formData = new FormData(ev.target);
      },
      getRoomInfo() {
        this.isInitDataLoaded = false;
        this.isAppLoading = true;
        axios({
          method: "GET",
          url: bookingApiObj.url,
          params: {
            action: "get_booking_page_info",
            security: bookingApiObj.security,
            page_id: pageInfoObj.page_id,
            bookings_from: this.weekStartDate,
            bookings_to: this.weekEndDate,
          },
        })
          .then((res) => {
            this.roomInfo = res.data.data.room_info;
            this.roomTimeSlots = res.data.data.room_time_slots;
            this.roomThisWeekBookedTimeSlots = res.data.data.room_booking_info;
            this.isAppLoading = false;
            this.isInitDataLoaded = true;
            if (
              typeof this.booking_cart.rooms[this.roomInfo.id] != "undefined"
            ) {
              this.cart = this.booking_cart.rooms[this.roomInfo.id];
              // console.log("test");
            }
          })
          .catch((err) => {
            this.isAppLoading = false;
            alert("Oops! something went wrong please try again");
          });
      },
      setCalendarPreviousWeek() {
        this.calendar.currentWeek -= 1;
        // this.getRoomInfo();
      },
      setCalendarNextWeek() {
        this.calendar.currentWeek += 1;
        // this.getRoomInfo();
      },
      switchToStep1() {
        this.currentStep = "step-1";
      },
      switchToStep2() {
        if (this.cart.length < 1) {
          return;
        }
        this.currentStep = "step-2";
      },
    },
  });
}
cartAppInit();
