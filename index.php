<?php 
  get_header(); 
  if ( have_posts() ):
  while ( have_posts() ) : the_post(); 
?>


<section class="content-section-wrapper">
  <header class="content-section-header">
    <div class="container">
      <h1 class="text-left"><strong><?php echo explode(' ', get_the_title())[0] ;?></strong>
        <?php echo implode(' ',array_slice(explode(' ', get_the_title()), 1)) ;?>
      </h1>
      <h4>Luxury and Natural Light Photography</h4>
    </div>
  </header>
  <div class="container">
    <div class="content-wrapper row">
      <div class="col-md-8">
        <main>
          <div class="article-wrapper">
            <div class="content pt-4">
              <?php  the_content() ;?>
            </div>
          </div>
        </main>
      </div>
      <div class="col-md-4 p-md-0">
        <?php  echo get_template_part('/template-parts/page-sidebar') ;?>
      </div>
    </div>
  </div>
</section>
<?php
  endwhile;
  endif; 
  get_footer(); 
