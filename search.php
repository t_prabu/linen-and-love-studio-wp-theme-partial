<?php get_header();

?>

<div class="content-section-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="left-panel-wrapper ">
                    <!--  -->
                    <div class="post-wrapper vmp-t-20 vmp-b-50 vm-bg-color-white">

                        <div class="post-title">
                            <h1 class="h1">
                              <?php printf(__('Search Results for: %s', 'shape'), '<span>' . get_search_query() . '</span>');?>
                            </h1>
                    </div>

                            <?php
$s = get_search_query();
$args = array(
    's' => $s,
);
// The Query
$the_query = new WP_Query($args);
if ($the_query->have_posts()) {

    while ($the_query->have_posts()) {
        $the_query->the_post();
        ?>
                    <li>
                        <a href="<?php the_permalink();?>"><?php the_title();?></a>
                    </li>
                 <?php
}
    wpbeginner_numeric_posts_nav();
} else {
    ?>
        <h4>Nothing Found</h4>
         <div class="alert alert-secondary" role="alert">
              <p>Sorry, but nothing matched your search criteria. Please try again with some different keywords.</p>
          </div>
          <?php get_search_form(); ?>
<?php }?>


<!--  -->

                </div>
            </div>
        </div>
            <!--  -->
            <div class="col-md-4">
               <?php get_template_part('/template-parts/page-sidebar')?>
            </div>
            <!--  -->
    </div>
</div>

<?php get_footer();?>