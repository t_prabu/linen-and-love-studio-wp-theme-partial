<?php 
  get_header(); 
  if ( have_posts() ):
  while ( have_posts() ) : the_post(); 
    
    $booking_page_url = '#';
    $page_children = get_children([
        'posts_per_page' => 1,
        'post_parent'    => $post->ID,
        'post_type'      => 'page'
    ]);
    if (count($page_children)>0) {
        $page_children_ids = array_keys($page_children);
        $booking_page_url = get_permalink($page_children[$page_children_ids[0]]->ID);
    }
 $bookBtnStr = 'Book '.str_replace('Studio', '', get_the_title());
    // $bookBtnStr = 'Coming Soon..';
?>
<!-- <style>
  .header-book-btn {
    pointer-events: none;
  }
</style> -->
<section class="content-section-wrapper studio-section-wrapper ">
  <header class="content-section-header studio-section-header ">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h1><strong><?php  echo preg_split('/Loft|Room|Studio/', get_the_title())[0]; ?>
            </strong>
            <?php  preg_match('/Loft|Room|Studio/', get_the_title(), $output_array); echo $output_array[0];?>
          </h1>
          <h4>Luxury and natural light photography</h4>
        </div>
        <div class="col-md-6">
          <a class="header-book-btn"
            href="<?php echo $booking_page_url ;?>">
            <?php echo $bookBtnStr ;?>
          </a>
        </div>
      </div>
    </div>
  </header>
  <div class="content-wrapper p-0 ">
    <?php the_content() ;?>
  </div>
</section>

<!-- fixed rooms floats -->
<div class="fixed-cta-wrapper">
  <?php if ( has_nav_menu( 'book-now-menu' ) ):  
									$menuargs = [
									    'theme_location'  => 'fixed-studio-menu',
									    'menu_class' => 'no-style-list',
									    'echo' => false,
									    'depth' => 1
									];
									;
									echo preg_replace('/<a /', '<a class="fixed-cta-btn" ', wp_nav_menu( $menuargs ));
								endif;
								?>
  <!-- <a href="#" class="fixed-cta-btn">Ball Room</a> -->
</div>
<!-- fixed rooms floats -->

<div class="fixed-float-book-btn-wrapper js-fixed-float-book-btn-wrapper"><a class="header-book-btn"
    href="<?php echo $booking_page_url ;?>"> <?php echo $bookBtnStr ;?></a></div>

<?php
  endwhile;
  endif; 
  ?>
<style>
  .fixed-float-book-now-btn-wrapper.active {
    display: none !important;
  }
</style>
<?php
  get_footer(); 
