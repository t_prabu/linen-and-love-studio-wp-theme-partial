<?php 
  get_header(); 
  if ( have_posts() ):
  while ( have_posts() ) : the_post(); 
?>


<section class="content-section-wrapper">
  <header class="content-section-header">
    <div class="container">
      <h1 class="text-left"><strong><?php echo explode(' ', get_the_title())[0] ;?></strong>
        <?php echo implode(' ',array_slice(explode(' ', get_the_title()), 1)) ;?>
      </h1>
      <h4>Luxury and natural light photography</h4>
    </div>
  </header>
  
  <!--  -->
  <div class="container-fluid">
  <div class="content-wrapper ">
    <!--  -->
    <div id="calendar-app" class="calendar-app-wrapper bg-F6F5F6 p-4">
      <div class="row">
        <div class="col-md-6 mb-2 text-left pl-4 pb-2">
          <div class="h2  text-left ">
            {{moment(selected_date).format('MMMM')}}
          </div>
          <div class="">
             <div class="btn-group" role="group" aria-label="Basic example">
            <button
              @click="changeMonth(-1)"
              type="button"
              class="btn btn-sm btn-secondary"
            >
              <i class="fa fa-chevron-left" aria-hidden="true"></i>
            </button>
            <v-date-picker
              v-model="picker_date"
              @input="setNow"
              :popover="{ placement: 'top', visibility: 'click' }"
              class="btn btn-sm btn-secondary"
              ><button class="btn btn-sm text-white p-0">
                {{moment(selected_date).format('Do - YYYY')}}
              </button>
            </v-date-picker>
            <button
              @click="changeMonth(1)"
              type="button"
              class="btn btn-sm btn-secondary"
            >
              <i class="fa fa-chevron-right" aria-hidden="true"></i>
            </button>
          </div>
          </div>
          <div class="muted py-2">Today - {{moment().format('Do, MMMM, YYYY')}} <a href="#todays-date"><i class="fa fa-link" aria-hidden="true"></i></a></div>
         
        </div>
        <div class="col-md-6 text-right py-3">
          <div
            v-for="room in trans_rooms_info"
            :key="room.id"
            class="text-white font-weight-bold d-inline-block h5 p-2 mr-3"
            :style="bg_color_style_attr(room.color)"
            v-html="room.page.post_title.replace('Book ', '')"
          ></div>
        </div>
      </div>
      
 <div style="overflow:auto;">
      <div class="day-wrapper-title-wrapper" style="min-width: 800px" >
        <div
          class="day-wrapper-title"
          v-for="(day, index) in weekdays"
          :key="index"
        >
          {{day}}
        </div>
      </div>
     
      <div
        class="month-calendar-wrapper" style="min-width: 800px" v-if="typeof current_month == 'number'"
      >
        <div
          class="day-wrapper"
          v-for="(date, index) in current_month_dates"
          :key="index"
          :class="{'not-this-month':date.month()!=current_month, 'active':date.isSame(moment(), 'day')}" :id="activeId(date)"
        >
          <div class="inner-wrapper">
            <div class="date-wrapper">{{date.format("D")}} <span v-if="date.month()!=current_month" class="muted small">{{date.format("MMMM")}}</span></div>
            <div
              class="events-wrapper"
              v-if="room_booking_info && room_booking_info[date.format('YYYY-MM-DD')]"
            >
              <div v-for="(booking, index) in room_booking_info[date.format('YYYY-MM-DD')]"
                :key="index"  v-html="compileEventHtml(booking, date.format('YYYY-MM-DD'))"
              >
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
    <!--  -->
  </div>
</div>

  <!--  -->
  
</section>

<?php
  endwhile;
  endif; 
  get_footer('manage-bookings'); 
