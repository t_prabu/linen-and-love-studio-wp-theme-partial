<?php if ( current_user_can('edit_post')) : ?>
<script>
    window.user_allow_manual = true;
</script>
<?php endif; ?>

<section id="booking-app" class="booking-widget-section-wrapper vmp-b-40">
    <div class="booking-widget-wrapper">
        <div class="booking-window-overlay js-booking-window-overlay" v-bind:class="{ 'd-none' : !isAppLoading }"></div>
        <div class="booking-steps-wrapper">
            <div class="container">
                <div class="row">
                    <div class=" col-md-4">
                        <div class="booking-step-wrapper date-time-step " @click="switchToStep1"
                            :class="{'active':(currentStep=='step-1')}">
                            <div class="icon"></div>
                            <div class="text">
                                <div class="big">Step 01</div>
                                <div class="small">Date & Start Time</div>
                            </div>
                        </div>
                    </div>
                    <div class=" col-md-4">
                        <div class="booking-step-wrapper details-step" @click="switchToStep2"
                            :class="{'active':(currentStep=='step-2')}">
                            <div class="icon"></div>
                            <div class="text">
                                <div class="big">Step 02</div>
                                <div class="small">Details</div>
                            </div>
                        </div>
                    </div>
                    <div class=" col-md-4">
                        <div class="booking-step-wrapper payment-step">
                            <div class="icon"></div>
                            <div class="text">
                                <div class="big">Step 03</div>
                                <div class="small">Payment</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--  -->
        <section class="booking-window-section-wrapper" :class="{'d-none':!(currentStep=='step-1')}">
            <cart-wrapper v-if="roomsData && booking_cart && renderCart" @remove-from-cart="removeFromCart"
                :rooms_data="roomsData" :cart="booking_cart" @cart-proceed-next="onCartProceedNext"
                @add-promo-code="addPromoCode" @remove-promo-code="removePromoCode" @on-is-app-loading="onIsAppLoading">
            </cart-wrapper>
            <div class="container">
                <p class="text-center">
                    Please select available time slots for your booking at the <?php echo str_replace('Book', '', get_the_title()) ;?>
                    Studio.
                </p>
                <div class="booking-window-wrapper vmp-b-100">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="reserved-color"></div>
                            Reserved
                        </div>
                        <div class="col-md-8 pt-4 pb-1 py-md-0 text-center text-md-right">
                            <div v-if="roomInfo.type=='studio' && Object.keys(sub_rooms).length>0" class="d-inline-block">
                                <a class="custom-btn bg-success text-white" href="#" @click.prevent="show_sub_rooms = true;show_sub_rooms_confirm = false" >Add Makeup Station</a>
                            </div>
                            <div class="d-inline-block">
                                <a class="custom-btn bg-green" href="#cart" v-show="(cart.length>0)"> Go to Cart </a>
                            </div>
                            <div class="d-inline-block">
                                <div class="d-inline-block">
                                    <v-date-picker v-model="calendar.pickerDate" @input="setNow"
                                        :popover="{ placement: 'top', visibility: 'click' }" :min-date="new Date()">
                                        <button class="calendar ">Calendar</button>
                                    </v-date-picker>
                                </div>
                                <button class="next-week" @click="setCalendarPreviousWeek"
                                    :class="{'d-none':!(calendar.currentWeek > calendar.thisWeek )}">
                                    Previous Week
                                </button>
                                <button class="next-week" @click="setCalendarNextWeek">Next Week</button>
                            </div>
                        </div>
                    </div>
                    <div class="weekly-calendar-wrapper-outter" v-if="roomInfo.page">
                        <div class="weekly-calendar-wrapper">
                            <!--  -->
                            <daily-wrapper v-for="(date, index) in weekDates" :key="index" :date="date"
                                :room-info="roomInfo">
                                <time-slot-wrapper :date="date" :room-info="roomInfo"
                                    :room_name="roomInfo.page.post_title.replace('Book', '').trim()"
                                    :room_id="roomInfo.id" :cart="cart" :room-time-slots="roomTimeSlots"
                                    :booked-time-slots="roomThisWeekBookedTimeSlots" @add-item-to-cart="addToCart">
                                </time-slot-wrapper>
                            </daily-wrapper>
                            <!--  -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--  -->
        <!--  -->
        <section class="details-window-section-wrapper" :class="{'d-none':!(currentStep=='step-2')}">
            <div class="container  ">
                <div class="bg-white p-4">
                    <!-- <form action="" @submit.prevent="onSubmitDetailsFormBookAsPostPay"
                        :class="{'was-validated': checkOutFormData.form.error }" novalidate> -->
                    <form action="" @submit.prevent="onSubmitDetailsForm"
                        :class="{'was-validated': checkOutFormData.form.error }" novalidate>
                        <p class="text-center pb-3">
                            <strong>Please provide your details in the form below to proceed with the
                                booking.</strong>
                        </p>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group mb-4">
                                    <label for="name">Name <span class="text-danger">*</span></label>
                                    <input v-model="checkOutFormData.name.value" type="text" class="form-control"
                                        name="name" required :readonly="checkOutFormData.form.ready">
                                    <small class="invalid-feedback">Name Required</small>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group mb-4">
                                    <label for="email">Email <span class="text-danger">*</span></label>
                                    <input v-model="checkOutFormData.email.value" type="email" class="form-control"
                                        name="email" required :readonly="checkOutFormData.form.ready">
                                    <small class="invalid-feedback">Email Required</small>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group mb-4">
                                    <label for="phone">Phone Number <span class="text-danger">*</span></label>
                                    <input v-model="checkOutFormData.phone.value" name="phone" type="number"
                                        minlength="10" maxlength="10" class="form-control"
                                        :class="{'is-invalid':checkOutFormData.phone.error}" required
                                        :readonly="checkOutFormData.form.ready">
                                    <small class="invalid-feedback">Phone Number invalid</small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group mb-4">
                                    <label for="photographer_name">Photographer's First and Last Name <span
                                            class="text-danger">*</span></label>
                                    <input v-model="checkOutFormData.photographer_name.value" name="photographer_name"
                                        type="text" class="form-control" required
                                        :readonly="checkOutFormData.form.ready">
                                    <small class="invalid-feedback">Photographer Name Required</small>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mb-4">
                                    <label for="type_of_shoot">Type of Shoot <span class="text-danger">*</span></label>
                                    <select v-model="checkOutFormData.type_of_shoot.value" name="type_of_shoot"
                                        class="form-control" required :readonly="checkOutFormData.form.ready">
                                        <option value=""></option>
                                        <option value="Portrait">Portrait</option>
                                        <option value="Wedding">Wedding</option>
                                        <option value="Fashion">Fashion</option>
                                        <option value="Family">Family</option>
                                        <option value="Boudoir">Boudoir</option>
                                        <option value="Newborn">Newborn</option>
                                        <option value="Other">Other</option>
                                    </select>
                                    <small class="invalid-feedback">Type of Shoot Required</small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group mb-4">
                                    <label for="">Note</label>
                                    <textarea v-model="checkOutFormData.note.value" name="note" class="form-control"
                                        :readonly="checkOutFormData.form.ready"></textarea>
                                    <!-- <small class="invalid-feedback">Help Required</small> -->
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" value="agree"
                                    v-model="checkOutFormData.agree_to_terms.value" name="agree_to_terms" required
                                    :readonly="checkOutFormData.form.ready" />
                                <label class="form-check-label">
                                    I agree to
                                    <a href="https://linenandlovestudios.com/policies-rules-guidelines/" target="_blank"
                                        rel="">Terms and Conditions</a>
                                </label>
                                <small class="invalid-feedback">You must agree before submitting.</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" value="agree"
                                    v-model="checkOutFormData.agree_to_guests.value" name="agree_to_guests" required
                                    :readonly="checkOutFormData.form.ready" />
                                <label class="form-check-label">
                                    I agree there will be no more than 10 guests in the room.
                                </label>
                                <small class="invalid-feedback">You must agree before submitting.</small>
                            </div>
                        </div>

                        <div class="alert alert-warning" v-if="user_allow_manual">
                            <h4 class="alert-heading">Process payment as Manual!</h4>
                            <div class="form-check text-center">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" v-model="set_as_manual_confirm">
                                    check this to set process payment made as manual or other means.
                                </label>
                            </div>
                        </div>

                        <!-- <div class="form-group">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" value="agree"
                                    v-model="checkOutFormData.agree_to_promo_emails.value" name="agree_to_promo_emails"
                                    required :readonly="checkOutFormData.form.ready" />
                                <label class="form-check-label">
                                    I agree to getting promotional emails
                                </label>
                                <small class="invalid-feedback">You must agree before submitting.</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" value="agree"
                                    v-model="checkOutFormData.agree_to_pre_setup.value" name="agree_to_pre_setup"
                                    required :readonly="checkOutFormData.form.ready" />
                                <label class="form-check-label">
                                    I agree that Willow, Birch and Orchid studios will have the holiday setup
                                    which can't be moved
                                </label>
                                <small class="invalid-feedback">You must agree before submitting.</small>
                            </div>
                        </div> -->

                        <div class="row pt-3">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="add-to-cart bg-green"
                                    :disabled="(!checkOutFormData.form.form_submit)"> Proceed To Payment
                                </button>
                                <button type="submit" class="add-to-cart bg-green d-none"
                                    :disabled="(!checkOutFormData.form.form_submit)"> Proceed To Payment
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </section>
        <!--  -->
    </div>
    <!-- <div class="message-bubble-wrapper js-message-bubble-wrapper" :class="{'active':show_sub_rooms_confirm}"> -->
    <div class="message-bubble-wrapper js-message-bubble-wrapper">
        <!-- <button type="button" class="btn btn-outline-danger close-btn"><i class="fa fa-times" aria-hidden="true"></i></button> -->
        <header>
            Would you like to reserve our accompanying facilities?
        </header>
        <main class="p-1 pt-2 text-left">
            <button type="button" class="btn btn-sm btn-outline-success close-btn mr-3 jjs-show-message-bubble"
                @click="show_sub_rooms = true;show_sub_rooms_confirm = false">Yes</button>
            <button type="button" class="btn btn-sm btn-outline-danger close-btn jjs-hide-message-bubble"
                @click="show_sub_rooms_confirm = false">Later</button>
        </main>
    </div>
    <template v-if="show_sub_rooms">
        <sub-rooms-component :calendar="calendar" :week-dates="weekDates"
                            :week-start-date="weekStartDate" :week-end-date="weekEndDate" :enable-calendar-ctrl="false" />    
    </template>
</section>
