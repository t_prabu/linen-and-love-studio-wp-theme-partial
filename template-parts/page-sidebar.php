<aside class="sidebar-wrapper">
	<?php
$args = array(
    'post_type' =>
	'post', 'posts_per_page' => '6', 'order' => 'DESC', ); $loop = new WP_Query($args); if ($loop->have_posts()): ?>
	<section class="recent-blogs-section-wrapper sidebar-section-wrapper">
		<header class="text-center">
			<h2>Recent Blogs</h2>
			<hr />
		</header>
		<div class="recent-blogs-wrapper    ">
			<?php
while ($loop->have_posts()): $loop->the_post(); ?>
			<div class="recent-blog-wrapper">
				<a href="<?php the_permalink()?>">
					<div class="row  p-0 m-0">
							<?php if (has_post_thumbnail()): ?>
						<div class="col-5 p-0 m-0">

							<div class="img-wrapper">
								<div class="img-bg" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(), 'post-thumb-small')[0]; ?>);"></div>
								<img src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(), 'post-thumb-small')[0]; ?>" class="img-fluid" alt="" />
							</div>
						</div>
							<?php endif; ?>
						<div class="col-7 pr-0 ">
							<div class="content">
								<h3><?php the_title()?></h3>
								<hr />
								<p><?php echo wp_trim_words(get_the_content(), 5, '...'); ?></p>
							</div>
						</div>
					</div>
				</a>
			</div>
			<?php
endwhile;
?>
		</div>
	</section>
	<?php endif;
wp_reset_postdata();
?>
</aside>
