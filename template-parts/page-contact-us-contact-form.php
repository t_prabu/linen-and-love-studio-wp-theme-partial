<!--  contact form section -->
<section class="contact-form-section-wrapper vmp-t-60 vmp-b-75">
	<div class="container">
		<div class="contact-form-wrapper">
			<form class="custom-form-wrapper">
				<div class="form-group row">
					<div class="col-sm-6">
						<input type="text" class="form-control" placeholder="Your Name" />
					</div>
					<div class="col-sm-6">
						<input type="text" class="form-control" placeholder="Your Email" />
					</div>
				</div>
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Subject" />
				</div>
				<div class="form-group">
					<textarea class="form-control" name="" placeholder="Message"></textarea>
				</div>
				<div class="form-group ">
					<button type="submit" class="btn">Submit</button>
				</div>
			</form>
		</div>
	</div>
</section>
<!--  /contact form section -->