<script type="text/x-template" id="cart-wrapper-template">
    <div id="cart" v-if="cart && rooms_data" class="cart-summery-wrapper"
    :class="{'d-none':!( Object.keys(cart.rooms).length > 0)}">
    <div class="container ">
        <div>
            <h2 class="text-left pb-3">
                <i class="fa fa-shopping-cart" aria-hidden="true"></i> Cart
            </h2>

            <p v-if="Object.keys($root.sub_rooms).length>0" class="text-center"><strong>NEW!</strong> Book our Makeup Room <a href="#" @click.prevent="$root.show_sub_rooms = true;$root.show_sub_rooms_confirm = false">Click Here</a></p>.
            <div class="d-none d-md-block">
                <div class="cart-header-wrapper row mx-0">
                    <div class="col-1 text-right"><strong>#</strong></div>
                    <div class="col-2"><strong>Date</strong></div>
                    <div class="col-3"><strong>Room</strong></div>
                    <div class="col-2"><strong>Time Slot</strong></div>
                    <div class="col-2"><strong>Price</strong></div>
                    <div class="col-1"></div>
                </div>
            </div>
            <div class="d-block d-md-none">
                <div class="cart-header-wrapper row mx-0">
                    <div class="col-2 text-right"><strong>#</strong></div>
                    <div class="col-10"><strong>Cart Item</strong></div>
                </div>
            </div>
            <div class="cart-items-wrapper pb-2 bg-white " v-for="(cart_items, key) in cart.rooms"
                v-if="cart_items.length>0">
                <div class="cart-item-wrapper row py-2 mx-0 " v-for="(item, i) in cart_items"
                    :class="{'bg-f9' : ((i+1)%2==0)}">
                    <div class="col-2 col-md-1 text-right">{{i+1}}</div>
                    <div class="col-10 col-md-2">
                        <strong class="d-inline-block d-md-none">Date - </strong> {{
                        item.dateTime | time('YYYY-MM-DD hh:mm A').format('YYYY MMM Do') }}
                    </div>
                    <div class="col-10 offset-2 offset-md-0 col-md-3">
                        <strong class="d-inline-block d-md-none">Room - </strong> {{
                        item.room_name }}
                    </div>
                    <div class="col-10 offset-2 offset-md-0 col-md-2 mt-2 mt-md-0">
                        {{item.time}} (1 Hr)
                    </div>
                    <div class="col-10 offset-2 offset-md-0 col-md-2 mt-2 mt-md-0">
                        <strong class="d-inline-block d-md-none">Price - </strong> $
                        {{item.price}}
                    </div>
                    <div class="col-12 col-md-1 text-right">
                        <button @click="removeFromCart(key,item)" type="button"
                            class="btn btn-outline-danger m-0 px-2 py-0">
                            <small><i class="fa fa-times" aria-hidden="true"></i>
                                <span class="d-block d-md-none">Remove</span></small>
                        </button>
                    </div>
                </div>
                <div class="cart-footer-wrapper bg-white">
                    <div class="row mx-0 py-1">
                        <div class="col-5 offset-1"><strong>Sub Total</strong></div>
                        <div class="offset-2 col-4">
                            <strong>$ {{subtotalWithoutDiscount(rooms_data[key], cart_items)}}
                            </strong>
                        </div>
                    </div>
                    <div class="row  mx-0 py-2 bg-f9" v-if="eligibleForDiscount(rooms_data[key], cart_items)">
                        <div class="col-5 offset-1">
                            <strong>Discount ({{eligibleDiscount(rooms_data[key],
                                cart_items).percent}}%)</strong>
                        </div>
                        <div class="offset-2 col-4">
                            <strong>
                                -$ {{+discountAmount(rooms_data[key], cart_items)}}
                            </strong>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mx-0 pb-1 bg-white" v-if="validatePromoCode">
                <div class="col-12">
                    <hr class="mb-1" />
                </div>
                <div class="col-5 offset-1">
                    <strong>Promo ({{ this.promo_code_data.off_percent}}%)</strong>
                </div>
                <div class="offset-2 col-4">
                    <strong>
                        -$
                        {{((cartTotalBeforePromo(rooms_data,cart.rooms)*this.promo_code_data.off_percent)/100).toFixed(2)}}
                    </strong>
                </div>
            </div>

            <div class="row mx-0 pb-1 bg-white">
                <div class="col-5 offset-1"><strong>Tax (13%)</strong></div>
                <div class="offset-2 col-4">
                    <strong>
                        $ {{(cartTotal(rooms_data,cart.rooms)*13/100).toFixed(2)}}
                    </strong>
                </div>
            </div>

            <div class="cart-footer-wrapper bg-white">
                <div class="row mx-0 py-1">
                    <div class="col-12 mb-3">
                        <hr class="my-1" />
                        <hr class="my-0" />
                    </div>
                    <div class="col-5 offset-1"><strong>Total</strong></div>
                    <div class="offset-2 col-4">
                        <strong>$ {{ (parseFloat(cartTotal(rooms_data, cart.rooms)) +
                            parseFloat((cartTotal(rooms_data, cart.rooms) * 13) /
                            100)).toFixed(2) }} (CAD)</strong>
                    </div>
                    <div class="col-12">
                        <hr class="mb-1" />
                    </div>
                </div>
            </div>

            <div class="cart-footer-wrapper bg-white">
                <div class="row mx-0">
                    <div class="col-md-5 offset-md-1"></div>
                    <div class="offset-md-2 col-md-4">
                        <div class="input-group pb-4">
                            <input type="text" class="form-control"
                                :class="{'is-invalid':promo_code_invalid, 'is-valid': validatePromoCode}"
                                placeholder="Promo Code" v-model="promo_code" />
                            <div class="input-group-append">
                                <button class="btn btn-success btn-sm" type="button" @click="applyPromoCode">
                                    Apply
                                </button>
                            </div>
                            <div class="invalid-feedback">Invalid or expired promo code.</div>
                            <div class="valid-feedback">
                                Promo code applied.
                                <span v-if="validatePromoCode">
                                    - {{ this.promo_code_data.off_percent}} %</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="text-center pt-4">
                <button class="bg-green" @click="cartProceedNext">Proceed Next</button>
            </div>
        </div>
    </div>
</div>    
</script>

<script type="text/x-template" id="sub-rooms-component-template">
    <div class="booking-widget-wrapper">
        <div class="booking-window-overlay no-bg-img"></div>
        <div class="sub-rooms-section-wrapper">                    
            <div class="sub-rooms-section-wrapper-inner">
                <button type="button" class="btn btn-sm btn-close btn-danger" @click="$root.show_sub_rooms=false">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button>
                <!-- <h3 class="heading">Please Choose a Facility</h3> -->
                <div class="booking-window-overlay" v-bind:class="{ 'd-none' : !isAppLoading }"></div>
                <h3 class="heading">Book Makeup Station</h3>

                <div class="d-none">
                    <div class="row justify-content-center pt-3">
                        <a href="#" class="fancy-btn-style-2 col-md-4" v-for="room in roomsData" :key="room.id"
                            :class="{'active': (selectedRoom && selectedRoom.id == room.id)}"
                            @click.prevent="selectedRoom = room">{{ room.page.post_title.replace('Book', '').trim()}}</a>
                    </div>
                </div>
                
                <div v-if="roomInfo.page">
                     <div class="text-center">
                        <strong>Price</strong>
                       8am-8pm $ <span style="color: #e2db9e;font-weight: bold;line-height: 1.2;">{{roomTimeSlots.monday[1].price}}</span>
                    Per hour
                    </div>
                    
                    <div class="text-center" v-if="roomInfo.room_discounts.length>0">
                        <strong>Discounts</strong>
                        <span v-for="(item, index) in roomInfo.room_discounts" :key="index">{{ item.hours }} - {{ item.max_hours }} Hrs <span class="text-danger">-{{item.percent}}%</span>
                        </span>
                    </div>
                    <div class="text-center">
                        <span class="text-danger">
                            *
                        </span> One chair per booking.
                    </div>
                </div>                

                <div class="booking-window-section-wrapper mt-1" v-if="roomInfo.page">
                                      
                    <div class="booking-window-wrapper">
                        <div class="row py-0">
    <div class="col-md-4">
        <div class="reserved-color"></div>
        Reserved
    </div>
    <div class="col-md-8 pt-4 pb-1 py-md-0 text-center text-md-right">
        <div class="d-inline-block">
            <div class="d-inline-block">
                <v-date-picker v-model="calendar.pickerDate" @input="setNow" popover-align="center" popover-direction="bottom" :min-date="new Date()">
                    <button class="btn btn-sm btn-secondary p-1 calendar ">Calendar</button>
                </v-date-picker>
            </div>
            <button class="btn btn-sm btn-secondary p-1 next-week" @click="setCalendarPreviousWeek"
                :class="{'d-none':!(calendar.currentWeek > calendar.thisWeek )}">
                Previous Week
            </button>
            <button class="btn btn-sm btn-secondary p-1 next-week" @click="setCalendarNextWeek">Next Week</button>
        </div>
    </div>
</div>
                        
                        <div class="weekly-calendar-wrapper-outter">
                            <div class="weekly-calendar-wrapper">
                                <!--  -->
                                <daily-wrapper v-for="(date, index) in weekDates" :key="index" :date="date"
                                    :room-info="roomInfo">
                                    <time-slot-wrapper :date="date" :room-info="roomInfo"
                                        :room_name="roomInfo.page.post_title.replace('Book', '').trim()"
                                        :room_id="roomInfo.id" :cart="cart" :room-time-slots="roomTimeSlots"
                                        :booked-time-slots="roomThisWeekBookedTimeSlots"
                                        @add-item-to-cart="addToCart">
                                    </time-slot-wrapper>
                                </daily-wrapper>
                                <!--  -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>

<script type="text/x-template" id="time-sub-slot-wrapper-manage-template">
    <div class="time-slot-wrapper" v-if="is_render && time_slot_data ">
  {{time_slot_data.time}}
  <div v-for="slot in time_slot_data.fillable_slots" :key="slot.index">
    <div
      class="btn-group"
      role="group"
      aria-label="Button group with nested dropdown"
    >
      <button
        type="button"
        class="btn btn-sm"
        :class="{'disabled': time_slot_data.in_the_past , 'btn-success': slot.reserved,  'active': slot.reserved }"
      >
        <div v-if="slot.reserved">
          Reserved
        </div>
        <div v-if="!slot.reserved">
          Open
        </div>
      </button>
      <div class="btn-group" role="group">
        <button
          id="btnGroupDrop1"
          type="button"
          class="btn btn-sm dropdown-toggle"
          :class="{'disabled': time_slot_data.in_the_past , 'btn-success': slot.reserved }"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
        ></button>
        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1" style="">
          <a
            class="dropdown-item"
            href="#"
            @click.prevent="updateTimeSlot('reserved', slot.index);slot.reserved=true;reRenderSlot()"
            >Reserved</a
          >
          <a
            class="dropdown-item"
            href="#"
            @click.prevent="updateTimeSlot('canceled', slot.index);slot.reserved=false;reRenderSlot()"
            >Canceled</a
          >
        </div>
      </div>
    </div>
  </div>
</div>
</script>