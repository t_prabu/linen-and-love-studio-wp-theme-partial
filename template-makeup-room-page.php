<?php /* Template Name: Makeup Room Page Template */ ?>
<?php 
  get_header(); 
  if ( have_posts() ):
  while ( have_posts() ) : the_post(); 

?>

<section class="content-section-wrapper studio-section-wrapper ">
	<header class="content-section-header studio-section-header ">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h1>Book <strong><?php  echo str_replace('Book', '', preg_split('/Loft|Room|Studio/', get_the_title())[0]); ?>
						</strong>
						<?php  preg_match('/Loft|Room|Studio/', get_the_title(), $output_array); echo $output_array[0];?>
					</h1>
					<h4>Luxury and natural light photography</h4>
				</div>
				<div class="col-md-6">
					<a class="header-book-btn" href="#booking-app">
						Go to Booking
					</a>
				</div>
			</div>
		</div>
	</header>
	<div class="content-wrapper p-0 ">
		<?php the_content() ;?>
		<!--  -->
		<?php  echo get_template_part('/template-parts/booking-page-widget') ;?>
		<!--  -->
		<!-- <div class="top-wrapper vmp-t-40 vmp-b-30 bg-white">
			<div class="container">
				<div class="row">
					<div class="col-lg-5 order-lg-2 ">
						<div class="booking-ellipse-slider-wrapper" style="margin-top: 0;">
							<div class="booking-ellipse-slider-inner-wrapper ">
								<div class="booking-ellipse-slider js-booking-ellipse-slider  owl-carousel owl-theme">
									<div class="item slide-1"><img
											src="<?php echo get_template_directory_uri() ;?>/images/ballroom/booking/1.png"
		alt="" class="img-fluid" /></div>
	<div class="item"><img
			src="<?php echo get_template_directory_uri() ;?>/images/ballroom/booking/2.png"
			alt="" class="img-fluid" /></div>
	</div>
	</div>
	</div>
	</div>
	<div class="col-lg-7 ">
		<p>
			The ballroom is a bright all-white square room with two large windows and beautiful grey
			floors, filled with natural light and unobstructed
			South West view. Each wall in this room has an intricate molding design, and it’s luxurious
			and romantic Parisian feel is highlighted with a
			beautiful fireplace in the middle. This space is perfect for bridal, editorial and elegant
			family photoshoots. The ballroom is a bright
			all-white square room with two large windows and beautiful grey floors, filled with natural
			light and unobstructed South West view.
		</p>
	</div>
	</div>
	</div>
	</div> -->



	<!-- <section class="studio-rates-and-time-section-wrapper vmp-y-40">
			<div class="container">
				<div class="studio-rates-and-time-wrapper">
					<h2>Rates & Time</h2>
					<hr />
					<div class="row">
						<div class="col-md-8 pr-md-5">
							<ul>
								<li class="date-time">
									<strong class="d-block">If booking the studio during after hours (between 8 pm and 8
										am) </strong>
									Please contact the administrator to confirm the booking
								</li>
								<li class="people">
									<strong class="d-block">Includes up to 10 people</strong>
									Please contact us to book the studio if the number of guests exceeds the capacity
								</li>
							</ul>
						</div>
						<div class="col-md-4 pl-md-0">
							<div class="hrs-rates-wrapper">
								<div class="hrs-rate-wrapper">
									<div class="rate">
										<span class="sign">
											$
										</span>
										<span class="number">80</span>
										<span class="d-inline-block">
											Per hour
										</span>
									</div>
									<div class="time">8:00 AM – 8:00 PM</div>
								</div>
								<div class="hrs-rate-wrapper">
									<div class="rate">
										<span class="sign">
											$
										</span>
										<span class="number">140</span>
										<span class="d-inline-block">
											Per hour
										</span>
									</div>
									<div class="time">8:00 PM – 8:00 AM</div>
								</div>
							</div>
							<p class="disclaimer d-none">
								For photography use only. For video production rates please contact us
							</p>
						</div>
					</div>
				</div>
			</div>
		</section> -->

	<section class="deposit-and-cancellation-section-wrapper vmp-b-0">
		<div class="container">
			<div class="content-wrapper row vmp-b-0">
				<div class="col-md-12">
					<main>
						<div class="article-wrapper">
							<div class="content pt-4">

								<section>
									<div class="container vmp-y-10">
										<p>We require full payment up front for all shoots at the time you book. Should
											you need to cancel for some reason, we would be happy to extend you credit
											but may have to apply a rescheduling fee depending on how much notice you
											give us. The fees are as follows:</p>

										<div class="row vmp-t-20">
											<div class="col-md-4">
												<div class="content_box1">
													<h5>More than 48 hours advance</h5>
													<hr>
													<p>20% of booking value</p>
												</div>
											</div>
											<div class="col-md-4">
												<div class="content_box1">
													<h5>24-48 hours advance notice</h5>
													<hr>
													<p>50% of booking value</p>
												</div>
											</div>
											<div class="col-md-4">
												<div class="content_box1">
													<h5>24 hour advance notice</h5>
													<hr>
													<p>Full value of booking</p>
												</div>
											</div>
										</div>
									</div>
								</section>

							</div>
						</div>
					</main>
				</div>
				<!-- <div class="col-md-4 p-md-0">
			              </div> -->
			</div>
		</div>
	</section>

	<!-- <section class="what-we-offer-section-wrapper booking-what-we-offer-section-wrapper vmp-y-70">
			<div class="container">
				<div class="what-we-offer-wrapper">
					<div class="row">
						<div class="content">
							<h2>What We <strong>OFFER</strong></h2>
							<hr />
							<ul>
								<li>1 white Victorian 3 seater couch in cream colour</li>
								<li>1 dark grey dome chair</li>
								<li>1 vintage white peacock chair (use carefully)</li>
								<li>1 large floor mirror with vintage frame</li>
								<li>1 pale pink velvet accent side chair</li>
								<li>2 v-flats (8ft), double-sided black and white</li>
								<li>Mint bench</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section> -->
	</div>
</section>

<!-- fixed rooms floats -->
<div class="fixed-cta-wrapper">
	<?php if ( has_nav_menu( 'book-now-menu' ) ):  
									$menuargs = [
									    'theme_location'  => 'fixed-studio-menu',
									    'menu_class' => 'no-style-list',
									    'echo' => false,
									    'depth' => 1
									];
									;
									echo preg_replace('/<a /', '<a class="fixed-cta-btn" ', wp_nav_menu( $menuargs ));
								endif;
								?>
	<!-- <a href="#" class="fixed-cta-btn">Ball Room</a> -->
</div>
<!-- fixed rooms floats -->

<?php
  endwhile;
  endif; 
  ?>
<style>
	.fixed-float-book-now-btn-wrapper.active {
		display: none !important;
	}
</style>
<?php
  get_footer(); 
