<?php

namespace Models;

class Room extends \Models\DefaultModel {
    public $fillable=[ 'page_id', 'slot_max_reservations','type', 'status', 'tax_percentage'];
    
    public function page() {
        return $this->hasOne('Models\Page',  'ID', 'page_id');
    }

    public function timeSlots() {
        return $this->hasMany('Models\TimeSlot', 'room_id', 'id');
    }
    
    public function roomBookings() {
        return $this->hasMany('Models\RoomBooking', 'room_id', 'id');
    }
    
    public function roomDiscounts() {
        return $this->hasMany('Models\RoomDiscount', 'room_id', 'id');
    }
    
/*     public function sub_rooms() {
        return $this->whereIn('type', ['makeup_room'])->with(['roomDiscounts','page']);
    } */
}
