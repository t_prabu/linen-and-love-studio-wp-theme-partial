<?php

namespace Models;

class Page extends \Models\DefaultModel {
    protected $table = 'wp_posts';
    public $fillable=['ID'];
}
