<?php

namespace Models;

class Booking extends \Models\DefaultModel {
    public $fillable=[  'room_id', 'user_id', 'invoice_id', 'tax_percent', 'tax_amount', 'discount_percent', 'discount_amount', 'total', 'user_name', 'user_email', 'user_phone', 'user_address', 'user_note', 'photographer_name', 'type_of_shoot' ];
    
    public function roomBooking() {
        return $this->hasMany('Models\RoomBooking', 'booking_id', 'id');
    }
    
    /*   public function bookingDiscount() {
          return $this->hasMany('Models\BookingDiscount', 'booking_id', 'id');
      } */
    
    public function room() {
        return $this->hasOne('Models\Room', 'id', 'room_id');
    }
}
