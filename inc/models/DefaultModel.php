<?php

namespace Models;

abstract class DefaultModel extends \WeDevs\ORM\Eloquent\Model {
    public $timestamps = false;
}
