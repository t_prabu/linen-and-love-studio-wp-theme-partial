<?php

namespace Models;

class RoomBooking extends \Models\DefaultModel {
    public $fillable=[ 'booking_id', 'room_id', 'booked_date', 'time_from', 'time_to', 'price', 'status'];
}
