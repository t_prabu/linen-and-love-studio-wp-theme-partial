<?php

namespace Models;

class RoomDiscount extends \Models\DefaultModel {
    public $fillable=[ 'room_id', 'hours', 'max_hours', 'percent'];
}