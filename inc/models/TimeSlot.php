<?php

namespace Models;

class TimeSlot extends \Models\DefaultModel {
    public $fillable=[ 'order_number', 'room_id', 'name', 'time_from', 'time_to', 'price' ];
}
