<?php

namespace Models;

class BookingDiscount extends \Models\DefaultModel {
    public $fillable=['booking_id',  'room_id',  'hours',  'percent',  'amount'];
}
