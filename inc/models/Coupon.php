<?php

namespace Models;

class Coupon extends \Models\DefaultModel {
    public $fillable=['code', 'off_percent', 'max_use_count', 'from_at', 'to_at', 'use_count', 'is_display', 'is_active', 'description' ];
}
