<?php

namespace Models;

class Invoice extends \Models\DefaultModel {
    public $fillable=[ 'booking_id', 'status', 'tax', 'sub_total', 'grand_total', 'discount_percent', 'coupon_code',  'gateway_response'];

    public function bookings() {
        return $this->hasMany('Models\Booking', 'invoice_id', 'id');
    }
}
