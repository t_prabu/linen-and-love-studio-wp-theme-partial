<?php
require get_template_directory() .'/vendor/autoload.php';
require 'functions/booking-page-options.php';
require 'functions/post-payment-gateway-transaction-response-api.php';
require 'functions/booking-page-client-api.php';
require 'functions/get-booked-by-monthly.php';

function sanitize_xss($value) {
    return htmlspecialchars(strip_tags($value));
}

/* <div class="container">
    <form action="http://www.studio.com/?rest_route=/payment-gateway/transaction-response/" method="POST">
    <button type="submit" class="btn btn-primary">Action</button>
    </form>
</div> */


function ext_booking_options_ui() {
    if (isset($_POST['online_booking_admin_email'])) {
        update_option('online_booking_admin_email', $_POST['online_booking_admin_email']);
    } ?>
<div class="vm-wrapper">
    <div id="poststuff">
        <div id="post-body" class="metabox-holder columns-2">
            <div id="post-body-content">
                <form method="post" action="" class="js-theme-options-form">
                    <div class="postbox-container">
                        <div class="meta-box-sortables">
                            <div id="product-code" class="postbox ">
                                <h2 class="hndle ui-sortable-handle"><span>Options</span></h2>
                                <div class="inside">
                                    <!--  -->
                                    <div class="form-wrap">
                                        <div class="wp-clearfix">
                                            <div class="form-field">
                                                <div class="wp-clearfix">
                                                    <label for="email"><strong>Emails to Notify</span>
                                                        </strong></label> <input name="online_booking_admin_email"
                                                        value="<?php echo get_option('online_booking_admin_email')  ; ?>"
                                                        placeholder="example@example.com, example@example.com"
                                                        type="text" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--  -->
                                    <?php
    settings_errors();
    settings_fields('online_booking_admin_email');
    do_settings_sections('online_booking_admin_email');
    submit_button(); ?>
                                    <!--  -->
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
}

add_action( 'admin_bar_menu', 'manage_bookings_admin_link', 999 );

function manage_bookings_admin_link( $wp_admin_bar ) {
    $args1 = ['id'    => 'manage-bookings-page-id',
        'title' => 'Manage Booking',
        'href'  => '/manage-bookings',
        'meta'  => [ 'class' => 'dashicons dashicons-calendar-alt' ]];
    $wp_admin_bar->add_node( $args1 );
    $args2 = ['id'    => 'manage-bookings-calendar',
        'title' => 'Booking Calendar',
        'href'  => '/calendar',
        'meta'  => [ 'class' => 'dashicons dashicons-calendar-alt' ]];
    $wp_admin_bar->add_node( $args2 );
}


add_action( 'admin_menu', 'register_booking_pages_page' );

function register_booking_pages_page() {
    add_menu_page( 'Manage Booking', 'Manage Booking', 'edit_pages', site_url( 'manage-bookings' ),  '', 'dashicons-book-alt', 5 );
    
    $rooms = \Models\Room::with(['page'])->get();
     
    /* dd($rooms); */
     
    foreach ($rooms as $key => $room) {
        $page_name = str_replace("book","", $room->page->post_title);
        add_submenu_page( site_url( 'manage-bookings' ),   $page_name.' Page',   $page_name.' Page',
    'edit_pages', site_url('wp-admin/post.php?post='.$room->page->ID.'&action=edit'));
    }
    
        
    /*     add_submenu_page( site_url( 'manage-bookings' ), 'Birch Booking Page', 'Birch Booking Page',
        'edit_pages', site_url('wp-admin/post.php?post=222&action=edit'));
    
        add_submenu_page( site_url( 'manage-bookings' ), 'Fern Booking Page', 'Fern Booking Page',
        'edit_pages', site_url('wp-admin/post.php?post=248&action=edit'));
    
        add_submenu_page( site_url( 'manage-bookings' ), 'Makeup Room Booking Page', 'Makeup Room Booking Page',
        'edit_pages', site_url('wp-admin/post.php?post=542&action=edit')); */
    
    add_submenu_page( site_url( 'manage-bookings' ), 'Calendar', 'Calendar',
    'edit_pages', site_url('wp-admin/post.php?post=437&action=edit'));
    
    add_submenu_page( site_url( 'manage-bookings' ), 'Manage Coupons', 'Manage Coupons',
    'edit_pages', 'booking_coupons.php', 'coupon_page_ui');
	
    add_submenu_page(
         site_url( 'manage-bookings' ),
        'Options',
        'Options',
        'manage_options',
        'ext_booking_options.php',
        'ext_booking_options_ui');
}