<?php

require 'coupon_page.php';

add_action('admin_enqueue_scripts', 'enqueue_booking_page_options_styles_scripts');
add_action( 'admin_enqueue_scripts' , 'register_booking_api_ajax' , 20 );
function enqueue_booking_page_options_styles_scripts() {
    wp_enqueue_style('timepicker-css', get_theme_file_uri('inc/plugins/jonthornton-jquery-timepicker/jquery.timepicker.min.css'), [], '1.0'); 
    
    wp_enqueue_style('tachyons-css', get_theme_file_uri('/inc/styles/tachyons.custom.min.css'), [], '1.0'); 
    
    wp_enqueue_style('admin-custom-css', get_theme_file_uri('/inc/styles/custom.css'), [], '1.0'); 
    
    wp_enqueue_style('bootstrap-datepicker-css', get_theme_file_uri('/plugins/bootstrap-datepicker/css/bootstrap-datepicker.standalone.min.css'), [], '1.0'); 
    
    wp_enqueue_script('moment', get_theme_file_uri('/plugins/moment/moment.min.js'), [], '1.0', false);
     
    wp_enqueue_script('bootstrap-datepicker-js', get_theme_file_uri('/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js'), [], '1.0', true); 
        
    wp_enqueue_script('timepicker-js', get_theme_file_uri('inc/plugins/jonthornton-jquery-timepicker/jquery.timepicker.min.js'), [], '1.0', true);
     
    wp_enqueue_script('admin-custom-js', get_theme_file_uri('/inc/scripts/custom.js'), [], '1.0', true);
    
    wp_enqueue_style('vm-bootstrap', get_theme_file_uri('/inc/styles/vm-wrapper-bootstrap.css'), [], '1.0'); 
       
    wp_enqueue_script('voucher_codes', get_theme_file_uri('/inc/scripts/voucher_codes.min.js'), [], '1.0', true);
    
    wp_enqueue_script('axios', get_theme_file_uri('/plugins/axios/axios.min.js'), [], '1.0', true);
    
    wp_enqueue_script('vue-dev', get_theme_file_uri('/plugins/vue/vue.js'), [], '1.0', true);
    
    // wp_enqueue_script('vue-moment', get_theme_file_uri('/plugins/vue-moment-lib/vue-moment-lib.umd.min.js'), [], '1.0', true);
    
    wp_enqueue_script('admin-coupon-app-js', get_theme_file_uri('/inc/scripts/admin-coupon-app.js'), [], '1.0', true);
}



add_action('admin_init', 'booking_page_options_meta');

function booking_page_options_meta() {
    add_meta_box('booking-page-options', 'Studio Booking Options', 'add_booking_page_options_meta_boxes_ui', 'page', 'normal', 'high');
}

function add_booking_page_options_meta_boxes_ui() {
    global $post;
    $custom_metas = get_post_custom($post->ID);
    $booking_page_options = get_post_meta($post->ID, 'booking-page-options', true);
    $thisRoom = null;
    $thisRoomTimeSlots = null;
    if ($booking_page_options) {
        $thisRoom = \Models\Room::where(['page_id' => $post->ID])->with(['roomDiscounts'])->get()->first();
        $thisRoomTimeSlots = \Models\TimeSlot::where(['room_id' => $thisRoom->id])->orderBy('order_number')->get();
        $thisRoomTimeSlots = $thisRoomTimeSlots->groupBy('name');
    }

    wp_nonce_field(plugin_basename(__FILE__), 'noncename_so_image'); ?>

<div class="vm-wrapper wp-clearfix border-box js-booking-form-wrapper">
    <input type="hidden" name="booking_option_page_id"
        value="<?php echo $post->ID ; ?>">

    <?php if ($booking_page_options): ?>
    <input type="hidden" name="booking_option_prev_status" class="js-booking-form-prev-status" value="false">
    <?php endif; ?>

    <!-- enable disable  -->
    <fieldset class="ba b--transparent ph0 mh0">
        <div class="cf border-box ">
            <div class="fl w-100 w5-ns lh-copy">
                <label class="db f6 fw5">Enable Booking Options</label>
            </div>
            <div class="fl w-100 w4-ns">
                <select name="booking_option_status" class="js-booking-form-status-select" class="w-100 pa0">
                    <option <?php echo $booking_page_options == 'false' ? 'selected' : '' ; ?>
                        value="false">Disabled</option>
                    <option <?php echo $booking_page_options == 'true' ? 'selected' : '' ; ?>
                        value="true">Enabled</option>
                </select>
            </div>
        </div>
    </fieldset>
    <!-- /enable disable  -->

    <div
        class="js-booking-form-options-wrapper border-box <?php echo $booking_page_options !== 'true' ? 'dn' : '' ; ?> ">
        <hr class="mv3">

        <fieldset class="ba b--transparent ph0 mh0">
            <div class="cf border-box ">
                <div class="fl w-100 w5-ns lh-copy">
                    <label class="db f6 fw5">Select Room Type</label>
                </div>
                <div class="fl w-100 w4-ns">
                    <select name="room_type" class="">
                        <option value=""></option>
                        <option <?php echo $thisRoom->type == 'studio' ? 'selected' : '' ; ?>
                            value="studio">Studio</option>
                        <option <?php echo $thisRoom->type == 'makeup_room' ? 'selected' : '' ; ?>
                            value="makeup_room">Makeup Station</option>
                    </select>
                </div>
            </div>
        </fieldset>

        <fieldset class="ba mh0 ph0 b--transparent ">
            <div class="cf border-box ">
                <div class="fl w-100 w5-ns">
                    <label class="db f6 fw5">
                        Max reservations per slot
                        <span class="red"> * </span>
                    </label>
                </div>
                <div class="fl w-100 w4-ns bg-black-10">
                    <input name="slot_max_reservations" class="pa2 ba bg-white w-100" type="number" placeholder="10"
                        value="<?php echo $thisRoom ? $thisRoom->slot_max_reservations : '' ; ?>"
                        required data-validate="number" min="1" step="1" />
                </div>
            </div>
        </fieldset>

        <fieldset class="ba mh0 ph0 b--transparent ">
            <div class="cf border-box ">
                <div class="fl w-100 w5-ns">
                    <label class="db f6 fw5">
                        Tax Percentage
                        <span class="red"> * </span>
                    </label>
                </div>
                <div class="fl w-100 w4-ns bg-black-10">
                    <input name="booking_option_tax_percentage" class="pa2 ba bg-white w-100" type="number"
                        placeholder="10"
                        value="<?php echo $thisRoom ? $thisRoom->tax_percentage : '' ; ?>"
                        required data-validate="number" />
                </div>
            </div>
        </fieldset>

        <div class="ba mh0 ph0 b--transparent ">
            <div class="cf border-box ">
                <div class="cf pb4">
                    <legend class="db f4 fw5 fl w-100 w4-ns ">
                        Discounts
                    </legend>
                    <div class="fl w-100 w4-ns pl3">
                        <button class="f8 button dim dib white bg-blue js-add-discount-field">Add</button>
                    </div>
                </div>
                <div class="js-discount-fields-wrapper">
                    <?php foreach ($thisRoom->roomDiscounts as $discount): ?>
                    <fieldset class="cf border-box pb3 js-discount-field">
                        <input type="hidden" class="js-discount-remove-field"
                            name="discounts[<?php echo $discount->id ; ?>][remove]"
                            value="false">
                        <input type="hidden" class="js-discount-id-field"
                            name="discounts[<?php echo $discount->id ; ?>][id]"
                            value="<?php echo $discount->id ; ?>">
                        <div class="fl w-100 w4-ns">
                            Hours (eg: 1-2 hrs)
                        </div>
                        <div class="fl w-100 w3-ns bg-black-10">
                            <input
                                name="discounts[<?php echo $discount->id ; ?>][hours]"
                                class="pa2 ba bg-white w-100" type="number" placeholder="10"
                                value="<?php echo $discount->hours ; ?>"
                                min="1" required data-validate="number" />
                        </div>
                        <div class="fl w-100 w3-ns bg-black-10"
                            style=" width: 25px; text-align: center; background: #fff; line-height: 32px; ">
                            -
                        </div>
                        <div class="fl w-100 w3-ns bg-black-10">
                            <input
                                name="discounts[<?php echo $discount->id ; ?>][max_hours]"
                                class="pa2 ba bg-white w-100" type="number" placeholder="10"
                                value="<?php echo $discount->max_hours ; ?>"
                                min="1" required data-validate="number" />
                        </div>
                        <div class="fl w-100 w4-ns pl3">
                            Percentage
                        </div>
                        <div class="fl w-100 w3-ns bg-black-10">
                            <input
                                name="discounts[<?php echo $discount->id ; ?>][percent]"
                                class="pa2 ba bg-white w-100" type="number" placeholder="10"
                                value="<?php echo $discount->percent ; ?>"
                                min="1" required data-validate="number" />
                        </div>
                        <div class="fl w-100 w4-ns pl3">
                            <button class="f8 button dim dib white bg-red js-remove-discount-field">Remove</button>
                        </div>
                    </fieldset>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

        <hr>
        <div class="cf mb3">
            <strong class="f5 fw6  dib lh-copy mv3">Daily Time Slots</strong>
            <button type="button"
                class="ba br1 f6 dim br2 ph3 pv2 ml2 dib white bg-dark-blue js-set-week-day-field-to-default">Set
                Default Values</button>
        </div>
        <div class="cf">
            <?php
                $daysArr = [ "monday", "tuesday", "wednesday", "thursday",  "friday" , "saturday" , "sunday" ];
    foreach ($daysArr as $key => $day):
            ?>
            <!--  -->
            <div class="pr3 fl w-50-ns w-100-m js-week-day-wrapper">
                <fieldset class="ba b--black-10 ph2 mh0 pt3 mb4 bg-near-white js-day-fieldset">
                    <input type="hidden"
                        name="days_options[<?php echo $day ; ?>][name]"
                        value="<?php echo $day ; ?>">
                    <legend class="ttc f5 fw6  ba b--gray pv1 ph2 dib bg-white"><?php echo $day ; ?>
                    </legend>
                    <fieldset class="ba mh0 ph0 mb2 b--transparent  js-time-slot-fieldset js-time-slot-fieldset-0">
                        <legend class="f5 mb1">Time Slot 1 <span class="red"> * </span></legend>
                        <div class="cf border-box  ">
                            <div class="fl w-third-ns w-100-m mb2 pa2 bg-black-05">
                                <label class="db lh-copy f6">From <span class="red"> * </span></label>
                                <input
                                    name="days_options[<?php echo $day ; ?>][time_slot][0][from]"
                                    class="pa2 ba bg-white w-100 js-time-from" type="text" placeholder="10:00AM"
                                    value="<?php echo $thisRoomTimeSlots && $thisRoomTimeSlots[$day] ? $thisRoomTimeSlots[$day][0]->time_from : '' ; ?>"
                                    required data-validate="short-time" />
                            </div>
                            <div class="fl w-third-ns w-100-m mb2 pa2 bg-black-05">
                                <label class="db lh-copy f6">To <span class="red"> * </span></label>
                                <input
                                    name="days_options[<?php echo $day ; ?>][time_slot][0][to]"
                                    class="pa2 ba bg-white w-100 js-time-to" type="text" placeholder="1:00PM"
                                    value="<?php echo $thisRoomTimeSlots && $thisRoomTimeSlots[$day] ? $thisRoomTimeSlots[$day][0]->time_to : '' ; ?>"
                                    required data-validate="short-time" />
                            </div>
                            <div class="fl w-third-ns w-100-m mb2 pa2 bg-black-10">
                                <label class="db lh-copy f6">Price Per Hour <span class="red"> * </span></label>
                                <input
                                    name="days_options[<?php echo $day ; ?>][time_slot][0][price]"
                                    class="pa2 ba bg-white w-100 js-per-hr-price" type="number" placeholder="120"
                                    value="<?php echo $thisRoomTimeSlots && $thisRoomTimeSlots[$day] ? $thisRoomTimeSlots[$day][0]->price : '' ; ?>"
                                    required data-validate="number" />
                            </div>
                        </div>

                    </fieldset>

                    <fieldset class="ba mh0 ph0 b--transparent js-time-slot-fieldset js-time-slot-fieldset-1">
                        <legend class="f5 mb1">Time Slot 2 <span class="red"> * </span></legend>
                        <div class="cf border-box  ">
                            <div class="fl w-third-ns w-100-m mb2 pa2 bg-black-05">
                                <label class="db lh-copy f6">From <span class="red"> * </span></label>
                                <input
                                    name="days_options[<?php echo $day ; ?>][time_slot][1][from]"
                                    class="pa2 ba bg-white w-100 js-time-from" type="text" placeholder="2:00PM"
                                    value="<?php echo $thisRoomTimeSlots && $thisRoomTimeSlots[$day] ? $thisRoomTimeSlots[$day][1]->time_from : '' ; ?>"
                                    required data-validate="short-time" />
                            </div>
                            <div class="fl w-third-ns w-100-m mb2 pa2 bg-black-05">
                                <label class="db lh-copy f6">To <span class="red"> * </span></label>
                                <input
                                    name="days_options[<?php echo $day ; ?>][time_slot][1][to]"
                                    class="pa2 ba bg-white w-100 js-time-to" type="text" placeholder="11:00PM"
                                    value="<?php echo $thisRoomTimeSlots && $thisRoomTimeSlots[$day] ? $thisRoomTimeSlots[$day][1]->time_to : '' ; ?>"
                                    required data-validate="short-time" />
                            </div>
                            <div class="fl w-third-ns w-100-m mb2 pa2 bg-black-10">
                                <label class="db lh-copy f6">Price Per Hour <span class="red"> * </span></label>
                                <input
                                    name="days_options[<?php echo $day ; ?>][time_slot][1][price]"
                                    class="pa2 ba bg-white w-100 js-per-hr-price" type="number" placeholder="120"
                                    value="<?php echo $thisRoomTimeSlots && $thisRoomTimeSlots[$day] ? $thisRoomTimeSlots[$day][1]->price : '' ; ?>"
                                    required data-validate="number" />
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="ba mh0 ph0 b--transparent js-time-slot-fieldset js-time-slot-fieldset-1">
                        <legend class="f5 mb1">Time Slot 3 <span class="red"> * </span></legend>
                        <div class="cf border-box  ">
                            <div class="fl w-third-ns w-100-m mb2 pa2 bg-black-05">
                                <label class="db lh-copy f6">From <span class="red"> * </span></label>
                                <input
                                    name="days_options[<?php echo $day ; ?>][time_slot][2][from]"
                                    class="pa2 ba bg-white w-100 js-time-from" type="text" placeholder="2:00PM"
                                    value="<?php echo $thisRoomTimeSlots && $thisRoomTimeSlots[$day] ? $thisRoomTimeSlots[$day][2]->time_from : '' ; ?>"
                                    required data-validate="short-time" />
                            </div>
                            <div class="fl w-third-ns w-100-m mb2 pa2 bg-black-05">
                                <label class="db lh-copy f6">To <span class="red"> * </span></label>
                                <input
                                    name="days_options[<?php echo $day ; ?>][time_slot][2][to]"
                                    class="pa2 ba bg-white w-100 js-time-to" type="text" placeholder="11:00PM"
                                    value="<?php echo $thisRoomTimeSlots && $thisRoomTimeSlots[$day] ? $thisRoomTimeSlots[$day][2]->time_to : '' ; ?>"
                                    required data-validate="short-time" />
                            </div>
                            <div class="fl w-third-ns w-100-m mb2 pa2 bg-black-10">
                                <label class="db lh-copy f6">Price Per Hour <span class="red"> * </span></label>
                                <input
                                    name="days_options[<?php echo $day ; ?>][time_slot][2][price]"
                                    class="pa2 ba bg-white w-100 js-per-hr-price" type="number" placeholder="120"
                                    value="<?php echo $thisRoomTimeSlots && $thisRoomTimeSlots[$day] ? $thisRoomTimeSlots[$day][2]->price : '' ; ?>"
                                    required data-validate="number" />
                            </div>
                        </div>
                    </fieldset>

                </fieldset>
            </div>
            <!--  -->
            <?php endforeach; ?>
        </div>
    </div>
</div>
<!-- script -->
<script>
    //     $('button.editor-post-publish-panel__toggle').length
    // $('button.editor-post-publish-button').length
    jQuery(document).on('mouseover.bookingsClickBob', 'button.editor-post-publish-button', function(e) {
        let thisEl = jQuery(this);
        // e.preventDefault();
        // e.stopPropagation();
        let prevStatus = jQuery('[name=booking_option_prev_status]');
        let bookingOptionStatus = jQuery('[name=booking_option_status]');
        if ((prevStatus.length > 0 && prevStatus.val() == 'true') && bookingOptionStatus.val() == 'true') {
            let bookingOptionsValidate = window.bookingOptionsValidate();
            if (!bookingOptionsValidate) {
                alert('Please check and compleat the booking options.');
                // return false;
            }
        }
        thisEl.off('mouseover.bookingsClickBob');
    });
</script>
<!-- /script -->
<?php
}


add_action('save_post', 'booking_page_save_options');

function booking_page_save_options() {
    remove_action('save_post', __FUNCTION__);

    global $post;
    
    if ($post->post_type != 'page') {
        return;
    }

    // Checks save status
    $is_autosave = wp_is_post_autosave($post_id);
    $is_revision = wp_is_post_revision($post_id);
    $is_valid_nonce = (isset($_POST['prfx_nonce']) && wp_verify_nonce($_POST['prfx_nonce'], basename(__FILE__))) ? 'true' : 'false';
    // Exits script depending on save status
    if ($is_autosave || $is_revision || !$is_valid_nonce) {
        return;
    }
    if ( $_POST['booking_option_prev_status'] && $_POST['booking_option_prev_status'] == 'true' ) {
        update_post_meta($post->ID, "booking-page-options", $_POST['booking_option_status']);
        if ($_POST['booking_option_status']=='true') {
            $thisRoom = \Models\Room::updateOrCreate( ['page_id' => $_POST['booking_option_page_id']], ['type' => $_POST['room_type'], 'slot_max_reservations' => $_POST['slot_max_reservations'], 'status' => $_POST['booking_option_status'], 'tax_percentage' => $_POST['booking_option_tax_percentage']] );
            
            $days_options = $_POST['days_options'];
            foreach ($days_options as $dayKey => $days_option) {
                foreach ($days_option['time_slot'] as $key => $time_slot) {
                    $thisOption = \Models\TimeSlot::updateOrCreate( [
                        'order_number' => $key,
                        'room_id' => $thisRoom->id,
                        'name' => $days_option['name'],
                    ], [
                        'time_from' => $time_slot['from'],
                        'time_to' => $time_slot['to'],
                        'price' => $time_slot['price'] && $time_slot['price'] > 0 ? $time_slot['price'] : null ,
                    ] );
                }
            }
            
            if (isset($_POST['discounts'])) {
                $discounts = $_POST['discounts'];
                $deleteIds = [];
                foreach ($discounts as $discount) {
                    if ($discount['remove'] == 'true') {
                        $deleteIds[] = $discount['id'];
                    } else {
                        if ($discount['id'] !== '') {
                            \Models\RoomDiscount::where('id', $discount['id'])->update(['hours'=> $discount['hours'], 'max_hours'=> $discount['max_hours'], 'percent'=> $discount['percent']]);
                        } else {
                            \Models\RoomDiscount::updateOrCreate(['room_id'=> $thisRoom->id, 'hours'=> $discount['hours'], 'max_hours'=> $discount['max_hours']],['room_id'=> $thisRoom->id, 'hours'=> $discount['hours'], 'max_hours'=> $discount['max_hours'], 'percent'=> $discount['percent']]);
                        }
                    }
                }
                if (count($deleteIds)>0) {
                    \Models\RoomDiscount::whereIn('id', $deleteIds)->delete();
                }
            }
        } else {
            $thisRoom = \Models\Room::where('page_id', $_POST['booking_option_page_id'])->get()->first();
            if ($thisRoom) {
                $thisRoom->update(['status' => false]);
            }
        }
    }
}
