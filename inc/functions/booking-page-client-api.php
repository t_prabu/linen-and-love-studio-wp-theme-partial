<?php

require 'get-booking-page-info.php';
require 'post-payment-pre-auth.php';
require 'post-payment-manual.php';
require 'post-book-as-post-payment.php';
require 'manage-booking-api.php';
require 'manage-booking-coupon-api.php';
require 'invoice-mail-html.php';
require 'get-room-data.php';

function register_booking_api_ajax() {
    wp_register_script( 'booking-ajax', false, [ ], false, false );
    wp_localize_script( 'booking-ajax', 'bookingApiObj', [ 'url' => admin_url( 'admin-ajax.php' ),
        'security' => wp_create_nonce( 'my-special-string' ) ] );
    wp_enqueue_script( 'booking-ajax' );
}
 add_action( 'wp_enqueue_scripts' , 'register_booking_api_ajax' , 20 );
