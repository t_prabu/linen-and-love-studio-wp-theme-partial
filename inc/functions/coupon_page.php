<?php
function coupon_page_ui() {
    ?>
<div class="vm-wrapper-bs" id="booking-coupon-app">

    <div class="container pt-4">
        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6 text-right">
                <button type="button" class="btn btn-primary" @click="onShowNewCouponForm">New Coupon</button>
            </div>

            <!--  -->
            <div class="alert alert-dismissible alert-success" v-show="alert.success.show">
                {{alert.success.message}}
            </div>
            <!--  -->

            <div class="card text-left col-12">
                <div class="card-body" v-if="indexData">
                    <h3 class="card-title text-left">Coupons</h3>

                    <div class="table-responsive">
                        <table class="table table-sm table-hover">
                            <caption>Displaying {{indexData.to}} out of {{indexData.total}}</caption>
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Code</th>
                                    <th scope="col">Off %</th>
                                    <th scope="col">Max Use</th>
                                    <th scope="col">Use Count</th>
                                    <th scope="col">From</th>
                                    <th scope="col">To</th>
                                    <th scope="col">Active</th>
                                    <th scope="col">Display</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(item, index) in indexData.data" :key="index">
                                    <th scope="row">{{indexData.from+index}}</th>
                                    <td>{{moment(item.created_at).format("YYYY-MM-DD, hh:mm A")}}</td>
                                    <td>{{item.code}}</td>
                                    <td>{{item.off_percent}} %</td>
                                    <td>{{item.max_use_count}}</td>
                                    <td>{{item.use_count}}</td>
                                    <td>{{moment(item.from_at).format("YYYY-MM-DD, hh:mm A")}}</td>
                                    <td>{{moment(item.to_at).format("YYYY-MM-DD, hh:mm A")}}</td>
                                    <td class="text-center">
                                        <div v-if="item.is_active == 1"><span class="badge badge-success">yes</span>
                                        </div>
                                        <div v-if="item.is_active == 0"><span class="badge badge-danger">no</span></div>
                                    </td>
                                    <td class="text-center">
                                        <div v-if="item.is_display == 1"><span class="badge badge-success">yes</span>
                                        </div>
                                        <div v-if="item.is_display == 0"><span class="badge badge-danger">no</span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-sm btn-success"
                                            @click="editCoupon(item.id)">Edit</button>
                                    </td>
                                </tr>

                            </tbody>
                        </table>

                        <div class="text-center" v-if="total_pages>1">
                            <nav aria-label="Page navigation">
                                <ul class="pagination">
                                    <li class="page-item" :class={active:item==current_page}
                                        v-for=" (item, index) in total_pages_arr" :key="index"><a class="page-link"
                                            href="#" @click.prevent="changePage(item)">{{item}}</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>

                </div>
            </div>

        </div>





    </div>


    <div class="modal fade show" :class="{'d-block':showNewCouponForm, 'd-none':!showNewCouponForm}">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">New Coupon</h5>
                    <button type="button" class="close p-2" data-dismiss="modal" aria-label="Close"
                        @click="cancelNewCouponForm">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form @submit.prevent="createCouponSubmit" class="js-create-coupon-form">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="code">Coupon Code</label>
                            <input required type="text" class="form-control" readonly name="code"
                                v-model="new_coupon_form.code">
                        </div>

                        <div class="form-group">
                            <label for="description">Coupon Title</label>
                            <input required type="text" class="form-control" name="description"
                                v-model="new_coupon_form.description" aria-describedby="description">
                        </div>

                        <div class="form-group">
                            <label for="max_use_count">Max number of use</label>
                            <input required type="number" class="form-control" name="max_use_count"
                                v-model="new_coupon_form.max_use_count" aria-describedby="max_use_count">
                        </div>

                        <div class="form-group">
                            <label for="off_percent">Off Percentage</label>
                            <input required type="number" class="form-control" name="off_percent"
                                v-model="new_coupon_form.off_percent" aria-describedby="off_percent">
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <label>Coupon valid period.</label>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="from_at"><small>From</small></label>
                                    <input required type="text"
                                        class="form-control js-create-coupon-from-at js-datepicker" name="from_at"
                                        @keydown="setFromDate" aria-describedby="from_at">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="to_at"><small>To</small></label>
                                    <input required type="text"
                                        class="form-control js-create-coupon-to-at js-datepicker" name="to_at"
                                        @keydown="setToDate" aria-describedby="to_at">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input"
                                            v-model="new_coupon_form.is_display">
                                        Display coupon on header.
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input"
                                            v-model="new_coupon_form.is_active">
                                        Coupon active.
                                    </label>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Create</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"
                            @click="cancelNewCouponForm">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade show" :class="{'d-block':showUpdateCouponForm, 'd-none':!showUpdateCouponForm}">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Coupon</h5>
                    <button type="button" class="close p-2" data-dismiss="modal" aria-label="Close"
                        @click="cancelUpdateCouponForm">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form @submit.prevent="updateCouponSubmit" class="js-update-coupon-form">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="code">Coupon Code</label>
                            <input required type="text" class="form-control" readonly id="code"
                                v-model="update_coupon_form.code">
                        </div>

                        <div class="form-group">
                            <label for="description">Coupon Title</label>
                            <input required type="text" class="form-control" name="description"
                                v-model="update_coupon_form.description" aria-describedby="description">
                            <small class="form-text text-muted">Its recommended to keep coupon title 6 words
                                or bellow.</small>
                        </div>

                        <div class="form-group">
                            <label for="max_use_count">Max number of use</label>
                            <input required type="number" class="form-control" id="max_use_count"
                                v-model="update_coupon_form.max_use_count" aria-describedby="max_use_count">
                        </div>

                        <div class="form-group">
                            <label for="off_percent">Off Percentage</label>
                            <input required type="number" class="form-control" id="off_percent"
                                v-model="update_coupon_form.off_percent" aria-describedby="off_percent">
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <label>Coupon valid period.</label>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="from_at"><small>From</small></label>
                                    <input required type="text"
                                        class="form-control js-update-coupon-from-at js-datepicker" id="from_at"
                                        @keydown="setFromDate" aria-describedby="from_at">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="to_at"><small>To</small></label>
                                    <input required type="text"
                                        class="form-control js-update-coupon-to-at js-datepicker" id="to_at"
                                        @keydown="setToDate" aria-describedby="to_at">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input"
                                            v-model="update_coupon_form.is_display">
                                        Display coupon on header.
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input"
                                            v-model="update_coupon_form.is_active">
                                        Coupon active.
                                    </label>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Update</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"
                            @click="cancelUpdateCouponForm">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal-backdrop fade show d-none" :class="{'d-block':isAppLoading, 'd-block': appOverlay}"></div>


</div>



<?php
// add_action('admin_enqueue_scripts', 'enqueue_booking_coupon_page_options_styles_scripts');
}

// function enqueue_booking_coupon_page_options_styles_scripts() {
//     wp_enqueue_style('timepicker-css', get_theme_file_uri('inc/plugins/jonthornton-jquery-timepicker/jquery.timepicker.min.css'), [], '1.0'); 
    
    
    
// }