<?php

add_action( 'wp_ajax_get_rooms_data', 'get_rooms_data' );

add_action( 'wp_ajax_nopriv_get_rooms_data', 'get_rooms_data' );

function get_rooms_data() {
    $roomIds = $_GET['rooms'];

    $rooms = \Models\Room::whereIn('id', $roomIds)->with(['roomDiscounts','page' ])->get();
    $roomsData = [];
    
    foreach ($rooms as $room) {
        $roomsData[$room->id] = $room;
    }

    $response = [
        "status" => "200",
        "message" => "OK",
        "data" => $roomsData ];
       
    header( 'Content-Type: application/json; charset=utf-8' );
    echo json_encode( $response );
    exit;
}


add_action( 'wp_ajax_get_sub_rooms_data', 'get_sub_rooms_data' );

add_action( 'wp_ajax_nopriv_get_sub_rooms_data', 'get_sub_rooms_data' );

function get_sub_rooms_data() {
    $roomIds = $_GET['rooms'];

    $rooms = \Models\Room::whereIn('type', ['makeup_room'])->where('status', 'true')->with(['roomDiscounts','page'])->get();
    $roomsData = [];
    
    foreach ($rooms as $room) {
        $roomsData[$room->id] = $room;
    }

    $response = [
        "status" => "200",
        "message" => "OK",
        "data" => $roomsData ];
       
    header( 'Content-Type: application/json; charset=utf-8' );
    echo json_encode( $response );
    exit;
}


add_action( 'wp_ajax_get_ig_gallery', 'get_ig_gallery' );

add_action( 'wp_ajax_nopriv_get_ig_gallery', 'get_ig_gallery' );

function get_ig_gallery() {

    $response = [
        "status" => "200",
        "message" => "OK",
        "data" => $roomsData ];
       
    header( 'Content-Type: application/json; charset=utf-8' );
    echo file_get_contents( get_template_directory().'/scripts/ig-gallery-min.json' );
    exit;
}
