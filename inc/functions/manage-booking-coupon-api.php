<?php

add_action( 'wp_ajax_manage_booking_coupon_api', 'manage_booking_coupon_api' );

add_action( 'wp_ajax_nopriv_manage_booking_coupon_api', 'manage_booking_coupon_api' );

function manage_booking_coupon_api() {
    
    // Takes raw data from the request
    $json = file_get_contents('php://input');
    // Converts it into a PHP object
    $request = json_decode($json);

    if (!$request) {
        $response = ["status" => "error", "message" => "no payload"];
        header( 'Content-Type: application/json; charset=utf-8' );
        echo json_encode( $response );
        die();
    }
    
    $couponController = new CouponController();
    
    if ($request->route == 'index') {
        $response = [
            "status" => "success", 
            "data" => $couponController->index($request), 
        ];
         
        header( 'Content-Type: application/json; charset=utf-8' );
        echo json_encode( $response );
       
        exit;
    }
    if ($request->route == 'store') {
        $response = [
            "status" => "success", 
            "data" => $couponController->store($request), 
        ];
         
        header( 'Content-Type: application/json; charset=utf-8' );
        echo json_encode( $response );
       
        exit;
    }
    if ($request->route == 'update') {
        $response = [
            "status" => "success", 
            "data" => $couponController->update($request), 
        ];
         
        header( 'Content-Type: application/json; charset=utf-8' );
        echo json_encode( $response );
       
        exit;
    }
    if ($request->route == 'edit') {
        $response = [
            "status" => "success", 
            "data" => $couponController->edit($request), 
        ];
         
        header( 'Content-Type: application/json; charset=utf-8' );
        echo json_encode( $response );
       
        exit;
    }
    if ($request->route == 'check_coupon_code') {
        $response = [
            "status" => "success", 
            "data" => $couponController->check_coupon_code($request), 
        ];
         
        header( 'Content-Type: application/json; charset=utf-8' );
        echo json_encode( $response );
       
        exit;
    }
    if ($request->route == 'get_coupon_code') {
        $response = [
            "status" => "success", 
            "data" => $couponController->get_coupon_code($request), 
        ];
         
        header( 'Content-Type: application/json; charset=utf-8' );
        echo json_encode( $response );
       
        exit;
    }
}


class CouponController {
    public function index($request) {
        $coupon = null;
        /*       if ($request->page_id) {
                  $coupon = \Models\Coupon::whereHas('bookings.room', function($q) use ($request) {
                      $q->where('page_id', $request->page_id);
                  })->orderBy('id', 'desc')->paginate($request->per_page ? $request->per_page : 10 ,['*'], 'page', $request->page);
              } else {
                  $coupon = \Models\Coupon::orderBy('id', 'desc')->paginate($request->per_page ? $request->per_page : 10 ,['*'], 'page', $request->page);
              } */
        $coupon = \Models\Coupon::orderBy('id', 'desc')->paginate($request->per_page ? $request->per_page : 10 ,['*'], 'page', $request->page);
        return $coupon;
    }
    
    public function edit($request) {
        $coupon = \Models\Coupon::where('id', $request->id)->get()->first();
        return $coupon;
    }
    
    public function store($request) {
        $coupon = \Models\Coupon::create(['code' => $request->code , 'description' => $request->description, 'off_percent' => $request->off_percent , 'max_use_count' => $request->max_use_count , 'from_at' => $request->from_at , 'to_at' => $request->to_at , 'use_count' => 0,  'is_active' => $request->is_active ? 1 : 0,'is_display' => $request->is_display ? 1 : 0]);
        return $coupon;
    }
    
    public function update($request) {
        $coupon = \Models\Coupon::where(['code' => $request->code])->update(['description' => $request->description, 'off_percent' => $request->off_percent , 'max_use_count' => $request->max_use_count , 'from_at' => $request->from_at , 'to_at' => $request->to_at, 'is_active' => $request->is_active ? 1 : 0,'is_display' => $request->is_display ? 1 : 0]);
        return $coupon;
    }
    
    
    public function check_coupon_code($request) {
        $coupon = \Models\Coupon::where(['code' => $request->code])->get()->first();
        return $coupon;
    }
    
    public function get_coupon_code($request) {
        $coupon = \Models\Coupon::where(['code' => $request->code])->get()->first();
        return $coupon;
    }
}
