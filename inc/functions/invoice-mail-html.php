<?php

function invoice_html($invoice) {
    ob_start(); ?>
<!doctype html>
<html>

<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" />
    <title>Invoice - Linen and Love – Studios</title>
    <!-- <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/styles/mail.css">
    -->
    <style>
        <?php echo file_get_contents(get_template_directory().'/styles/mail.css') ; ?>
    </style>
</head>

<body class="respond" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
    <div class="container pt-4">
        <!--  -->
        <div class="invoice-wrapper">
            <div id="invoice">
                <div class="invoice overflow-auto">
                    <div>
                        <header>
                            <div class="row">
                                <div class="col">
                                    <a target="_blank"
                                        href="<?php echo site_url() ; ?>">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png"
                                            class="img-fluid mb-2" data-holder-rendered="true" />
                                    </a>
                                </div>
                                <div class="col company-details">
                                    <div>155 Mcintosh Dr. Unit 10 Markham Ontario L3R0N6</div>
                                    <div>905-604-0221</div>
                                    <div><?php echo get_option('online_booking_admin_email') ;?></div>
                                </div>
                            </div>
                        </header>
                        <main>
              <div class="row contacts">
                <div class="col invoice-to">
                  <div class="text-gray-light ">INVOICE TO:</div>
                  <h2 class="to"><?php echo $invoice->bookings->first()->user_name ;?>
                  </h2>
                  <div class="address"><?php echo $invoice->bookings->first()->user_address ;?>
                  </div>
                  <div class="address"><?php echo $invoice->bookings->first()->user_phone ;?>
                  </div>
                  <div class="email"><?php echo $invoice->bookings->first()->user_email ;?>
                  </div>
                </div>
                <div class="col invoice-details">
                  <h2 class="invoice-id text-right">INVOICE #<?php echo $invoice->id ;?>
                  </h2>
                  <div class="date">Date of Invoice: <?php echo date("Y-m-d", strtotime($invoice->created_at)) ;?>
                  </div>

                  <div class="date">Payment Status:
                    <?php if ($invoice->status == 'approved' ): ?>
                    <div class="d-inline text-strong text-success">Approved</div>
                    <?php endif; ?>
                    <?php if ($invoice->status == 'payment_pending' ): ?>
                    <div class="d-inline text-strong text-success">Payment Pending</div>
                    <?php endif; ?>
                    <?php if ($invoice->status !== 'approved' && $invoice->status !== 'payment_pending' ): ?>
                    <div class="d-inline text-strong text-danger">Declined</div>
                    <?php endif; ?>
                  </div>
                  <?php if ($invoice->gateway_response): ?>
                  <?php 
                  $payment_method = json_decode($invoice->gateway_response);
                  ?>
                  <?php if (isset($payment_method->type)):  ?>
                  <div class="date text-capitalize">Payment Type: <span class="text-success"><?php echo $payment_method->type ;?></span>
                  </div>
                  <?php endif; ?>
                  <?php endif; ?>
                </div>
              </div>
              <?php foreach ( $invoice->bookings as $key =>  $booking ): ?>
              <table border="0" cellspacing="0" cellpadding="0" style="margin-bottom:0;">
                <?php if ($key == 0):?>
                <thead>
                  <tr>
                    <th class="no text-center" style="width:50px;">#</th>
                    <th class="text-left" style="width:200px;">Studio Name</th>
                    <th class="text-left">Date</th>
                    <th class="text-center" style="width:100px;">Time Slot</th>
                    <th class="text-right" style="width:200px;">Price</th>
                  </tr>
                </thead>
                <?php endif;?>
                <?php $tabIndex = 0; ?>
                <tbody>
                  <?php 
                  $roomBookings = $booking->roomBooking->sortBy('time_from');
                  foreach (  $roomBookings as  $item ): ?>
                  <?php  $tabIndex++ ;  ?>
                  <tr>
                    <td class="no text-center" style="width:50px;"><?php echo $tabIndex ;?>
                    </td>
                    <td class="text-left" style="width:200px;">
                      <h3>
                        <?php echo str_replace('Book', '', $booking->room->page->post_title)  ;?>
                      </h3>
                    </td>
                    <td class="text-left">
                      <h3>
                        <?php echo $item->booked_date  ;?>
                      </h3>
                    </td>
                    <td class="unit text-cente" style="width:200px;"><?php echo date("g:i A", strtotime($item->time_from)) ;?>
                      to <?php echo date("g:i A", strtotime($item->time_to)) ;?>
                    </td>
                    <td class="total" style="width:200px;">$ <?php echo $item->price ;?>
                      CAD</td>
                  </tr>
                  <?php endforeach; ?>
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="3"></td>
                    <td>Sub Total</td>
                    <td>$
                      <?php 
                      $sub_total = 0;
                      $roomBookings = $booking->roomBooking->sortBy('time_from');
                      foreach ($roomBookings as $roomBooking) {
                          $sub_total += $roomBooking->price;
                      }
                      echo $sub_total;
                      ?>
                    </td>
                  </tr>
                  <?php if ($booking->discount_percent > 0 ): ?>
                  <tr>
                    <td colspan="3"></td>
                    <td>Discount (<?php echo  $booking->discount_percent; ?>%) </td>
                    <td>
                      -<?php echo number_format( ($sub_total*$booking->discount_percent/100), 2 , '.', '')  ;?>
                    </td>
                  </tr>
                  <?php endif; ?>

                  <tr style="display:none;"></tr>
                  <?php if ($key == $invoice->bookings->count()-1):?>
                  <?php if ($invoice->coupon_code):?>
                  <tr>
                    <td colspan="3"></td>
                    <td>Promo <?php echo $invoice->coupon_code ;?>
                      (<?php echo $invoice->discount_percent ;?>%)
                    </td>
                    <td> -<?php echo (($invoice->sub_total*$invoice->discount_percent/100)) ;?>
                    </td>
                  </tr>
                  <?php endif;?>
                  <tr>
                    <td colspan="3"></td>
                    <td>Tax (13%)</td>
                    <?php if ($invoice->coupon_code):?>
                    <td> <?php echo  number_format( (($invoice->sub_total-($invoice->sub_total*$invoice->discount_percent/100))*13/100), 2 , '.', '') ;?>
                    </td>
                    <?php endif;?>
                    <?php if (!$invoice->coupon_code):?>
                    <td> <?php echo number_format( ($invoice->sub_total*13/100), 2 , '.', '') ;?>
                    </td>
                    <?php endif;?>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="3"></td>
                    <td>GRAND TOTAL</td>
                    <td>$ <?php echo number_format($invoice->grand_total, 2 , '.', ''); ?>
                      CAD
                    </td>
                  </tr>
                  <?php endif;?>
                </tfoot>
              </table>
              <?php endforeach; ?>

              <!-- <div class="thanks">Thank you!</div> -->
              <div>
                <div class="text-gray-light" style="font-size:18px;"><span>BOOKING DETAILS : </span></div>
                <div>
                  Photographer Name : <strong><?php echo $invoice->bookings->first()->photographer_name ;?></strong>
                </div>
                <div>
                  Type of Shoot : <strong><?php echo $invoice->bookings->first()->type_of_shoot ;?></strong>
                </div>
                <div>
                  <p>
                    Note : <?php echo nl2br($invoice->bookings->first()->user_note) ;?>
                  </p>
                </div>
              </div>
              <div class="notices">
                <div>NOTICE:</div>
                <div class="notice">
                  Please contact our office for any alterations or assistance.
                </div>
              </div>
            </main>
                        <footer>
                            Invoice was created on a computer and is valid without the signature and seal.
                        </footer>
                    </div>
                    <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
                    <div></div>
                </div>
            </div>
        </div>
        <!--  -->
    </div>
</body>

</html>
<?php
     return ob_get_clean();
}