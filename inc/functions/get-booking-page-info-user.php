<?php

add_action( 'wp_ajax_get_booking_page_info_user', 'get_booking_page_info_user' );

add_action( 'wp_ajax_nopriv_get_booking_page_info_user', 'get_booking_page_info_user' );

function get_booking_page_info_user() {
    $pageId = $_GET['page_id'];
    $bookingsFrom = $_GET['bookings_from'];
    $bookingsTo = $_GET['bookings_to'];
    
    $room = \Models\Room::where('page_id', $pageId)->with(['roomDiscounts'])->get()->first();
    
    $roomTimeSlot = \Models\TimeSlot::where('room_id', $room->id)->get()->groupBy('name');

    $roomBookings = \Models\RoomBooking::where([['room_id', $room->id], ['status', 'confirmed']])->whereBetween('time_from', [$bookingsFrom, $bookingsTo])->get()->groupBy('booked_date');
        
    // $room = \Models\Room::where('page_id', $pageId)->with(['timeSlots', 'roomBookings' => function($query) use ($bookingsFrom, $bookingsTo) {
    //     if ($bookingsFrom && $bookingsTo) {
    //         $query->whereBetween('time_from', [$bookingsFrom, $bookingsTo]);
    //     }
    // } ])->get()->first();
    
    $response = [
        "status" => "200",
        "message" => "OK",
        "data" => [
            "room_info" => $room,
            "room_time_slots" => $roomTimeSlot,
            "room_booking_info" => $roomBookings
        ]  ];
       
    header( 'Content-Type: application/json; charset=utf-8' );
    echo json_encode( $response );
    exit;
}
