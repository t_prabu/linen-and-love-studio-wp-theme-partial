<?php

add_action( 'wp_ajax_get_booked_by_monthly', 'get_booked_by_monthly' );

add_action( 'wp_ajax_nopriv_get_booked_by_monthly', 'get_booked_by_monthly' );

function get_booked_by_monthly() {

    $bookingsFrom = $_GET['from_at'];
    $bookingsTo = $_GET['to_at'];
  
   
    // $rooms_info = \Models\Room::with(['page'])->get()->groupBy('id');
    $rooms_info = \Models\Room::with(['page:ID,post_title'])->get();
    $trans_rooms_info = [];
    
    foreach ($rooms_info as $key => $room_info) {
        $trans_rooms_info[$room_info->id] = $room_info;
    }

    $bookingIds = [];

    $roomBookings = \Models\RoomBooking::where([['status', 'confirmed']])->whereBetween('time_from', [$bookingsFrom, $bookingsTo])->get()->sortBy('time_from')->groupBy('booked_date');
    
    // dd($roomBookings);
    
    $grouped_by_booked_date = [];
  
    foreach ($roomBookings as $dateKey => $roomBooking) {
        
         $grouped_by_booking_id = $roomBooking->sortBy('time_from')->groupBy('booking_id');    
         
         foreach ($grouped_by_booking_id as $key => $booking) {
            $bookingIds[] = $key;
         }
         
         foreach ($grouped_by_booking_id as $key => $booking) {
            // $grouped_by_booking_id = $booking->sortBy('booking_id')->groupBy('room_id');
            $grouped_by_booking_id = $booking->sortBy('time_from')->groupBy('room_id');
            $grouped_by_booked_date[$dateKey][$key] = $grouped_by_booking_id;
         }        

        //  $grouped_by_booked_date[$dateKey] = $grouped_by_booking_id;
    }
    // dd($grouped_by_booked_date['2020-01-25']);
    
    $bookings_info = \Models\Booking::whereIn('id',  $bookingIds)->get();
    
    $trans_bookings_info = [];
    
    foreach ($bookings_info as $key => $booking_info) {
       $trans_bookings_info[$booking_info->id] =  $booking_info;
    }
    

    $res_data =  [
            "rooms_info" => $trans_rooms_info,
            "bookings_info" => $trans_bookings_info,
            "room_booking_info" => $grouped_by_booked_date
        ];
        
        // dd($res_data['room_booking_info']['2020-02-08']);
    
    $response = [
        "status" => "200",
        "message" => "OK",
        "data" => $res_data ];
       
    header( 'Content-Type: application/json; charset=utf-8' );
    echo json_encode( $response );
    exit;
}
