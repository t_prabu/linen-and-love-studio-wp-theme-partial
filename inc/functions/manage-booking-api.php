<?php

add_action( 'wp_ajax_manage_booking_api', 'manage_booking_api' );

add_action( 'wp_ajax_nopriv_manage_booking_api', 'manage_booking_api' );

function manage_booking_api() {
    
    // Takes raw data from the request
    $json = file_get_contents('php://input');
    // Converts it into a PHP object
    $request = json_decode($json);

    if (!$request) {
        $response = ["status" => "error", "message" => "no payload"];
        header( 'Content-Type: application/json; charset=utf-8' );
        echo json_encode( $response );
        die();
    }
    
    $bookingController = new BookingController();
    
    if ($request->route == 'index') {
        $response = [
            "status" => "success", 
            "data" => $bookingController->index($request), 
        ];
         
        header( 'Content-Type: application/json; charset=utf-8' );
        echo json_encode( $response );
       
        exit;
    }
    if ($request->route == 'approve') {
        $response = [
            "status" => "success", 
            "data" => $bookingController->approve($request), 
        ];
         
        header( 'Content-Type: application/json; charset=utf-8' );
        echo json_encode( $response );
       
        exit;
    }
    if ($request->route == 'cancel') {
        $response = [
            "status" => "success", 
            "data" => $bookingController->cancel($request), 
        ];
         
        header( 'Content-Type: application/json; charset=utf-8' );
        echo json_encode( $response );
       
        exit;
    }
    if ($request->route == 'update-time-slot-status') {
        $response = [
            "status" => "success", 
            "data" => $bookingController->update_time_slot_status($request), 
        ];
         
        header( 'Content-Type: application/json; charset=utf-8' );
        echo json_encode( $response );
       
        exit;
    }
    
    
    $booking = \Models\Booking::create([ 'room_id' => $data->booking->room_id , 'user_id' => $data->booking->user_id , 'user_name' => sanitize_xss($data->booking->user_name) , 'user_email' => sanitize_xss($data->booking->user_email) , 'user_phone' => sanitize_xss($data->booking->user_phone) , 'user_address' => sanitize_xss($data->booking->user_address) , 'user_note' => sanitize_xss($data->booking->user_note) , 'photographer_name' => sanitize_xss($data->booking->photographer_name) , 'type_of_shoot' => sanitize_xss($data->booking->type_of_shoot) ]);
        
    $invoice = \Models\Invoice::create([ 'booking_id'=> $booking->id, 'status'=>$data->invoice->status, 'tax'=>$data->invoice->tax, 'sub_total'=>$data->invoice->sub_total, 'grand_total'=>$data->invoice->grand_total]);

    $roomBookingArr = [];
    
    $response = [
        "status" => "success", 
    ];
         
    header( 'Content-Type: application/json; charset=utf-8' );
    echo json_encode( $response );
       
    exit;
}


class BookingController {
    public function index($request) {
        $order = null;
        // if (count($request->all())>0) {
        //     $filterData = $request->all();
        //     foreach ($filterData as $key => $data) {
        //         if (!$data || $key == 'page' || $key == 'per_page' || $key == 'created_at') {
        //             unset($filterData[$key]);
        //         } else {
        //             if ($key == 'customer_name') {
        //                 $filterData[] = [
        //                     $key,
        //                     'like',
        //                     $filterData[$key].'%',
        //                 ];
        //                 unset($filterData[$key]);
        //             }

        //         }
        //     }
        //     if ($request->created_at) {
        //         $order = Order::where($filterData)->orderBy('id', 'desc')->whereDate('created_at', $request->created_at)->with(['customer', 'country', 'invoice', 'orderItems.product:id,product_code', 'address.country'])->paginate($request->per_page ? $request->per_page : 15);
        //     } else {
        //         $order = Order::where($filterData)->orderBy('id', 'desc')->with(['customer', 'country', 'invoice', 'orderItems.product:id,product_code', 'address.country'])->paginate($request->per_page ? $request->per_page : 15);
        //     }
        // } else {
        $invoice = null;
        if ($request->page_id) {
            $invoice = \Models\Invoice::whereHas('bookings.room', function($q) use ($request) {
                $q->where('page_id', $request->page_id);
            })->with(['bookings.room.page:ID,post_title', 'bookings.roomBooking'])->orderBy('id', 'desc')->paginate($request->per_page ? $request->per_page : 10 ,['*'], 'page', $request->page);
        } else {
            $invoice = \Models\Invoice::with(['bookings.room.page:ID,post_title', 'bookings.roomBooking'])->orderBy('id', 'desc')->paginate($request->per_page ? $request->per_page : 10 ,['*'], 'page', $request->page);
        }
        // }

        return $invoice;
    }
    public function approve($request) {
        $response_status = 'approved';
        $room_status = 'confirmed';  
        $invoice = \Models\Invoice::where('id', $request->invoice_id)->with(['bookings.roomBooking'])->get()->first();
        $invoice->bookings->each(function ($booking, $key) use ($room_status) {
            // $booking->roomBooking->update(['status' => $room_status]);
            $roomBooking = \Models\RoomBooking::where('booking_id', $booking->id)->update(['status' => $room_status]);
        });
        $invoice->update(['status' => $response_status]);

        return $invoice;
    }

    public function cancel($request) {
        $response_status = 'canceled';
        $room_status = 'canceled';
        $invoice = \Models\Invoice::where('id', $request->invoice_id)->with(['bookings.roomBooking'])->get()->first();
        $invoice->bookings->each(function ($booking, $key) use ($room_status) {
            // $booking->roomBooking->update(['status' => $room_status]);
            $roomBooking = \Models\RoomBooking::where('booking_id', $booking->id)->update(['status' => $room_status]);
        });
        $invoice->update(['status' => $response_status]);

        return $invoice;
    }
    public function update_time_slot_status($request) {
        $data = $request->data;
        $status = $data->reserved ? 'confirmed' : 'canceled';
                
        $roomBookingAll = \Models\RoomBooking::where(['time_from'=>$data->time_from, 'room_id'=>$data->room_id])->get(); 
        $roomBooking = $roomBookingAll->get($data->index); 
        
        if ($roomBooking) {
            $roomBooking->update(['status' => $status]);
        } else {
            $roomBookingArr = ['room_id'=> $data->room_id, 'booked_date'=>$data->booked_date, 'time_from'=>$data->time_from, 'time_to'=>$data->time_to, 'price'=>$data->price, 'status' => $status];
            $roomBooking = \Models\RoomBooking::create($roomBookingArr);
        }
        // $roomBooking = \Models\RoomBooking::where('booked_date', $data->booked_date)->update(['status' => $status]);      
        return $roomBooking;
    }
}
