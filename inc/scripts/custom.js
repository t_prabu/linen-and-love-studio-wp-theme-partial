window.bookingOptionsValidate = function () {
  let $ = jQuery;
  let parent = $(".js-booking-form-options-wrapper");
  let validationFields = parent.find("[data-validate]");
  let notFoundError = true;

  $.each(validationFields, function (i, el) {
    let thisEl = $(el);

    if (thisEl.data("validate") == "number") {
      if (!/[0-9]{1}/.test(thisEl.val())) {
        thisEl.addClass("form-input--error");
        notFoundError = false;
        console.log(thisEl);
      } else {
        thisEl.removeClass("form-input--error");
      }
    } else if (thisEl.data("validate") == "short-time") {
      if (!/\d{1,2}\:\d{1,2} [PM|AM]{2}/.test(thisEl.val())) {
        thisEl.addClass("form-input--error");
        notFoundError = false;
        console.log(thisEl);
      } else {
        thisEl.removeClass("form-input--error");
      }
    }
  });

  return notFoundError;
};

function bookingOptionValidate(el) {
  let $ = jQuery;
  let thisEl = $(el);
  let parent = $(".js-booking-form-options-wrapper");
  let notFoundError = true;
  function getTwentyFourHourTime(amPmString) {
    let tempTime = amPmString;
    let d = new Date("1/1/2013 " + tempTime);
    return d.getHours() * 60 + d.getMinutes();
  }

  if (thisEl.data("validate") == "number") {
    if (!/[0-9]{1}/.test(thisEl.val())) {
      thisEl.addClass("form-input--error");
      notFoundError = false;
    } else {
      thisEl.removeClass("form-input--error");
    }
  } else if (thisEl.data("validate") == "short-time") {
    if (!/\d{1,2}\:\d{1,2} [PM|AM]{2}/.test(thisEl.val())) {
      thisEl.addClass("form-input--error");
      notFoundError = false;
    } else {
      if (thisEl.hasClass("js-time-from") || thisEl.hasClass("js-time-to")) {
        let thisSibClass = thisEl.hasClass("js-time-from")
          ? ".js-time-to"
          : ".js-time-from";
        let thisSib = thisEl
          .closest(".js-time-slot-fieldset")
          .find(thisSibClass);
        let thisSibVal = thisSib.val();

        if (thisEl.hasClass("js-time-from")) {
          thisSibVal =
            thisSibVal == ""
              ? getTwentyFourHourTime("11:59PM")
              : getTwentyFourHourTime(thisSibVal);
          let thisFieldTime = getTwentyFourHourTime(thisEl.val());
          let thisSibFieldTime = thisSibVal;

          if (!(thisFieldTime < thisSibFieldTime)) {
            thisEl.addClass("form-input--error");
            notFoundError = false;
          }
        }
        if (thisEl.hasClass("js-time-to")) {
          thisSibVal =
            thisSibVal == ""
              ? getTwentyFourHourTime("12:00 AM")
              : getTwentyFourHourTime(thisSibVal);
          let thisFieldTime = getTwentyFourHourTime(thisEl.val());
          let thisSibFieldTime = thisSibVal;

          if (!(thisFieldTime > thisSibFieldTime)) {
            thisEl.addClass("form-input--error");
            notFoundError = false;
          }
        }
      }
    }
  }
  if (notFoundError) {
    thisEl.removeClass("form-input--error");
  }
}

(function ($) {
  "use strict";

  $(".js-booking-form-status-select").on("change", function () {
    let thisEl = $(this);
    let parent = thisEl.closest(".js-booking-form-wrapper");
    let optionsWrapper = parent.find(".js-booking-form-options-wrapper");

    let prevStatusHtmlStr =
      '<input type="hidden" name="booking_option_prev_status" class="js-booking-form-prev-status" value="true">';

    if (parent.find(".js-booking-form-prev-status").length < 1) {
      parent.append(prevStatusHtmlStr);
    } else {
      parent.find(".js-booking-form-prev-status").val("true");
    }

    if (thisEl.val() == "true") {
      optionsWrapper.removeClass("dn");
    } else if (thisEl.val() == "false") {
      optionsWrapper.addClass("dn");
    }
  });

  $(".js-booking-form-options-wrapper").on("change", "input,select", function () {
    let thisEl = $(this);
    let parent = thisEl.closest(".js-booking-form-wrapper");
    let optionsWrapper = parent.find(".js-booking-form-options-wrapper");
    let prevStatusHtmlStr =
      '<input type="hidden" name="booking_option_prev_status" class="js-booking-form-prev-status" value="true">';
    if (parent.find(".js-booking-form-prev-status").length < 1) {
      parent.append(prevStatusHtmlStr);
    } else {
      parent.find(".js-booking-form-prev-status").val("true");
    }
    // window.bookingOptionsValidate();
  });

  $(".js-booking-form-options-wrapper").on("click", "button", function () {
    let thisEl = $(this);
    let parent = thisEl.closest(".js-booking-form-wrapper");
    let optionsWrapper = parent.find(".js-booking-form-options-wrapper");
    let prevStatusHtmlStr =
      '<input type="hidden" name="booking_option_prev_status" class="js-booking-form-prev-status" value="true">';
    if (parent.find(".js-booking-form-prev-status").length < 1) {
      parent.append(prevStatusHtmlStr);
    } else {
      parent.find(".js-booking-form-prev-status").val("true");
    }
    // window.bookingOptionsValidate();
  });

  $(".js-booking-form-options-wrapper").on(
    "keyup change",
    "input",
    function () {
      let thisEl = $(this);

      console.log(thisEl.val());

      bookingOptionValidate(thisEl);
    }
  );

  $(".js-time-from").timepicker({ timeFormat: "g:i A" });
  $(".js-time-to").timepicker({ timeFormat: "g:i A" });

  $(".js-set-week-day-field-to-default").on("click", function () {
    setWeekDaysDefaultValues();
  });

  $(".js-add-discount-field").on("click", function () {
    addDiscountField();
  });
  $(".js-discount-fields-wrapper").on(
    "click",
    ".js-remove-discount-field",
    function () {
      removeDiscountField(this);
    }
  );
})(jQuery);

function removeDiscountField(el) {
  let $ = jQuery;
  let thisEl = $(el);
  let parent = thisEl.closest(".js-discount-field");

  if (parent.find(".js-discount-id-field").val()) {
    parent.find(".js-discount-remove-field").val("true");
    parent.addClass("dn");
  }
  if (!parent.find(".js-discount-id-field").val()) {
    parent.remove();
  }
}

function addDiscountField() {
  let $ = jQuery;
  let parent = $(".js-discount-fields-wrapper");
  let id = (new Date().getTime() + "").slice(9);
  let html = `<fieldset class="cf border-box pb3 js-discount-field">
                        <input type="hidden" class="js-discount-remove-field" name="discounts[${id}][remove]" value="false">
                        <input type="hidden" class="js-discount-id-field" name="discounts[${id}][id]" value="">
                        <div class="fl w-100 w4-ns">
                            Hours (eg: 1-2 hrs)
                        </div>
                        <div class="fl w-100 w3-ns bg-black-10">
                            <input name="discounts[${id}][hours]" class="pa2 ba bg-white w-100" type="number" placeholder="10"
                                value="" min="1" required data-validate="number" />
                        </div>
                        <div class="fl w-100 w3-ns bg-black-10" style=" width: 25px; text-align: center; background: #fff; line-height: 32px; ">
                            -
                        </div>
                        <div class="fl w-100 w3-ns bg-black-10">
                            <input name="discounts[${id}][max_hours]" class="pa2 ba bg-white w-100" type="number" placeholder="10"
                                value="" min="1" required data-validate="number" />
                        </div>
                        <div class="fl w-100 w4-ns pl3">
                            Percentage
                        </div>
                        <div class="fl w-100 w3-ns bg-black-10">
                            <input name="discounts[${id}][percent]" class="pa2 ba bg-white w-100" type="number"
                                placeholder="10" value="" min="1" required data-validate="number" />
                        </div>
                        <div class="fl w-100 w4-ns pl3">
                            <button class="f8 button dim dib white bg-red js-remove-discount-field">Remove</button>
                        </div>
                    </fieldset>`;
  parent.append(html);
}

function setWeekDaysDefaultValues() {
  let $ = jQuery;
  let weekDaysWrapper = $(".js-week-day-wrapper");
  let defaultData = [
    {
      time_from: "12:00 AM",
      time_to: "7:00 AM",
      price: 140,
    },
    {
      time_from: "8:00 AM",
      time_to: "8:00 PM",
      price: 80,
    },
    {
      time_from: "9:00 PM",
      time_to: "11:00 PM",
      price: 140,
    },
  ];
  weekDaysWrapper.each(function () {
    let thisEl = $(this);
    let thisElFieldSet = thisEl.find("fieldset:eq(0)");
    let thisElSubFieldSet = thisElFieldSet.find("fieldset");
    let counter = 0;
    thisElSubFieldSet.each(function () {
      let thisEl = $(this);
      thisEl.find(".js-time-from").val(defaultData[counter].time_from);
      thisEl.find(".js-time-to").val(defaultData[counter].time_to);
      thisEl.find(".js-per-hr-price").val(defaultData[counter].price);
      counter += 1;
    });
  });
}
