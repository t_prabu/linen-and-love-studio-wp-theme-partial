function bookingCouponAppInit() {
  if (!document.querySelector("#booking-coupon-app")) {
    return;
  }

  jQuery(function() {
    jQuery(".js-datepicker").datepicker({});
  });

  new Vue({
    el: "#booking-coupon-app",
    data: {
      indexData: null,
      current_page: 1,
      per_page: 10,
      total_pages: 1,
      selected_coupon_id: null,
      isAppLoading: false,
      showNewCouponForm: false,
      showUpdateCouponForm: false,
      appOverlay: false,
      new_coupon_form: {
        code: null,
        max_use_count: null,
        off_percent: null,
        from_at: null,
        to_at: null,
        is_display: false,
        is_active: true,
        description: null,
      },
      update_coupon_form: {
        code: null,
        max_use_count: null,
        off_percent: null,
        from_at: null,
        to_at: null,
        is_display: false,
        is_active: true,
        description: null,
      },
      alert: {
        success: {
          show: false,
          message: false
        },
        danger: {
          show: false,
          message: false
        }
      }
    },
    mounted() {
      this.isAppLoading = false;
      this.getIndex();
    },
    created() {},
    methods: {
      showSuccess(message) {
        this.alert.success.show = true;
        this.alert.success.message = message;
        setTimeout(() => {
          this.alert.success.show = false;
        }, 5000);
      },
      showDanger(message) {
        this.alert.danger.show = true;
        this.alert.danger.message = message;
        setTimeout(() => {
          this.alert.danger.show = false;
        }, 5000);
      },
      changePage(int) {
        this.current_page = int;
        this.getIndex();
      },
      editCoupon(id) {
        this.selected_coupon_id = id;
        this.getCoupon();
        this.showUpdateCouponForm = true;
        this.appOverlay = true;
      },
      checkCouponCode(code) {
        /*  */
        this.isAppLoading = true;
        axios({
          method: "POST",
          url: bookingApiObj.url,
          params: {
            action: "manage_booking_coupon_api",
            security: bookingApiObj.security
          },
          data: {
            route: "check_coupon_code",
            code: code
          }
        })
          .then(res => {
            this.isAppLoading = false;
            if (res.data.data) {
              this.new_coupon_form.code = this.generateCouponCode();
            }
          })
          .catch(err => {
            this.isAppLoading = false;
            console.log(err);
            this.showDanger("Oops! something went wrong please try again");
          });
        /*  */
      },
      generateCouponCode() {
        let code = null;
        code = voucher_codes.generate({
          length: 3,
          prefix: "LAL-",
          charset: "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
          count: 1
        });
        this.checkCouponCode(code[0]);
        return code[0];
      },
      createCouponSubmit() {
        if (!document.querySelector(".js-create-coupon-form").checkValidity()) {
          return false;
        }
        /*  */
        this.isAppLoading = true;
        let from_at = moment(
          document.querySelector(".js-create-coupon-from-at").value,
          "MM/DD/YYYY HH:MM:SS"
        ).format("YYYY-MM-DD HH:mm:ss");
        let to_at = moment(
          document.querySelector(".js-create-coupon-to-at").value,
          "MM/DD/YYYY HH:MM:SS"
        ).format("YYYY-MM-DD HH:mm:ss");
        axios({
          method: "POST",
          url: bookingApiObj.url,
          params: {
            action: "manage_booking_coupon_api",
            security: bookingApiObj.security
          },
          data: {
            route: "store",
            code: this.new_coupon_form.code,
            off_percent: this.new_coupon_form.off_percent,
            max_use_count: this.new_coupon_form.max_use_count,
            from_at: from_at,
            to_at: to_at,
            use_count: this.new_coupon_form.use_count,
            is_display: this.new_coupon_form.is_display,
            is_active: this.new_coupon_form.is_active, 
            description: this.new_coupon_form.description 
          }
        })
          .then(res => {
            this.isAppLoading = false;
            if (res.data.data) {
              this.getIndex();
              this.cancelNewCouponForm();
              this.showSuccess("Coupon Created.");
            }
          })
          .catch(err => {
            this.isAppLoading = false;
            console.log(err);
            this.showDanger("Oops! something went wrong please try again");
          });
        /*  */
      },
      updateCouponSubmit() {
        if (!document.querySelector(".js-update-coupon-form").checkValidity()) {
          return false;
        }
        /*  */
        this.isAppLoading = true;
        let from_at = moment(
          document.querySelector(".js-update-coupon-from-at").value,
          "MM/DD/YYYY HH:MM:SS"
        ).format("YYYY-MM-DD HH:mm:ss");
        let to_at = moment(
          document.querySelector(".js-update-coupon-to-at").value,
          "MM/DD/YYYY HH:MM:SS"
        ).format("YYYY-MM-DD HH:mm:ss");
        axios({
          method: "POST",
          url: bookingApiObj.url,
          params: {
            action: "manage_booking_coupon_api",
            security: bookingApiObj.security
          },
          data: {
            route: "update",
            id: this.selected_coupon_id,
            code: this.update_coupon_form.code,
            off_percent: this.update_coupon_form.off_percent,
            max_use_count: this.update_coupon_form.max_use_count,
            from_at: from_at,
            to_at: to_at,
            use_count: this.update_coupon_form.use_count,
            is_display: this.update_coupon_form.is_display,
            is_active: this.update_coupon_form.is_active,
            description: this.update_coupon_form.description
          }
        })
          .then(res => {
            this.isAppLoading = false;
            if (res.data.data) {
              this.getIndex();
              this.cancelUpdateCouponForm();
              this.showSuccess("Coupon Updated.");
            }
          })
          .catch(err => {
            this.isAppLoading = false;
            console.log(err);
            this.showDanger("Oops! something went wrong please try again");
          });
        /*  */
      },
      onShowNewCouponForm() {
        for (const key in this.new_coupon_form) {
          if (this.new_coupon_form.hasOwnProperty(key)) {
            switch (key) {
              case "is_display":
                continue;
                break;
              case "is_active":
                continue;
                break;
            }
            this.new_coupon_form[key] = null;
          }
        }
        this.new_coupon_form.code = this.generateCouponCode();
        this.showNewCouponForm = true;
        this.appOverlay = true;
      },
      cancelNewCouponForm() {
        this.showNewCouponForm = false;
        this.appOverlay = false;
      },
      cancelUpdateCouponForm() {
        this.showUpdateCouponForm = false;
        this.appOverlay = false;
      },
      handleIndexData(res) {
        this.indexData = res.data.data;
        this.total_pages = Math.ceil(this.indexData.total / this.per_page);

        // console.log(res.data.data);
      },
      getIndex() {
        /*  */
        this.isAppLoading = true;
        axios({
          method: "POST",
          url: bookingApiObj.url,

          params: {
            action: "manage_booking_coupon_api",
            security: bookingApiObj.security
          },
          data: {
            route: "index",
            per_page: this.per_page,
            page: this.current_page
          }
        })
          .then(res => {
            this.isAppLoading = false;
            this.handleIndexData(res);
          })
          .catch(err => {
            this.isAppLoading = false;
            console.log(err);
            this.alertDanger("Oops! something went wrong please try again");
          });
        /*  */
      },
      getCoupon() {
        /*  */
        this.isAppLoading = true;
        axios({
          method: "POST",
          url: bookingApiObj.url,
          params: {
            action: "manage_booking_coupon_api",
            security: bookingApiObj.security
          },
          data: {
            route: "edit",
            id: this.selected_coupon_id
          }
        })
          .then(res => {
            this.isAppLoading = false;
            console.log(res);
            if (res.data.data) {
              this.update_coupon_form = {
                code: res.data.data.code,
                max_use_count: res.data.data.max_use_count,
                off_percent: res.data.data.off_percent,
                from_at: res.data.data.from_at,
                to_at: res.data.data.to_at,
                is_display: res.data.data.is_display  == 1  ? true  : false,
                is_active: res.data.data.is_active  == 1  ? true  : false,
                description: res.data.data.description,
              };
              document.querySelector(
                ".js-update-coupon-from-at"
              ).value = moment(
                res.data.data.from_at,
                "YYYY-MM-DD HH:mm:ss"
              ).format("MM/DD/YYYY");
              document.querySelector(".js-update-coupon-to-at").value = moment(
                res.data.data.to_at,
                "YYYY-MM-DD HH:mm:ss"
              ).format("MM/DD/YYYY");
            }
          })
          .catch(err => {
            this.isAppLoading = false;
            console.log(err);
            this.alertDanger("Oops! something went wrong please try again");
          });
        /*  */
      },
      setFromDate() {
        console.log(e.target.value);
      },
      setToDate() {
        console.log(e.target.value);
      },
      moment: window.moment
    },
    computed: {
      total_pages_arr() {
        let tempArr = [];
        for (let index = 0; index < this.total_pages; index++) {
          tempArr.push(index + 1);
        }
        return tempArr;
      }
    }
  });
}
bookingCouponAppInit();
